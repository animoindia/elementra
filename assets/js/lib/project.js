(function ($) {
    "use strict";
	$(document).ready(function () {
    
		$('.div_loading_image').hide();
	
		// Select Dropdown
		$('html').click(function() {
			$('.select .dropdown').hide(); 
		});
		$('.select').click(function(event){
			event.stopPropagation();
		});
		$('.select .select-control').click(function(){
			$(this).parent().next().toggle();
		})    
		$('.select .dropdown li').click(function(){
			$(this).parent().toggle();
			var text = $(this).attr('rel');
			$(this).parent().prev().find('div').text(text);
		})
		
		$(document).on('click', '.map-point-sm', function() {			  
			var show = $(this).data('show'); 
			$(show).removeClass("hide").siblings().addClass("hide");			
		});	
		
		$(function(){ 
			$('.divisiondetail button.map-point-sm').click(function(){ 
				$('.divisiondetail button.map-point-sm.active').removeClass('active');	
				$(this).addClass('active');				
			});	
			$('.divisiondetail1 button.map-point-sm').click(function(){ 
				$('.divisiondetail1 button.map-point-sm.active').removeClass('active');	
				$(this).addClass('active');				
			});			
		});
		
		/*
		$(".cls-tab").on('click', function() {			  
			var ref = $(this).attr('href');
			if(ref == "#t1")
				$('#divisiondetail .step1').addClass('active');	
			else
				$('#divisiondetail .step11').addClass('active');	
				
		});	
		*/
		
		$("#project_action").off("change");
		$("#project_action").on("change", function () {
			
			var action_val = $.trim($(this).val());
			var prj_id = [];
			var error = 0;
			var error_message = 'Please select atleast one project to proceed';
			
			if(action_val != ''){
				$('.prj_chkbox:checked').each(function(){ 
					var curr_status = $(this).attr('data-status');
					var curr_step = $(this).attr('data-step');
					
					if(action_val == 'req_approval'){
						if(curr_status == 2){
							prj_id.push($(this).val());	
						} else if(curr_status > 2) {
							error = 1;
							error_message = 'Please select only those project for which approval is Pending';
						}
						
					} else if(action_val == 'assignment'){
						if(curr_status == 7){
							prj_id.push($(this).val());	
						} else if(curr_status != 7) {
							error = 1;
							error_message = 'Please select only approved project to assign site supervisor';
						}
						
					} else if(action_val == 'approve'){
						if(curr_status == 9){
							prj_id.push($(this).val());	
						} else if(curr_status != 9) {
							error = 1;
							error_message = 'Please select project assigned for approval';
						}
						
					} else if(action_val == 'reject'){
						if(curr_status == 9){
							prj_id.push($(this).val());	
						} else if(curr_status != 9) {
							error = 1;
							error_message = 'Please select project assigned for approval';
						}
						
					} else if(action_val == 'tc'){
						if(curr_status == 9){
							prj_id.push($(this).val());	
						} else if(curr_status != 9) {
							error = 1;
							error_message = 'Please select project peding for technical check';
						}
						
					} else if(action_val == 'mp'){
						if(curr_step == 3){
							prj_id.push($(this).val());	
						} else if(curr_step != 3) {
							error = 1;
							error_message = 'Please select only a project for which Technical check has been done';
						} 
						
					} else if(action_val == 'fm'){
						if(curr_step == 4){
							prj_id.push($(this).val());	
						} else if(curr_step != 4) {
							error = 1;
							error_message = 'Please select only a project for which Marking Point has been done';
						} 
						
					} else if(action_val == 'pdi'){
						if(curr_step == 5){
							prj_id.push($(this).val());	
						} else if(curr_step != 5) {
							error = 1;
							error_message = 'Please select only a project for which Final Measument has been done';
						}
						
					} else if(action_val == 'installation' ){
						if(curr_step == 6){
							prj_id.push($(this).val());	
						} else if(curr_step != 6) {
							error = 1;
							error_message = 'Please select only a project for which PDI has been done';
						} 
						
					} else if(action_val == 'finished' ){
						if(curr_step == 7){
							prj_id.push($(this).val());	
						} else if(curr_step != 7) {
							error = 1;
							error_message = 'Please select only a project for which installation has been done';
						} 
						
					} else {
						error = 1;
					}
				})
				
				if(prj_id.length == 0){
					alert(error_message);
					return false;
				}else if(prj_id.length > 0 && error == 0){
					
					var prjAction = '';
					switch(action_val){
						case 'req_approval':
						case 'assignment':
							prjAction = 'assignment';
							break;
						case 'tc':
						case 'mp':
						case 'fm':
						case 'pdi':
						case 'installation':
						case 'finished':
							prjAction = 'progress';
							break;
						default:
							prjAction = 'approval';
							break;	
					}
					
					$.ajax({
						url		: "/project/"+prjAction,
						method	: "POST",
						data	: { 'action' : action_val, 'project_ids' : prj_id },
						beforeSend: function() {
							$('.div_loading_image').show();
						},
						success: function (result) {
							$('.div_loading_image').hide();
							$('.modal-content').html(result);
							$('#myModal').modal('show');					
						},
						error: function(xhr,status,error){
							$('.div_loading_image').hide();
						}
					});
				} else {
					alert(error_message);
					return false;
				}
			} else {
				//window.location.href = '/project/list';
				return false;
			}
		}) 
		
		$('.btn_submit_assignment').on('click', function(){
			$( "#frm_assignment" ).validate();
			if($( "#frm_assignment" ).valid()){
				var formData = $('#frm_assignment').serialize();
				var i = 0;
				$.ajax({
					type : "POST",
					url: '\/project/saveAssignment',
					cache:false,
					traditional: true,
					data: formData,
					beforeSend: function() {
						$('.div_loading_image').show();
					},
					success: function(data, textStatus, jqXHR){
						$('.response').show();
						$('.response').removeClass('error');
						$('.response').addClass('success');
						$('.response').html(jqXHR.statusText);
						alert(jqXHR.statusText);
						setTimeout($('#myModal').modal('toggle'), 15000);
						$('.div_loading_image').hide();
					},
					error: function(xhr,status,error){
						$('.response').show();
						$('.response').removeClass('success');
						$('.response').addClass('error');
						$('.response').html(error);
						$('.div_loading_image').hide();
					}
				});
			}
		});
		
		$('.btn_submit_approval').on('click', function(){
			$( "#frm_approval" ).validate();
			if($( "#frm_approval" ).valid()){
				var formData = $('#frm_approval').serialize();
				var i = 0;
				$.ajax({
					type : "POST",
					url: '\/project/saveApproval',
					cache:false,
					traditional: true,
					data: formData,
					beforeSend: function() {
						$('.div_loading_image').show();
					},
					success: function(data, textStatus, jqXHR){
						
						$('.response').show();
						$('.response').removeClass('error');
						$('.response').addClass('success');
						$('.response').html(jqXHR.statusText);
						alert(jqXHR.statusText);
						setTimeout($('#myModal').modal('toggle'), 15000);
						$('.div_loading_image').hide();
					},
					error: function(xhr,status,error){
						$('.response').show();
						$('.response').removeClass('success');
						$('.response').addClass('error');
						$('.response').html(error);
						$('.div_loading_image').hide();
					},
				});
			}
		});
		
		$('form#frm_step').submit(function(e){
			
		    e.preventDefault();    
			$("#frm_step").validate();
			
			if($("#frm_step").valid()){
			    
				var formData = new FormData(this);

				var i = 0;
				$.ajax({
					type : "POST",
					url: '\/project/saveProgress',
					cache:false,
					traditional: true,
					data: formData,
					contentType: false,
					processData: false,
					beforeSend: function() {
						$('.div_loading_image').show();
					},
					success: function(data, textStatus, jqXHR){
						
						$('.response').show();
						$('.response').removeClass('error');
						$('.response').addClass('success');
						$('.response').html(jqXHR.statusText);
						alert(jqXHR.statusText);
						setTimeout($('#myModal').modal('toggle'), 15000);
						$('.div_loading_image').hide();
						location.reload();
					},
					error: function(xhr,status,error){
						$('.response').show();
						$('.response').removeClass('success');
						$('.response').addClass('error');
						$('.response').html(error);
						$('.div_loading_image').hide();
					},
				});
			}
		});

		$('form#frm_project_new').submit(function(e){
			
		    e.preventDefault();    
			$("#frm_project_new").validate();
			
			if($("#frm_project_new").valid()){
			    
				var formData = new FormData(this);
				var prj_status = $(document.activeElement).val();
				formData.append('status', prj_status);

				$.ajax({
					type : "POST",
					url: '\/project/save',
					cache:false,
					traditional: true,
					data: formData,
					contentType: false,
					processData: false,
					beforeSend: function() {
						$('.div_loading_image').show();
					},
					success: function(data, textStatus, jqXHR){
						
						$('.response').show();
						$('.response').removeClass('error');
						$('.response').addClass('success');
						$('.response').html('Project added successfuly');
						alert('Project added successfuly');
						$('.div_loading_image').hide();
						window.location.href = 'list', true;
					},
					error: function(xhr,status,error){
						$('.response').show();
						$('.response').removeClass('success');
						$('.response').addClass('error');
						$('.response').html(error);
						alert('Opps some error occured!');
						$('.div_loading_image').hide();
					},
				});
			}
		});


		$('form#frm_project_edit').submit(function(e){
			
		    e.preventDefault();    
			$("#frm_project_edit").validate();
			
			if($("#frm_project_edit").valid()){
			    
				var formData = new FormData(this);
				var prj_status = $(document.activeElement).val();
				formData.append('status', prj_status);

				var i = 0;
				$.ajax({
					type : "POST",
					url: '\/project/update',
					cache:false,
					traditional: true,
					data: formData,
					contentType: false,
					processData: false,
					beforeSend: function() {
						$('.div_loading_image').show();
					},
					success: function(data, textStatus, jqXHR){
						
						$('.response').show();
						$('.response').removeClass('error');
						$('.response').addClass('success');
						$('.response').html('Project updated successfuly');
						alert('Project updated successfuly');
						$('.div_loading_image').hide();
						window.location.href = 'list', true;
					},
					error: function(xhr,status,error){
						$('.response').show();
						$('.response').removeClass('success');
						$('.response').addClass('error');
						$('.response').html(error);
						alert('Opps some error occured!');
						$('.div_loading_image').hide();
					},
				});
			}
		});
		

		getAccordion("#tabs",768);
	
	});	
	function getAccordion(element_id,screen) 
	{
		//$(window).resize(function () { location.reload(); });

		if ($(window).width() < screen) 
		{
			var concat = '';
			obj_tabs = $( element_id + " li" ).toArray();
			obj_cont = $( ".tab-content .tab-pane" ).toArray();
			jQuery.each( obj_tabs, function( n, val ) 
			{
				concat += '<div id="' + n + '" class="panel panel-default">';
				concat += '<div class="panel-heading" role="tab" id="heading' + n + '">';
				concat += '<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' + n + '" aria-expanded="false" aria-controls="collapse' + n + '">' + val.innerText + '</a></h4>';
				concat += '</div>';
				concat += '<div id="collapse' + n + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + n + '">';
				concat += '<div class="panel-body">' + obj_cont[n].innerHTML + '</div>';
				concat += '</div>';
				concat += '</div>';
			});
			$("#accordion").html(concat);
			$("#accordion").find('.panel-collapse:first').addClass("in");
			$("#accordion").find('.panel-title a:first').attr("aria-expanded","true");
			$(element_id).remove();
			$(".tab-content").remove();
		}	
	}
	
	/**********************************Code Start Image Gallery****************************************************/
		// Gallery image hover
		$( ".img-wrapper" ).hover(
		  function() {
			$(this).find(".img-overlay").animate({opacity: 1}, 600);
		  }, function() {
			$(this).find(".img-overlay").animate({opacity: 0}, 600);
		  }
		);

		// Lightbox
		var $overlay = $('<div id="overlay"></div>');
		var $image = $("<img>");
		//var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-left"></i></div>');
		//var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-right"></i></div>');
		//var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

		// Add overlay
		//$overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
		//$overlay.append($image).append($exitButton);
		$("#gallery").append($overlay);

		// Hide overlay on default
		$overlay.hide();

		// When an image is clicked
		$(".img-overlay").click(function(event) {
		  // Prevents default behavior
		  event.preventDefault();
		  // Adds href attribute to variable
		  var imageLocation = $(this).prev().attr("href");
		  var imageName = $(this).prev().attr("title");
		 
		  // Add the image src to $image
			$('#imagepreview').attr('src', imageLocation); // here asign the image to the modal when the user click the enlarge link
			$('#image_name').html(imageName); // here asign the image to the modal when the user click the enlarge link
			$('#imagemodal').modal('show');
		  // Fade in the overlay
		  $overlay.fadeIn("slow");
		});

		// When the overlay is clicked
		$overlay.click(function() {
			$(".modal").modal("hide");
			// Fade out the overlay
			$(this).fadeOut();
		});

		// When next button is clicked
		/*$nextButton.click(function(event) {
		  // Hide the current image
		  $("#overlay img").hide();
		  // Overlay image location
		  var $currentImgSrc = $("#overlay img").attr("src");
		  console.log($currentImgSrc);
		  // Image with matching location of the overlay image
		  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
		  // Finds the next image
		  var $nextImg = $($currentImg.closest(".image").next().find("img"));
		  console.log($nextImg);
		  console.log($nextImg.length);
		  // All of the images in the gallery
		  var $images = $("#image-gallery img");
		  // If there is a next image
		  if ($nextImg.length > 0) { 
			// Fade in the next image
			$("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
		  } else {
			// Otherwise fade in the first image
			$("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
		  }
		  // Prevents overlay from being hidden
		  event.stopPropagation();
		});

		// When previous button is clicked
		$prevButton.click(function(event) {
		  // Hide the current image
		  $("#overlay img").hide();
		  // Overlay image location
		  var $currentImgSrc = $("#overlay img").attr("src");
		  // Image with matching location of the overlay image
		  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
		  // Finds the next image
		  var $prvImg = $($currentImg.closest(".image").prev().find("img"));
		   console.log($prvImg);
		  // Fade in the next image
		  $("#overlay img").attr("src", $prvImg.attr("src")).fadeIn(800);
		  // Prevents overlay from being hidden
		  event.stopPropagation();
		});*/

		// When the exit button is clicked
		$('#exitButton').on('click', function() {
		  // Fade out the overlay
		  $("#overlay").fadeOut("slow");
		  $(".modal").modal("hide");
		  
		});
		
		$('#imagemodal').on('hidden.bs.modal', function () {
			$("#overlay").fadeOut("slow");
		})
		/**********************************Code Start Image Gallery****************************************************/
	
}(jQuery));
