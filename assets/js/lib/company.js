/* jquery.validate plagin added using cdn. Go to jqueryvalidation.org to see what methods are provided */
/* Create custom validation method */
/*$.validator.addMethod("startWithA", function(value, element) {
	return /^A/.test(value);
}, "Field must start with A");
*/
// Wait for the DOM to be ready
(function ($) {
	"use strict";
	
	$('.div_loading_image').hide();
	
  $("form[name='frm_company']").validate({
		// Specify validation rules
		rules: {
		  "data[name]": "required",
		  "data[address]": "required",
		  "data[gst_no]": "required",
		  "data[contact_person]": "required",
		  "data[contact_no]": "required",
		  "data[email]": {
			required: true,
			email: true
		  }
		},
		messages: {
		  "data[name]": "Please enter company name",
		  "data[address]": "Please enter Address",
		  "data[gst_no]": "Please enter GST Number",
		  "data[contact_no]": "Please enter Contact Number",
		  "data[contact_person]": "Please enter Contact Person",
		  "data[email]": "Please enter a valid email address"
		},
	});
	
	$('form#frm_company').submit(function(e){
			
		e.preventDefault();
		
		$( "#frm_company" ).validate();
		if($( "#frm_company" ).valid()){
			
			var formData = new FormData(this);
			
			$.ajax({
				type : "POST",
				url: '\/company/save',
				cache:false,
				traditional: true,
				data: formData,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('.div_loading_image').show();
				},
				success: function(data, textStatus, jqXHR){
					
					$('.response').show();
					$('.response').removeClass('error');
					$('.response').addClass('success');
					$('.response').html(jqXHR.statusText);
					alert(jqXHR.statusText);
					setTimeout($('#myModal').modal('toggle'), 15000);
					$('.div_loading_image').hide();
					window.location.href = 'list', true;
				},
				error: function(xhr,status,error){
					$('.response').show();
					$('.response').removeClass('success');
					$('.response').addClass('error');
					$('.response').html(error);
					$('.div_loading_image').hide();
				},
				complete: function() {
				},
			});
		}
	});
	
}(jQuery));