// Wait for the DOM to be ready
(function ($) {
	
	"use strict";
 	
	$('.div_loading_image').hide();
	
	$('form#frm_feedback').submit(function(e){
		e.preventDefault()
		$( "#frm_feedback" ).validate();
		if($( "#frm_feedback" ).valid()){
			var formData = new FormData(this);
			
			$.ajax({
				type : "POST",
				url: '\/feedback/save',
				cache:false,
				traditional: true,
				data: formData,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('.div_loading_image').show();
				},
				success: function(data, textStatus, jqXHR){
					
					$('.response').show();
					$('.response').removeClass('error');
					$('.response').addClass('success');
					$('.response').html(jqXHR.statusText);
					alert(jqXHR.statusText);
					$('.div_loading_image').hide();
					location.reload();
				},
				error: function(xhr,status,error){
					$('.response').show();
					$('.response').removeClass('success');
					$('.response').addClass('error');
					$('.response').html(error);
					alert(error);
					$('.div_loading_image').hide();
				},
				complete: function() {
				},
			});
		}
	});
	
}(jQuery));