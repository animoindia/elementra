// Wait for the DOM to be ready
(function ($) {
	"use strict";
	
    var mainApp = {

        initFunction: function () {
            /*MENU 
            ------------------------------------*/
            $('#main-menu').metisMenu();
			
            $(window).bind("load resize", function () {
                if ($(this).width() < 961) {
                    $('div.sidebar-collapse').addClass('collapse')
                } else {
                    $('div.sidebar-collapse').removeClass('collapse')
                }
            });
        
        },

        initialization: function () {
            mainApp.initFunction();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.initFunction(); 
		$("#sideNav").click(function(){
			if($(this).hasClass('closed')){
				$('.navbar-side').animate({left: '0px'});
				$(this).removeClass('closed');
				$('#page-wrapper').animate({'margin-left' : '260px'});
				
			}
			else{
			    $(this).addClass('closed');
				$('.navbar-side').animate({left: '-260px'});
				$('#page-wrapper').animate({'margin-left' : '0px'}); 
			}
		});
		
		$(".navbar-nav .dropdown-menu li a").click(function(){
			var selText = $(this).text();
			$(this).parents('.navbar-nav').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
		});
		
		//* File upload Code Taken From URL: https://codepen.io/jacoahmad/pen/mmYRqe */
		var readURL = function(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('.profile-pic').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}
		$(".file-upload").on('change', function(){
			readURL(this);
		});

		$(".upload-button").on('click', function() {
			$(".file-upload").click();
		});
		//* END Code Taken From URL: https://codepen.io/jacoahmad/pen/mmYRqe */
		
		$('#myModal').on('hidden.bs.modal', function () {
			//$('.div_loading_image').show();
			//location.reload();
			$(this).removeData();
		})
		
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY'
		});	
	
		var protocol = $(location).attr('protocol');
		var hostname = '//'+$(location).attr('hostname');
		var domain   = protocol+hostname; 
		
    });
	
}(jQuery));