// Wait for the DOM to be ready
(function ($) {
	"use strict";
	
	$('.div_loading_image').hide();
	
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
	$("form[name='frm_user']").validate({
		// Specify validation rules
		rules: {
		  title: "required",
		  firstname: "required",
		  lastname: "required",
		  mobile_no: "required",		  
		  email: {
			required: true,
			email: true
		  },
		  password: {
			required: true,
			minlength: 5
		  }
		},
		messages: {
		  firstname: "Please enter your firstname",
		  lastname: "Please enter your lastname",
		  password: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long"
		  },
		  email: "Please enter a valid email address"
		},
	});
	
	$('form#frm_user').submit(function(e){
		e.preventDefault()
		$( "#frm_user" ).validate();
		if($( "#frm_user" ).valid()){
			var formData = new FormData(this);
			
			$.ajax({
				type : "POST",
				url: '\/user/save',
				cache:false,
				traditional: true,
				data: formData,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('.div_loading_image').show();
				},
				success: function(data, textStatus, jqXHR){
					
					$('.response').show();
					$('.response').removeClass('error');
					$('.response').addClass('success');
					$('.response').html(jqXHR.statusText);
					alert(jqXHR.statusText);
					setTimeout($('#myModal').modal('toggle'), 15000);
					$('.div_loading_image').hide();
					window.location.href = 'list', true;
				},
				error: function(xhr,status,error){
					$('.response').show();
					$('.response').removeClass('success');
					$('.response').addClass('error');
					$('.response').html(error);
					$('.div_loading_image').hide();
				},
				complete: function() {
				},
			});
		}
	});
	
	$("form[name='frm_changepwd']").validate({
		rules: {
		  cnf_password: {
			required: true,
			minlength: 6
		  },
		  "data[password]": {
			required: true,
			minlength: 6
		  }
		},
		
		messages: {
		   cnf_password: {
			required: "confirm password required",
			minlength: "Your password must be at least 6 characters long"
		  },
		   "data[password]": {
			required: "new password required",
			minlength: "Your password must be at least 6 characters long"
		  },
		  
		},
	});
	
	$("form[name='frm_changepwd']").submit(function(e){
		e.preventDefault()
	});
	
	$('.btn_submit_changepwd').on('click', function(){
		$( "#frm_changepwd" ).validate();
		if($( "#frm_changepwd" ).valid()){
			var formData = $('#frm_changepwd').serialize();
			var i = 0;
			$.ajax({
				type : "POST",
				url: '\/user/savePassword',
				cache:false,
				traditional: true,
				data: formData,
				beforeSend: function() {
					$('.div_loading_image').show();
				},
				success: function(data, textStatus, jqXHR){
					
					$('.response').show();
					$('.response').removeClass('error');
					$('.response').addClass('success');
					$('.response').html(jqXHR.statusText);
					alert(jqXHR.statusText);
					setTimeout($('#myModal').modal('toggle'), 15000);
					$('.div_loading_image').hide();
				},
				error: function(xhr,status,error){
					$('.response').show();
					$('.response').removeClass('success');
					$('.response').addClass('error');
					$('.response').html(error);
					$('.div_loading_image').hide();
				},
				complete: function() {
				},
			});
		}
	});
}(jQuery));

