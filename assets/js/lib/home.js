// Wait for the DOM to be ready
(function ($) {
	"use strict";
	
	$('.div_loading_image').hide();
	
	$("form[name='frm_newpwd']").submit(function(e){
		e.preventDefault()
	});
	
	$("form[name='frm_newpwd']").validate({
		rules: {
		  current_password: {
			required: true,
			minlength: 4
		  },
		  cnf_password: {
			required: true,
			minlength: 6
		  },
		  "data[password]": {
			required: true,
			minlength: 6
		  }
		},
		
		messages: {
		  current_password: {
			required: "current password required",
			minlength: "Your password must be at least 6 characters long"
		  },
		   cnf_password: {
			required: "confirm password required",
			minlength: "Your password must be at least 6 characters long"
		  },
		   "data[password]": {
			required: "new password required",
			minlength: "Your password must be at least 6 characters long"
		  },
		  
		},
	});
	
	$('.btn_submit_newpwd').on('click', function(){
		$( "#frm_newpwd" ).validate();
		if($( "#frm_newpwd" ).valid()){
			var formData = $('#frm_newpwd').serialize();
			var i = 0;
			$.ajax({
				type : "POST",
				url: '\/home/saveNewPassword',
				cache:false,
				traditional: true,
				data: formData,
				beforeSend: function() {
					$('.div_loading_image').show();
				},
				success: function(data, textStatus, jqXHR){
					
					$('.response').show();
					$('.response').removeClass('error');
					$('.response').addClass('success');
					$('.response').html(jqXHR.statusText);
					alert(jqXHR.statusText);
					$('.div_loading_image').hide();
					window.location.href = '/', true;
				},
				error: function(xhr,status,error){
					$('.response').show();
					$('.response').removeClass('success');
					$('.response').addClass('error');
					$('.response').html(error);
					$('.div_loading_image').hide();
				},
				complete: function() {
				},
			});
		}
	});
	
	$('form#frm_forgot_pwd').submit(function(e){
			
		e.preventDefault();
		
		$( "#frm_forgot_pwd" ).validate();
		if($( "#frm_forgot_pwd" ).valid()){
			
			var formData = new FormData(this);
			
			$.ajax({
				type : "POST",
				url: 'validateUser',
				cache:false,
				traditional: true,
				data: formData,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('.div_loading_image').show();
				},
				success: function(data, textStatus, jqXHR){
					
					$('.response').show();
					$('.response').removeClass('error');
					$('.response').addClass('success');
					$('.response').html(jqXHR.statusText);
					alert(jqXHR.statusText);
					setTimeout($('#myModal').modal('toggle'), 15000);
					$('.div_loading_image').hide();
					location.reload();
				},
				error: function(xhr,status,error){
					$('.response').show();
					$('.response').removeClass('success');
					$('.response').addClass('error');
					$('.response').html(error);
					$('.div_loading_image').hide();
				},
				complete: function() {
				},
			});
		}
	});
	
	$("form[name='frm_resetpwd']").submit(function(e){
		e.preventDefault()
	});
	$('.btn_submit_resetpwd').on('click', function(){
		$( "#frm_resetpwd" ).validate();
		if($( "#frm_resetpwd" ).valid()){
			var formData = $('#frm_resetpwd').serialize();
			var i = 0;
			$.ajax({
				type : "POST",
				url: '\/home/updatePassword',
				cache:false,
				traditional: true,
				data: formData,
				beforeSend: function() {
					$('.div_loading_image').show();
				},
				success: function(data, textStatus, jqXHR){
					
					$('.response').show();
					$('.response').removeClass('error');
					$('.response').addClass('success');
					$('.response').html(jqXHR.statusText);
					alert(jqXHR.statusText);
					$('.div_loading_image').hide();
					window.location.href = '/', true;
				},
				error: function(xhr,status,error){
					$('.response').show();
					$('.response').removeClass('success');
					$('.response').addClass('error');
					$('.response').html(error);
					$('.div_loading_image').hide();
				},
				complete: function() {
				},
			});
		}
	});	
	
	$('.a_forgot_pwd').on('click', function(){
		$('.div_login').hide();
		$('.div_forgot_pwd').show();
		$('#frm_admin_login')[0].reset();
	});
	
	$('.go_back').on('click', function(){
		$('.div_forgot_pwd').hide();
		$('.div_login').show();
		$('#frm_forgot_pwd')[0].reset();
	});
	
}(jQuery));