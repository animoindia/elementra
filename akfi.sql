-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2019 at 09:35 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `akfl`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company_master`
--

DROP TABLE IF EXISTS `tbl_company_master`;
CREATE TABLE IF NOT EXISTS `tbl_company_master` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `address` varchar(500) NOT NULL,
  `gst_no` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `website` varchar(150) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `created_by` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `gst_no` (`gst_no`),
  UNIQUE KEY `email` (`email`),
  KEY `contact_no` (`contact_no`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_company_master`
--

INSERT INTO `tbl_company_master` (`id`, `name`, `address`, `gst_no`, `email`, `contact_person`, `contact_no`, `website`, `logo`, `created_by`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'AKFItted Interrior', 'Andheri - West', '1234566', 'admin@akfi.com', 'Darshil', '989205805', 'https://www.akfl.com', 'logo_220119101722_vajpayee_loam.JPG', 1, NULL, '2019-01-22 11:54:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_master`
--

DROP TABLE IF EXISTS `tbl_department_master`;
CREATE TABLE IF NOT EXISTS `tbl_department_master` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `sort_index` int(1) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_department_master`
--

INSERT INTO `tbl_department_master` (`id`, `name`, `sort_index`, `is_active`, `created_at`) VALUES
(1, 'Management', NULL, 1, '2018-11-26 13:33:03'),
(2, 'Account', NULL, 1, '2018-11-26 13:35:13'),
(3, 'Operations', NULL, 1, '2018-11-26 13:35:13'),
(4, 'Others', NULL, 1, '2018-11-26 13:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_designation_master`
--

DROP TABLE IF EXISTS `tbl_designation_master`;
CREATE TABLE IF NOT EXISTS `tbl_designation_master` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `sort_index` int(1) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_designation_master`
--

INSERT INTO `tbl_designation_master` (`id`, `name`, `sort_index`, `is_active`, `created_at`) VALUES
(1, 'Administrator', NULL, 1, '2018-11-26 13:31:34'),
(2, 'Accountant', NULL, 1, '2018-11-26 13:31:34'),
(3, 'Site Supervisor', NULL, 1, '2018-11-26 13:31:34'),
(4, 'Designer', NULL, 1, '2018-11-26 13:31:34'),
(5, 'Architect', NULL, 1, '2018-11-26 13:31:34'),
(6, 'Technician', NULL, 1, '2018-11-26 13:31:34'),
(7, 'Others', NULL, 1, '2018-11-26 13:31:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feedback_master`
--

DROP TABLE IF EXISTS `tbl_feedback_master`;
CREATE TABLE IF NOT EXISTS `tbl_feedback_master` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `project_id` int(1) NOT NULL,
  `type` varchar(50) NOT NULL,
  `data` text NOT NULL,
  `created_by` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_form_field_master`
--

DROP TABLE IF EXISTS `tbl_form_field_master`;
CREATE TABLE IF NOT EXISTS `tbl_form_field_master` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `form_id` int(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(500) NOT NULL,
  `sort_order` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `form_category_id` (`form_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_form_field_master`
--

INSERT INTO `tbl_form_field_master` (`id`, `form_id`, `name`, `description`, `sort_order`, `created_at`, `is_active`) VALUES
(1, 1, 'Project Type', 'Project Type', NULL, '2018-12-03 07:30:00', 1),
(2, 1, 'Project For', 'Project For', NULL, '2018-12-03 07:30:00', 1),
(3, 1, 'Client Name', 'Client Name', NULL, '2018-12-03 07:30:00', 1),
(4, 1, 'Client Phone', 'Client Phone', NULL, '2018-12-03 07:30:00', 1),
(5, 1, 'Client Address', 'Client Address', NULL, '2018-12-03 07:30:00', 1),
(6, 1, 'Client Details', 'Client Details', NULL, '2018-12-03 07:30:00', 1),
(7, 1, 'Client Remarks', 'Client Remarks', NULL, '2018-12-03 07:30:00', 1),
(8, 2, 'Core Material', 'Core Material', NULL, '2018-12-03 07:30:00', 1),
(9, 2, 'Base Door Finish', 'Base Door Finish', NULL, '2018-12-03 07:30:00', 1),
(10, 2, 'Wall Door Finish', 'Wall Door Finish', NULL, '2018-12-03 07:30:00', 1),
(11, 2, 'Tall Door Finish', 'Tall Door Finish', NULL, '2018-12-03 07:30:00', 1),
(12, 3, 'Base Shutter Colour', 'Base Shutter Colour', NULL, '2018-12-03 07:30:00', 1),
(13, 3, 'Base Shutter Back Colour', 'Base Shutter Back Colour', NULL, '2018-12-03 07:30:00', 1),
(14, 3, 'Wall Shutter Colour', 'Wall Shutter Colour', NULL, '2018-12-03 07:30:00', 1),
(15, 3, 'Wall Shutter Back Colour', 'Wall Shutter Back Colour', NULL, '2018-12-03 07:30:00', 1),
(16, 3, 'Wall Carcass Colour', 'Wall Carcass Colour', NULL, '2018-12-03 07:30:00', 1),
(17, 3, 'Tall Shutter Colour', 'Tall Shutter Colour', NULL, '2018-12-03 07:30:00', 1),
(18, 3, 'Tall Carcass Colour', 'Tall Carcass Colour', NULL, '2018-12-03 07:30:00', 1),
(19, 3, 'Tall Cabinet Visible Side Colour', 'Tall Cabinet Visible Side Colour', NULL, '2018-12-03 07:30:00', 1),
(20, 3, 'Drawer Bottom And Back Colour', 'Drawer Bottom And Back Colour', NULL, '2018-12-03 07:30:00', 1),
(21, 4, 'Base Cabinet Depth', 'Base Cabinet Depth', NULL, '2018-12-03 07:30:00', 1),
(22, 4, 'Base Shutter Height', 'Base Shutter Height', NULL, '2018-12-03 07:30:00', 1),
(23, 4, 'Wall Cabinet Height ', 'Wall Cabinet Height', NULL, '2018-12-03 07:30:00', 1),
(24, 4, 'Wall Cabinet Depth', 'Wall Cabinet Depth', NULL, '2018-12-03 07:30:00', 1),
(25, 4, 'Tall Cabinet Depth', 'Tall Cabinet Depth', NULL, '2018-12-03 07:30:00', 1),
(26, 4, 'Tall Cabinet Height', 'Tall Cabinet Height', NULL, '2018-12-03 07:30:00', 1),
(27, 4, 'Skirting Height', 'Skirting Height', NULL, '2018-12-03 07:30:00', 1),
(28, 4, 'Skirting Finish', 'Skirting Finish', NULL, '2018-12-03 07:30:00', 1),
(29, 4, 'Platform Depth', 'Platform Depth', NULL, '2018-12-03 07:30:00', 1),
(30, 4, 'Counter Height', 'Counter Height', NULL, '2018-12-03 07:30:00', 1),
(31, 5, 'Drawer Systems', 'Drawer Systems', NULL, '2018-12-03 07:30:00', 1),
(32, 5, 'Lift Up Systems', 'Lift Up Systems', NULL, '2018-12-03 07:30:00', 1),
(33, 5, 'Hinges', 'Hinges', NULL, '2018-12-03 07:30:00', 1),
(34, 6, 'Dustbin', 'Dustbin', NULL, '2018-12-03 07:30:00', 1),
(35, 6, 'Detergent', 'Detergent', NULL, '2018-12-03 07:30:00', 1),
(36, 6, 'Cutlery Tray', 'Cutlery Tray', NULL, '2018-12-03 07:30:00', 1),
(37, 6, 'Dishrack', 'Dishrack', NULL, '2018-12-03 07:30:00', 1),
(38, 6, 'Pullout Baskets', 'Pullout Baskets', NULL, '2018-12-03 07:30:00', 1),
(39, 6, 'Handle (Base)', 'Handle (Base)', NULL, '2018-12-03 07:30:00', 1),
(40, 6, 'Handle (1st Layer Wall Cabinet)', 'Handle (1st Layer Wall Cabinet)', NULL, '2018-12-03 07:30:00', 1),
(41, 6, 'Handle (Tall)', 'Handle (Tall)', NULL, '2018-12-03 07:30:00', 1),
(42, 6, 'Profile Details', 'Profile Details', NULL, '2018-12-03 07:30:00', 1),
(43, 6, 'Tall Units', 'Tall Units', NULL, '2018-12-03 07:30:00', 1),
(44, 7, 'Chimney', 'Chimney', NULL, '2018-12-03 07:30:00', 1),
(45, 7, 'Hob', 'Hob', NULL, '2018-12-03 07:30:00', 1),
(46, 7, 'Sink', 'Sink', NULL, '2018-12-03 07:30:00', 1),
(47, 7, 'Tap', 'Tap', NULL, '2018-12-03 07:30:00', 1),
(48, 7, 'Micro', 'Micro', NULL, '2018-12-03 07:30:00', 1),
(49, 7, 'Oven', 'Oven', NULL, '2018-12-03 07:30:00', 1),
(50, 7, 'Dishwasher', 'Dishwasher', NULL, '2018-12-03 07:30:00', 1),
(51, 7, 'Fridge', 'Fridge', NULL, '2018-12-03 07:30:00', 1),
(52, 7, 'Any Other, Appliance', 'Any Other, Appliance', NULL, '2018-12-03 07:30:00', 1),
(53, 7, 'Counter Top', 'Counter Top', NULL, '2018-12-03 07:30:00', 1),
(54, 7, 'Granite Edge Type For Counter', 'Granite Edge Type For Counter', NULL, '2018-12-03 07:30:00', 1),
(55, 8, 'Led Light Details', 'Led Light Details', NULL, '2018-12-03 07:30:00', 1),
(56, 8, 'Led Light Placements', 'Led Light Placements', NULL, '2018-12-03 07:30:00', 1),
(57, 8, 'Base Cabinet Shelves', 'Base Cabinet Shelves', NULL, '2018-12-03 07:30:00', 1),
(58, 8, 'Wall Cabinet Shelves', 'Wall Cabinet Shelves', NULL, '2018-12-03 07:30:00', 1),
(59, 8, 'Tall Cabinet Shelves', 'Tall Cabinet Shelves', NULL, '2018-12-03 07:30:00', 1),
(60, 9, '2D Autocad With Measurements and Detials', '2D Autocad With Measurements and Detials', NULL, '2018-12-03 07:30:00', 1),
(61, 9, '3D Drawings', '3D Drawings', NULL, '2018-12-03 07:30:00', 1),
(62, 10, 'Site Measurements', 'Site Measurements', NULL, '2018-12-03 07:30:00', 1),
(63, 10, 'Hardware Technical Check', 'Hardware Technical Check', NULL, '2018-12-03 07:30:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_form_section_master`
--

DROP TABLE IF EXISTS `tbl_form_section_master`;
CREATE TABLE IF NOT EXISTS `tbl_form_section_master` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `header` varchar(255) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `form` varchar(250) NOT NULL,
  `sort_order` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_form_section_master`
--

INSERT INTO `tbl_form_section_master` (`id`, `header`, `description`, `form`, `sort_order`, `created_at`, `is_active`) VALUES
(1, 'Client & Site Details', 'Client & Site Details', 'enquiry', NULL, '2018-11-27 02:35:41', 1),
(2, 'Base Material And Shutter Finish', 'Base Material And Shutter Finish', 'enquiry', NULL, '2018-11-27 02:35:41', 1),
(3, 'Front And Internal Colors', 'Front And Internal Colors', 'enquiry', NULL, '2018-11-27 02:35:41', 1),
(4, 'Heights And Depths', 'Heights And Depths', 'enquiry', NULL, '2018-11-27 02:39:10', 1),
(5, 'Hardware Details', 'Hardware Details', 'enquiry', NULL, '2018-11-27 02:39:10', 1),
(6, 'Accessories', 'Accessories', 'enquiry', NULL, '2018-11-27 02:39:10', 1),
(7, 'Appliances And Counter Details', 'Appliances And Counter Details', 'enquiry', NULL, '2018-11-27 02:39:10', 1),
(8, 'Special Remarks', 'Special Remarks', 'enquiry', NULL, '2018-11-27 02:39:10', 1),
(9, 'Drawings With Markings', 'Drawings With Markings', 'enquiry', NULL, '2018-11-27 02:39:10', 1),
(10, 'Technical Check', 'Technical Check', 'audit', NULL, '2018-11-27 02:39:10', 1),
(11, 'Marking Of Points', 'Marking Of Points', 'audit', NULL, '2018-11-27 02:39:10', 1),
(12, 'Final Measurements', 'Final Measurements', 'audit', NULL, '2018-11-27 02:39:10', 1),
(13, 'PDI', 'PDI', 'audit', NULL, '2018-11-27 02:39:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_master`
--

DROP TABLE IF EXISTS `tbl_project_master`;
CREATE TABLE IF NOT EXISTS `tbl_project_master` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `project_for` varchar(50) NOT NULL,
  `client_name` varchar(150) NOT NULL,
  `client_contact` varchar(15) NOT NULL,
  `data` text NOT NULL,
  `remarks` text NOT NULL,
  `status` int(2) NOT NULL,
  `file_path` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(2) NOT NULL,
  `updated_by` int(2) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_transaction_log`
--

DROP TABLE IF EXISTS `tbl_project_transaction_log`;
CREATE TABLE IF NOT EXISTS `tbl_project_transaction_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `project_id` int(10) NOT NULL,
  `data` text,
  `remarks` text,
  `step_id` int(1) NOT NULL,
  `status_id` int(2) NOT NULL,
  `assigned_to` int(2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(2) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `status` (`status_id`),
  KEY `created_by` (`created_by`),
  KEY `enquiry_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status_master`
--

DROP TABLE IF EXISTS `tbl_status_master`;
CREATE TABLE IF NOT EXISTS `tbl_status_master` (
  `id` int(1) NOT NULL,
  `status` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `status_name` (`status`),
  KEY `status_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_status_master`
--

INSERT INTO `tbl_status_master` (`id`, `status`, `description`, `created_by`, `created_at`, `is_active`) VALUES
(1, 'Drafted', 'Drafted', 1, '2017-10-08 07:22:11', 1),
(2, 'Confirmed', 'Confirmed', 1, '2017-10-08 07:22:11', 1),
(3, 'Cancelled', 'Cancelled', 1, '2017-10-09 02:39:52', 1),
(4, 'In Process', 'In Process', 1, '2017-10-08 07:22:11', 1),
(5, 'Completed', 'Completed', 1, '2017-10-08 07:22:11', 1),
(6, 'Success', 'Success', 1, '2017-10-09 22:38:43', 1),
(7, 'Approved', 'Approved', 1, '2017-10-09 22:38:43', 1),
(8, 'Rejected', 'rejected', 1, '2017-10-09 22:38:43', 1),
(9, 'Assigned', 'assigned', 1, '2017-10-09 22:38:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_step_master`
--

DROP TABLE IF EXISTS `tbl_step_master`;
CREATE TABLE IF NOT EXISTS `tbl_step_master` (
  `id` int(1) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_step_master`
--

INSERT INTO `tbl_step_master` (`id`, `name`, `description`, `created_by`, `created_at`, `is_active`) VALUES
(1, 'Accountant Approval', 'Accountant Approval', 1, '2017-10-08 01:52:11', 1),
(2, 'Site Supervisor', 'Site Supervisor', 1, '2017-10-08 01:52:11', 1),
(3, 'Technical Check', 'Technical Check', 1, '2017-10-08 21:09:52', 1),
(4, 'Marking Of Points', 'Marking Of Points', 1, '2017-10-08 01:52:11', 1),
(5, 'Final Measurement', 'Final Measurement', 1, '2017-10-08 01:52:11', 1),
(6, 'PDI', 'PDI', 1, '2017-10-08 01:52:11', 1),
(7, 'Installation', 'Installation', 1, '2017-10-08 01:52:11', 1),
(8, 'Finished', 'Finished', 1, '2017-10-08 01:52:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_master`
--

DROP TABLE IF EXISTS `tbl_user_master`;
CREATE TABLE IF NOT EXISTS `tbl_user_master` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `company_id` int(2) NOT NULL,
  `user_type_id` int(1) NOT NULL DEFAULT '3',
  `designation_id` int(2) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `title` varchar(10) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `f_name` varchar(255) DEFAULT NULL,
  `m_name` varchar(255) DEFAULT NULL,
  `l_name` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `avtar` varchar(255) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `pincode` varchar(50) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `activation_key` varchar(500) NOT NULL,
  `profile_activated` int(1) DEFAULT '0',
  `profile_activated_date` timestamp NULL DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `mobile_no` (`mobile_no`) USING BTREE,
  KEY `company_id` (`company_id`),
  KEY `user_type` (`user_type_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user_master`
--

INSERT INTO `tbl_user_master` (`id`, `company_id`, `user_type_id`, `designation_id`, `mobile_no`, `email`, `password`, `title`, `gender`, `f_name`, `m_name`, `l_name`, `dob`, `avtar`, `address`, `pincode`, `city`, `state`, `activation_key`, `profile_activated`, `profile_activated_date`, `created_by`, `created_at`, `updated_by`, `updated_at`, `is_active`) VALUES
(1, 1, 1, 1, '9892058088', 'admin@akfi.com', '$2y$12$YbCiMnqarpKK5OgwI7NcxuYX.NEghDELzO/QhbRMrG8Z0cqSG0wWa', 'MR', 'male', 'Darshil', 'm', 'shah', '2020-12-20', 'avtar_220119114222_vajpayee_loam.JPG', 'asdfg', '400059', 'Mumbai', 'Maharashtra', '', 1, '2018-12-03 13:00:00', 1, '2018-12-22 13:00:00', 1, '2019-01-22 12:48:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_type_master`
--

DROP TABLE IF EXISTS `tbl_user_type_master`;
CREATE TABLE IF NOT EXISTS `tbl_user_type_master` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `sort_index` int(1) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user_type_master`
--

INSERT INTO `tbl_user_type_master` (`id`, `type`, `sort_index`, `is_active`, `created_at`) VALUES
(1, 'super admin', NULL, 1, '2018-11-26 18:55:31'),
(2, 'admin', NULL, 1, '2018-11-26 18:56:24'),
(3, 'user', NULL, 1, '2018-11-26 18:56:40');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_form_field_master`
--
ALTER TABLE `tbl_form_field_master`
  ADD CONSTRAINT `from_ref_id` FOREIGN KEY (`form_id`) REFERENCES `tbl_form_section_master` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
