<?php

define('MIN_PAGE_LIMIT',5); 
define('MAX_PAGE_LIMIT',90000);
define('MAX_RECORD_LIMIT',1);

define('EMAIL_FROM_ADMIN', "admin@akfittedinteriors.in");

define('EMAIL_CC', "admin@akfittedinteriors.in");
define('EMAIL_BCC', "admin@akfittedinteriors.in");

define('EMAIL_SUBJECT_REGISTRATION', 'Welcome to AKFIttedinterior!');
define('EMAIL_SUBJECT_RESET_PWD', 'Reset your AKFIttedinterior Account Password');
define('EMAIL_SUBJECT_PWD_CHANGED', 'Your AKFIttedinterior account password has been changed.');
define('EMAIL_SUBJECT_FEEDBACK', 'FEEDBACK for the project : ');

define('IMG_PATH', '../../assets/img/');
define('IMAGE_VIEW_PATH', '../../assets/uploads/images/');
define('IMG_UPLOAD_PATH', 'assets/uploads/images');

define('GST', '3');
define('LANGUAGE', 'EN');
define('CURRENCY', 'INR');

define('ARR_TITLE', serialize(array(''=> 'Select', 'Mr.'=> 'Mr', 'Ms.'=> 'Miss', 'Mrs.'=> 'Mrs')));
define('ARR_GENDER', serialize(array(''=> 'Select', 'Male'=> 'Male', 'Female'=> 'Female')));
define('ARR_ADDRESSTYPE', serialize(array('Home'=> 'Home', 'Office'=> 'Office')));
define('ARR_CATEGORY', serialize(array('Kitchen'=> 'Kitchen', 'Wardrobe'=> 'Wardrobe')));
define('ARR_DESIGNATION', serialize(array(''=>'Designation', '1'=> 'Administrator', '2'=> 'Accountant', '3'=> 'Site Supervisor', '4'=> 'Designer')));