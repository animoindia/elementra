<?php

class DBManager
{
	/**
     * The database manager instance. 
     *
     */
  	protected $dbh;
	protected $objUtil;
	
    public function __construct(Container $container = null)
    {
		$this->objUtil 	= new Util();
		
        try {
			$dbh = new PDO("mysql:dbname=".DB_NAME.";host=".DB_HOST.";charset=utf8", DB_USER, DB_PWD, array(PDO::ATTR_PERSISTENT => true));

			$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch (PDOException $e) { 

			$dbh ='Error: '.$e->getMessage();
			die();
		}

		return $this->dbh = $dbh;
    }
	
    protected function executeSQL($params) {

    	@extract($params);
		$result = [];
		$action = explode(' ', trim($query));
		$action = strtoupper($action[0]);		

    	try {
    		if($action == 'SELECT') {
				
				$stmt = $this->dbh->prepare($query);
				
				if( isset($bindValues) ) {
					foreach($bindValues as $key => $value) {
						$stmt->bindValue($key, $value); // Another Method with STRING
						//$stmt->bindParam($key, $value, PDO::PARAM_INT); // Another Method with INT
					}
				}

				$stmt->execute();
				//$stmt->debugDumpParams();
								
				$result['result'] = ($fetch == 'one') ? $stmt->fetch(PDO::FETCH_ASSOC) : $stmt->fetchAll(PDO::FETCH_ASSOC) ;
				//print_r($result['result']);
				//exit;

    		} else {
				
    			$this->dbh->beginTransaction();
				
				$stmt = $this->dbh->prepare($query);
				
				if( isset($bindValues) ) {
					foreach($bindValues as $key => $value) {
						$stmt->bindValue($key, $value); // Another Method with STRING
						//$stmt->bindParam($key, $value, PDO::PARAM_INT); // Another Method with INT
					}
				}
				//$stmt->debugDumpParams();
			
				$stmt->execute();
				
				$result['lastInsertedId'] = $this->dbh->lastInsertId(); 

				$this->dbh->commit();
				
    		}
			
    		$result['success'] = 'success';
		
		} catch (Exception $e) {

			if($action != 'SELECT') {
		  		$this->dbh->rollBack();
		  	}

		  	$result['error'] = "Failed: " . $e->getMessage();
		}
		//print_r($result);
		//exit;
		unset($action);

		return $result;
    }
	
	public function insertTableData($table, $data) {
		
		$data = $this->objUtil->sanitizeData($data);
		
		$columns 	= implode(',', array_keys($data));
		$values 	= $this->objUtil->arrayKeyPrefix($data);
		$values 	= implode(',', array_keys($values));
		
		$query 	= "INSERT INTO $this->table ($columns) values ($values)";
		
		$params = [];
		$params['action'] = 'INSERT';
		$params['query']  = $query;
		
		foreach($data as $key => $value){
			$params['bindValues'][":$key"]   = $value;
		}
		
		return $this->executeSQL($params);
	}
	
    public function updateTableData( $table, $data ) {

		$id = $data['id'];

		$data = $this->objUtil->sanitizeData($data['data']);
				
		@extract($data);
		
		$query 	= "UPDATE $table SET ";
		
		foreach($data as $key=>$value) {
			$query .= " $key = :$key ,";
		}
		
		$query = rtrim($query, ',');
		
		$query .= " WHERE id = :id";
		
		$params = [];
		$params['action'] = 'UPDATE';
		$params['query']  = $query;
		$params['bindValues'][":id"]  = $id;
		
		foreach($data as $key => $value){
			$params['bindValues'][":$key"]  = $value;
		}
		
		return $this->executeSQL($params);
	}
}