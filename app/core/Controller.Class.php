<?php
Class Controller
{
	public function __construct() {
	
	}
	protected function setModel( $model ) {
		
		$model = trim($model);

		require_once ROOT_PATH . 'app/'.APP.'/models/' . $model .'.Class.php' ;

		return new $model();
	}
	protected function renderView( $view, $data = [] ) {

		require_once ROOT_PATH . 'app/'.APP.'/views/' . $view .'.php' ;

	}
	protected function getTemplateData( $view, $data = [] ) {

		ob_start();

		include_once ROOT_PATH . 'app/'.APP.'/views/' . $view .'.php' ;

		return ob_get_contents();

	}

}