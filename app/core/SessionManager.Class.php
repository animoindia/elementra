<?php

class SessionManager
{
	function __construct($app) {
		
		if(!self::validateSession()) { 
			self::destroySession("AKFIttedinteriors-$app");
		} else {
			self::startSession("AKFIttedinteriors-$app",  0, '/', 'akfittedinteriors.in', false);
		}
	}
	
	static function validateUserSession($userType)
	{
		return (session_status() === PHP_SESSION_ACTIVE && (isset($_SESSION['USER_ID']))) ? TRUE : FALSE;
	}
	
	
	static function startSession($name, $limit = 0, $path = '/', $domain = null, $secure = null)
	{
		// Set the cookie name
		//session_name($name);

		// Set SSL level
		$https = isset($secure) ? $secure : isset($_SERVER['HTTPS']);

		// Set session cookie options
		session_set_cookie_params($limit, $path, $domain, $https, true);
		
		if(!isset($_SESSION)) {
			session_start();
		}
		
		// Make sure the session hasn't expired, and destroy it if it has
		if(self::validateSession()) {
			// Check to see if the session is new or a hijacking attempt
			if(!self::preventHijacking())
			{
				// Reset session data and regenerate id
				$time = $_SERVER['REQUEST_TIME'];
				$_SESSION = array();
				$_SESSION['IPaddress']  = $_SERVER['REMOTE_ADDR'];
				$_SESSION['userAgent']  = $_SERVER['HTTP_USER_AGENT'];
				$_SESSION['OBSOLETE'] = true;
				$_SESSION['EXPIRES'] = time() + 7200;
				$_SESSION['LAST_ACTIVITY'] = $time;
				
				//self::regenerateSession();
				
			// Give a 5% chance of the session id changing on any request
			}else if(rand(1, 100) <= 5){

				self::regenerateSession();
				
			}
		}else{
			$_SESSION = array();
			session_destroy();
			session_start();
		}
	}
   
	static function regenerateSession()
	{
		// If this session is obsolete it means there already is a new id
		if(isset($_SESSION['OBSOLETE']) && $_SESSION['OBSOLETE'] == true)
			return;

		// Set current session to expire in 60 Minuts
		$time = $_SERVER['REQUEST_TIME'];
		$_SESSION['OBSOLETE'] = true;
		$_SESSION['EXPIRES'] = time() + 7200;
        $_SESSION['LAST_ACTIVITY'] = $time;

		// Create new session without destroying the old one
		session_regenerate_id(false);

		// Grab current session ID and close both sessions to allow other scripts to use them
		$newSession = session_id();
		session_write_close();

		// Set session ID to the new one, and start it back up again
		session_id($newSession);
		session_start();

		// Now we unset the obsolete and expiration values for the session we want to keep
		unset($_SESSION['OBSOLETE']);
		unset($_SESSION['EXPIRES']);
	}
	
	static protected function preventHijacking()
	{
		if(!isset($_SESSION['IPaddress']) || !isset($_SESSION['userAgent']))
			return false;

		if ($_SESSION['IPaddress'] != $_SERVER['REMOTE_ADDR'])
			return false;

		if( $_SESSION['userAgent'] != $_SERVER['HTTP_USER_AGENT'])
			return false;

		return true;
	}

	static protected function validateSession()
	{
		if( isset($_SESSION['OBSOLETE']) && !isset($_SESSION['EXPIRES']) && !isset($_SESSION['LAST_ACTIVITY']) )
			return false;

		$time =  $_SERVER['REQUEST_TIME'];
        $timeout_duration = 7200;
        if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration)
            return false;
        
        /*if(isset($_SESSION['EXPIRES']) && $_SESSION['EXPIRES'] < time())
			return false;
        */
        
		return true;
	}
	
	static function destroySession($name){

		// Initialize the session.
		// If you are using session_name("something"), don't forget it now!
		//session_name($name);
		
		if(!isset($_SESSION)) {
			session_start();
		}
		
		// Unset all of the session variables.
		unset($_SESSION);
		$params = [];
		// If it's desired to kill the session, also delete the session cookie.
		// Note: This will destroy the session, and not just the session data!
		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}

		// Finally, destroy the session.
		session_destroy();
		session_start();
	}
}