<?php

Class App
{

	protected $controller 	= 'Home';
	protected $method   	= 'indexAction';
	protected $params   	= [];
	
	public function __construct() {
		
		$url = $this->parseUrl();
		
		$app = 'admin';
		
		if(isset($url[1])){
			$controller = isset($url[0]) ? ucfirst(strtolower(trim($url[0]))) : ucfirst(strtolower(trim($this->controller)));
			$method 	= isset($url[1]) ? $url[1].'Action' : $this->method;
		} else {
			$controller = ucfirst(strtolower(trim($this->controller)));
			$method 	= isset($url[0]) ? $url[0].'Action' : $this->method;
		}
		
		if( file_exists( 'app/'.$app.'/controllers/'.$controller.'.php') ) {
			
			$this->controller = $controller;
			unset($url[0]);
		}
		
		require_once  'app/'.$app.'/controllers/'.$this->controller.'.php';
		
		$this->controller = new $this->controller();
		
		if (@method_exists($this->controller, $method) ) {
			$this->method = $method;
			unset($url[1]);
		} else {
			$this->controller = new Home();
			$this->method = 'errorAction';
			unset($url[1]);
		}
		
		$this->params['requestObj'] = $_REQUEST;
		$this->params['params'] 	= $url ? array_values( $url ) : [];
		
		call_user_func( array($this->controller, $this->method), $this->params );
		
	}

	public function parseUrl() {

		if( isset($_GET['url']) ) {
			//return $url = explode( '/',filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL) );
			return $url = explode( '/',filter_var(rtrim(urldecode($_GET['url']), '/')) );
		}
	}

}