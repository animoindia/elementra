<?php

Class Util {


	public function encryptPassword($password){

		$pre_string = 'p4$$';
		$end_string = 'w0rd';

		$password = $pre_string.$password.$end_string; //register password field

		$options = [
			'cost' => 12,
		];

		return password_hash($password, PASSWORD_BCRYPT, $options);
	}

	public function verifyPasssword($password, $hash){
		$pwd = 'p4$$'.$password.'w0rd';

		return password_verify($pwd, $hash);
	}

	public function isValidEmail($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
	}

    public function encodeData($dataSet) {

        $jsonString 	= json_encode($dataSet, true);

        return base64_encode($jsonString);
    }
    public function decodeData($dataSet) {

        $decryptedString = base64_decode($dataSet);

        return json_decode($decryptedString, true);
    }
	public function arrayKeyPrefix($data) {

        return array_combine(array_map(function($k){ return ':'.$k; }, array_keys($data)), $data);

    }
	public function convertToDateTime($date) {

        return date("Y-m-d h:i:s", strtotime($date));

    }

	// Function to sanitize data to prevent from XSS
	public function sanitizeData($data) {

		$data_keys  = array_keys($data);
		$data  		= array_map('trim', $data);
		$data   	= array_map("stripslashes", $data);
		$data   	= array_map("htmlspecialchars", $data, array_fill(0, sizeof($data), ENT_QUOTES));
		$data   	= array_combine($data_keys , array_values($data));

	   return $data;
	}

	//gets the data from a URL
	public function getTinyUrl($url)  {

		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}

	// Function to Upload Files
	public function uploadFiles($paramsArray = array()){

		@extract($paramsArray);
		$image_root_dir = ROOT_PATH.IMG_UPLOAD_PATH;
		
		if(is_array($directory)){
			foreach($directory as $key=>$val){

				$target_dir = $image_root_dir.'/'.$val;
				if (!is_dir($target_dir)){
					mkdir($target_dir, 0777, true);
				}
				$image_root_dir = $target_dir;
			}
		} else {
			$target_dir = isset($directory) ? $image_root_dir.$directory : $image_root_dir;
			if (!is_dir($target_dir)){
				mkdir($target_dir, 0777, true);
			}
		}

		$source_dir 		= $image_root_dir.'/temp';
		$fileArray 			= array();
		$fileArray['path']  = $target_dir;
		$prefix				= isset($name_prefix) ? $name_prefix : '';

		$_FILES = @$files;
		
		if(is_array($_FILES)){
			if($_FILES['name'] != '' && $_FILES['size'] > 0){
			    if(is_array($_FILES['name'])) {
					foreach($_FILES['name'] as $key=>$val){
						$uploadFile = $target_dir .'/'.$prefix.basename($val);
						move_uploaded_file($_FILES['tmp_name'][$key], $uploadFile);
					}
				} else {
					$uploadFile = $target_dir .'/'. $prefix.basename($_FILES['name']);
					move_uploaded_file($_FILES['tmp_name'], $uploadFile);
				}
			}
		}

		/*if(isset($_FILES['files']) && !empty($_FILES['files']['name'][0]) && is_dir($target_dir)){

			foreach($_FILES["files"]['tmp_name'] as $key=>$value){
				@$fileName = $_FILES["files"]['name'][$key];
				//$is_uloaded = move_uploaded_file($value, $target_dir.'/'.$fileName);
				$is_uloaded = @copy($source_dir.'/'.$_FILES["files"]['name'][$key], $target_dir.'/'.$fileName);

				if($is_uloaded){
					$fileArray['name'][] 		= $fileName;
					$fileArray['act_name'][] 	= $_FILES["files"]['name'][$key];
					$fileArray['type'][] 		= $_FILES["files"]['type'][$key];
					$fileArray['size'][] 		= $_FILES["files"]['size'][$key];
				}

				//array_map('unlink', glob(__DIR__.'/../temp/'.$_FILES["files"]['name'][$key]));
			}
		}*/
		//print_R($fileArray);

		return $fileArray;
	}

	function dateDifference( $myDate ) {

		$date1 = date('Y-m-d H:i:s', strtotime($myDate));
		$date2 = date('Y-m-d H:i:s');

		$diff = abs(strtotime($date2) - strtotime($date1));

		$arrDateDiff = [];
		$arrDateDiff['years']  = floor($diff / (365*60*60*24));
		$arrDateDiff['months'] = floor(($diff - $arrDateDiff['years'] * 365*60*60*24) / (30*60*60*24));
		$arrDateDiff['days']   = floor(($diff - $arrDateDiff['years'] * 365*60*60*24 - $arrDateDiff['months']*30*60*60*24)/ (60*60*24));

		return $arrDateDiff;
	}

	function dateTimezoneConverter($params){
		@extract($params);

		$currentTZ  = date_default_timezone_get();
		$targetTZ	= new DateTimeZone($targetTimeZone);

		$currentDateTime  	= date('d M Y H:i:s', strtotime(@$dateTime));
		$conversionDateTZ 	= new DateTime($currentDateTime, new DateTimeZone($currentTZ));

		return $conversionDateTZ->setTimezone($targetTZ);
	}
	
	function setAjaxResponse($arrParams) {
		
		if(isset($arrParams['error'])){
			header('HTTP/1.1 400 '.$arrParams['error']);
			header('Content-Type: application/json; charset=UTF-8');
			die(json_encode(array('message' => $arrParams['error'], 'code' => 1337)));
		} else {
			header("HTTP/1.1 200 ". $arrParams['successMsg']);
			header('Content-Type: application/json');
			print json_encode($arrParams);
		}
		return;
	}

	public function getPagination($totalRecord, $refPage, $currentPage, $per_page_record = MAX_RECORD_LIMIT){

		$totalPages = ceil($totalRecord / $per_page_record);
		$pagination='<div class="all_pages" style="float: right"><ul class="pagination">';

		if($currentPage > 1){
			$pagination 	.= '<li class="first"><a href="#" data-page="1" ref_page="$refPage" ref_page_no="1" rec_per_page="'.$per_page_record.'" title="First" class="a_pagination">&laquo;</a></li>';
			$pagination 	.= '<li><a href="#" data-page="'.($currentPage-1).'" ref_page="$refPage" ref_page_no="'.($currentPage-1).'" rec_per_page="'.$per_page_record.'" title="Previous" class="a_pagination">&lt;</a></li>';

			for($i = ($currentPage-2); $i < $currentPage; $i++){
				if($i > 0){
					$pagination .= '<li><a href="#" data-page="'.$i.'" ref_page="$refPage" ref_page_no="'.$i.'" rec_per_page="'.$per_page_record.'" title="Page'.$i.'" class="a_pagination">'.$i.'</a></li>';
				}
			}
		}

		$activeClass	= ($currentPage == 1) ? "first"  : ($currentPage == $totalPages) ? "last" : '';
		$pagination .= "<li class='$activeClass active'>".$currentPage.'</li>';

		if($totalPages < MIN_PAGE_LIMIT){
			for($i = $currentPage+1; $i <= $totalPages; $i++){
				$pagination .= '<li><a href="#" data-page="'.$i.'" ref_page_no="'.$i.'" title="Page '.$i.'" rec_per_page="'.$per_page_record.'" class="a_pagination">'.$i.'</a></li>';
			}
		} else {
			for($i = $currentPage+1; $i <= ($currentPage + 2); $i++){
				if($i <= $totalPages){
					$pagination .= '<li><a href="#" data-page="'.$i.'" ref_page_no="'.$i.'" title="Page '.$i.'" rec_per_page="'.$per_page_record.'" class="a_pagination">'.$i.'</a></li>';
				}
			}
		}

		if($currentPage < $totalPages){
			$pagination .= '<li><a href="#" data-page="'.($currentPage+1).'" ref_page="$refPage" ref_page_no="'.($currentPage+1).'" rec_per_page="'.$per_page_record.'" title="Next" class="a_pagination" >&gt;</a></li>'; //next link
			$pagination .= '<li class="last"><a href="#" data-page="'.$totalPages.'" ref_page="$refPage" ref_page_no="'.$totalPages.'" rec_per_page="'.$per_page_record.'" title="Last" class="a_pagination">&raquo;</a></li>'; //last link
		}

		/*for($i = 1; $i <= $totalPages; $i++) {
			$selected = ($i == $currentPage) ? 'page_selected' : '';
			$pagination .= "<i class='$selected'><a href='#' class='a_pagination' ref_page='$refPage' ref_page_no='$i'>".$i."</a></i>";
		}*/

		$pagination .= '</ul></div>';

		return $pagination;
	}

	public function sendEmail($arrParams){

		@extract($arrParams);

		$mail	= new PHPMailer; // call the class
		$mail->IsSMTP();
		$mail->Host 	= SMTP_HOST; //Hostname of the mail server
		$mail->Port		= SMTP_PORT; //Port of the SMTP like to be 25, 80, 465 or 587
		$mail->SMTPAuth = true; //Whether to use SMTP authentication
		$mail->SMTPAutoTLS = false;
        $mail->SMTPSecure = false;
        //$mail->SMTPDebug = 2;
		$mail->Username = SMTP_USER; //Username for SMTP authentication any valid email created in your domain
		$mail->Password = SMTP_PWD; //Password for SMTP authentication

		$mail->Priority = isset($priority) ? $priority : 1;// Set Email Priority

		$mail->SetFrom($emailFrom); //From address of the mail

		$mail->Subject  = @$subject;

		$emailBCC		= EMAIL_BCC;

		//To address who will receive this email
		if(isset($emailTo) && is_array($emailTo)){
			foreach($emailTo as $key=>$val){
				$mail->AddAddress($val); //To address who will receive this email
			}
		} else {
			$mail->AddAddress($emailTo); //To address who will receive this email
		}

		//CC address
		if(isset($emailCC)){
			if(is_array($emailCC)) {
				foreach($emailCC as $key=>$val){
					$mail->AddCC($val); //CC address who will receive this email
				}
			} else {
				$mail->AddCC($emailCC); //CC address who will receive this email
			}

		}

		//BCC address
		if(isset($emailBCC)){
			if(is_array($emailBCC)) {
				foreach($emailBCC as $key=>$val){
					$mail->AddBCC($val); //BCC address who will receive this email
				}
			} else {
				$mail->AddAddress($emailBCC); //To address who will receive this email
			}
		}

		$mail->MsgHTML($body); //Put your body of the message you can place html code here

		/*if(isset($files['name'])){
			foreach($files['name'] as $key=>$value){
				$mail->AddAttachment($files['path'].'/'.$value, $value, 'base64', $files['type'][$key]); //Attach a file here if any or comment this line,
			}
		}*/

		ob_end_clean();
		
		//print_R($mail);
		
		//var_dump($mail->Send());
		
		//exit;

		return $mail->Send(); //Send the mails
	}

}