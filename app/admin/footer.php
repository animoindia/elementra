		<footer class="<?php echo $cls_footer;?>">
			<strong>AKFI FITTED INTERIORS</strong>
			All Right Reserved. &nbsp;  | &nbsp; 
			Designed & Developed By: <a target="_blank" href="http://e-techsolutions.co.in">E-Tech Solutions</a>
		</footer>
	</div>
	<!-- /. PAGE INNER  -->
		<!-- /. PAGE INNER  -->
        
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script type="text/javascript" src="./../assets/js/jquery/jquery-1.10.2.js"></script>
		
		<!-- Bootstrap Js -->
		<script type="text/javascript" src="./../assets/js/bootstrap/bootstrap.min.js"></script>
		<!-- Bootstrap Datetimepicker Js -->
		<script type="text/javascript" src="./../assets/js/bootstrap/moment.min.js"></script>
		<script type="text/javascript" src="./../assets/js/bootstrap/bootstrap-datetimepicker.min.js"></script>
		<!-- Bootstrap easypiechart Js -->
		<script type="text/javascript" src="./../assets/js/easypiechart/easypiechart.js"></script>
		<script type="text/javascript" src="./../assets/js/easypiechart/easypiechart-data.js"></script>

		<!-- jQuery Validate Js -->
		<script type="text/javascript" src="./../assets/js/jquery/jquery.validate.min.js"></script>
		<!-- Metis Menu Js -->
		<script type="text/javascript" src="./../assets/js/jquery/jquery.metisMenu.js"></script>
		<!-- DATA TABLE SCRIPTS -->
		<script type="text/javascript" src="./../assets/js/dataTables/jquery.dataTables.js"></script>
		<script type="text/javascript" src="./../assets/js/dataTables/dataTables.bootstrap.js"></script>
				
		<!-- Star Ratings Js -->
		<script type="text/javascript" src="./../assets/js/star-ratings.js"></script>
		<script type="text/javascript" src="./../assets/js/steps_jquery.backstretch.min.js"></script>
		<script type="text/javascript" src="./../assets/js/steps_form.js"></script>
		
		<!-- FEEDBACK Star Ratings JS -->
		<script type="text/javascript" src="./../assets/js/star-ratings-feedback.js"></script>
		
		<!-- Dropdown Hover Js -->
		<script type="text/javascript" src="./../assets/js/hoverintent.min.js"></script>
		<script type="text/javascript" src="./../assets/js/dropdown_hover.js"></script>
		
		<!-- Custom Js -->
		<script src="./../assets/js/lib/main.js"></script>
		
		<?php 
		if(file_exists(ROOT_PATH .'assets/js/lib/'. strtolower(MODULE) .'.js')) { ?>
			<script src="./../assets/js/lib/<?= MODULE; ?>.js"></script>
		<?php } ?>
		
		<script>
			$(document).ready(function () {
				$('#dataTables-example').dataTable();
			});
		</script>
	</body>
</html>