<?php 
@extract($data); 
@extract($data['result']);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="myModalLabel">Change Password</h4>
</div>
<form role="form"  name="frm_changepwd" id="frm_changepwd" method="post" action="#">
	<div class="modal-body">
		<div class="row">
			<div class="form-group col-xs-12 pull-left">
				<label for="exampleInputEmail1">New Password</label>
				<input type="password" class="form-control reset_pwd" id="new_Password" placeholder="New Password" name="data[password]" required />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12 pull-left">
				<label for="exampleInputEmail1">Confirm Password</label>
				<input type="password" class="form-control reset_cnf_pwd" id="confirm_Password" placeholder="Confirm Password" name="cnf_password" required />
			</div>
		</div>
		<div class='response' style="display:none;"></div>
	</div>
	
	<div class="modal-footer">
		<div class="row">
			<div class="form-group col-sm-12 col-xs-12 pull-left">
				<button type="button" class="loginBtn btn_submit btn_submit_changepwd">Save changes</button>
				<button type="button" class="cancel-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
			</div>
		</div>
	</div>
</form>
<script src="./../assets/js/lib/main.js"></script>
<script src="./../assets/js/lib/user.js"></script>