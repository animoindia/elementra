<?php
	@extract($data);
	@extract($data['result']);
?>
<!-- Breadcrumbs-->
<div class="header">
	<h1 class="page-header">User</h1>
	<ol class="breadcrumb">
		<li><a href="\">Dashboard</a></li>
		<li><a href="\user/profile?id=<?= $_SESSION['USER_ID']?>">Profile</a></li>
		<li class="active">Edit</li>
	</ol>
</div>
<div id="page-inner">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="card-title">
						<div class="title">Profile</div>
					</div>
				</div>
				<div class="panel-body">
					<?php if(isset($data['error'])) { ?>
						<div class='col-xs-12 error'><?= $data['error'];?></div>
					<?php } ?>
					<form name="frm_user" id="frm_user" method="post" action="\user/save" enctype="multipart/form-data">
						<div class="col-xs-12">
							<!-- Profile Picture Wrapper Code Starts Here -->

							<div class="col-sm-3 col-xs-12 profil-pic-wrapper">

								<div class="row">
									<div class="small-12 medium-2 large-2 columns">
										<div class="circle">
											<!-- User Profile Image -->
											<?php if(isset($avtar) && $avtar != '') { ?>
												<img class="profile-pic" src="<?= IMAGE_VIEW_PATH;?>users/<?= $id;?>_<?= $avtar;?>">
											<?php } else { ?>
												<img class="profile-pic" src="<?= IMG_PATH;?>/defaultUser.jpg">
											<?php } ?>

											<!-- Default Image -->
											<i class="fa fa-user fa-5x"></i>
										</div>
										<div class="p-image">
											<i class="fa fa-camera upload-button"></i>
											<input class="file-upload" type="file" accept="image/*" name="image[]"/>
										</div>
									</div>
								</div>
							</div>
							<!-- Profile Picture Wrapper Code Ends Here -->

							<div class="col-sm-9 col-xs-12 profil-fields-wrapper">

								<div class="row">
									<div class="form-group col-xs-6 pull-left">
										<label for="exampleInputEmail1">Company</label>
										<input type='text' class='form-control' value='<?= @$company;?>' disabled readOnly />
										<input type='hidden' class='form-control' value='<?= @$company_id;?>' name="data[company_id]" readOnly />
									</div>
								</div>	
								<div class="row">
									<div class="form-group col-sm-6 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Department</label>
										<input type='text' class='form-control' value='<?= @$department;?>' disabled readOnly />
										<input type='hidden' class='form-control' value='<?= @$department_id;?>' name="data[department_id]" readOnly />
									</div>
									<div class="form-group col-sm-6 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Designation</label>
										<input type='text' class='form-control' value='<?= @$designation;?>' disabled readOnly />
										<input type='hidden' class='form-control' value='<?= @$designation_id;?>' name="data[designation_id]" readOnly />
									</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-6 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Date Of Birth</label>
										<div class='input-group date' id='datetimepicker1'>
											<input type='text' class="form-control" name="data[dob]" placeholder="Date Of Birth" value="<?= @$dob;?>" required />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
									<div class="form-group col-sm-6 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Gender</label>
										<select class="form-control" id="data[gender]" name="data[gender]" required>
											<option value="">Gender</company>
											<option value="male" <?= (@strtolower(trim($gender)) == 'male') ? 'selected' : '';?>>Male</option>
											<option value="female" <?= (@strtolower(trim($gender)) == 'female') ? 'selected' : '';?>>Female</option>
										</select>
									</div>
								</div>
								<div class='row'>
									<div class="form-group col-sm-2 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Title</label>
										<select class="form-control" id="data[title]" name="data[title]" required>
											<option value="">Title</company>
											<option value="MR" <?= (@strtolower(trim($title)) == 'mr') ? 'selected' : '';?>>MR.</option>
											<option value="MS" <?= (@strtolower(trim($title)) == 'ms') ? 'selected' : '';?>>MS.</option>
											<option value="MRS" <?= (@strtolower(trim($title)) == 'mrs') ? 'selected' : '';?>>MRS.</option>
										</select>
									</div>
									<div class="form-group col-sm-4 col-xs-12 pull-left">
										<label for="exampleInputEmail1">First Name</label>
										<input type="text" class="form-control" id="data[f_name]" placeholder="First Name" name="data[f_name]" value="<?= @$f_name;?>" required />
									</div>
									<div class="form-group col-sm-3 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Middle Name</label>
										<input type="text" class="form-control" id="data[m_name]" placeholder="Middle Name" name="data[m_name]" value="<?= @$m_name;?>" required />
									</div>
									<div class="form-group col-sm-3 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Last Name</label>
										<input type="text" class="form-control" id="data[l_name]" placeholder="Last Name" name="data[l_name]" value="<?= @$l_name;?>" required/>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-6 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Contact No</label>
										<input type="text" class="form-control" id="data[mobile_no]" placeholder="Mobile No." name="data[mobile_no]" value="<?= @$mobile_no;?>" required/>
									</div>
									<div class="form-group col-sm-6 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Email</label>
										<input type="email" class="form-control" id="data[email]" placeholder="Email" name="data[email]" value="<?= @$email;?>" required/>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-sm-6 col-xs-12 pull-left">
										<label for="exampleInputEmail1">Pincode</label>
										<input type="text" class="form-control" id="data[pincode]" placeholder="pincode" name="data[pincode]" value="<?= @$pincode;?>" required/>
									</div>
									<div class="form-group col-sm-6 col-xs-12 pull-left">
										<label for="exampleInputEmail1">City</label>
										<input type="text" class="form-control" id="data[city]" placeholder="City" name="data[city]" value="<?= @$city;?>" required/>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-xs-12 pull-left">
										<label for="exampleInputEmail1">Address</label>
										<textarea class="form-control" rows="3" id="address" placeholder="Enter Address" name="data[address]" required><?= @$address;?></textarea>
									</div>
								</div>
								
								<input type="hidden" class="form-control" id="action" name="action" value="<?= @$action;?>" readonly />
								<input type="hidden" class="form-control" id="id" name="id" value="<?= @$id;?>" readonly />
								<input type="hidden" class="form-control" id="profile" name="profile" value="profile" readonly />
								

								<div class="row">
									<div class="form-group col-sm-12 col-xs-12 pull-left">
										<button type="submit" class="loginBtn">Save changes</button>
										<button type="button" class="cancel-btn" data-dismiss="">Cancel</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>