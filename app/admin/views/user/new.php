<?php
	@extract($data);
	@extract($data['result']);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="myModalLabel"><?= @($action == 'edit') ? @$action.' '.$f_name : ' New User'; ?> </h4>
</div>

<form role="form" name="frm_user" id="frm_user" action='#' method="post" class="frm_user" enctype="multipart/form-data">
	<div class="modal-body">
		<div class="row">
			<div class="form-group col-xs-12 pull-left">
				<!-- Profile Picture Wrapper Code Starts Here -->
				<div class="col-xs-12 profil-pic-wrapper modal-popup">
					<div class="row">
						<div class="small-12 medium-2 large-2 columns">
							<div class="circle">
								<!-- User Profile Image -->
								<?php if(isset($avtar) && $avtar != '') { ?>
									<img class="profile-pic" src="<?= IMAGE_VIEW_PATH;?>users/<?= $id;?>_<?= $avtar;?>">
								<?php } else { ?>
									<img class="profile-pic" src="<?= IMG_PATH;?>defaultUser.jpg">
								<?php } ?>

								<!-- Default Image -->
								<i class="fa fa-user fa-5x"></i>
							</div>
							<div class="p-image">
								<i class="fa fa-camera upload-button"></i>
								<input class="file-upload" type="file" accept="image/*" name="image"/>
							</div>
						</div>
					</div>
				</div>
				<!-- Profile Picture Wrapper Code Ends Here -->'
				<div class="col-xs-12 profil-fields-wrapper modal-popup">
					<div class="row">
						<div class="form-group col-xs-12 pull-left">
							<label for="exampleInputEmail1">Company</label>
							<?php $readOnly = ($_SESSION['USER_TYPE'] == 1) ? '' : 'readOnly disabled'; ?>
							<?php if($_SESSION['USER_TYPE'] == 1) { ?>
								<select class="form-control" id="data[company_id]" name="data[company_id]" required >
									<option value=""> Company </option>
									<?php foreach($data['arrCompany']['result'] as $key => $val) { ?>
										<option value= <?= $val['id'];?> <?= (@$company_id == $val['id'] || $_SESSION['COMPANY_ID'] == $val['id']) ? 'selected' : '';?>><?= $val['name'];?></option>
									<?php } ?>
								</select>
							<?php } else { ?>
								<input type='text' class='form-control' value='<?= @isset($company) ? $company : $_SESSION['COMPANY_NAME'];?>' disabled readOnly />
								<input type='hidden' class='form-control' value='<?= @isset($company_id) ? $company_id : $_SESSION['COMPANY_ID'];?>' name="data[company_id]" readOnly />
							<?php } ?>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-8 col-xs-12 pull-left">
							<label for="exampleInputEmail1">Designation</label>
							<?php if($_SESSION['USER_TYPE'] == 1) { ?>
								<select class="form-control" id="data[designation_id]" name="data[designation_id]" required>
									<option value="">Designation</option>
									<option value="1" <?= (@$designation_id == '1') ? 'selected' : '';?>>Administrator</option>
									<option value="2" <?= (@$designation_id == '2') ? 'selected' : '';?>>Accountant</option>
									<option value="3" <?= (@$designation_id == '3') ? 'selected' : '';?>>Site Supervisor</option>
								</select>
							<?php } else if($action == 'edit' && $_SESSION['USER_TYPE'] == 2 && $_SESSION['USER_ID'] == @$id) {?>
								<select class="form-control" id="designation" name="designation" disabled>
									<option value="">Designation</option>
									<option value="1" <?= (@$designation_id == '1') ? 'selected' : '';?>>Administrator</option>
								</select>
								<input type='hidden' class="form-control" id="data[designation_id]" name="data[designation_id]" value="1" readOnly />
							<?php } else { ?>
								<select class="form-control" id="designation" name="designation" disabled>
									<option value="4" <?= (@$designation_id == '4') ? 'selected' : '';?>>Designer</option>
								</select>
								<input type='hidden' class="form-control" id="data[designation_id]" name="data[designation_id]" value="4" readOnly />
							<?php } ?>
						</div>
						<div class="form-group col-sm-4 col-xs-12 pull-left">
							<label for="exampleInputEmail1">Title</label>
							<select class="form-control" id="data[title]" name="data[title]" required>
								<option value="">Title</option>
								<option value="MR" <?= (@strtolower(trim($title)) == 'mr') ? 'selected' : '';?>>MR.</option>
								<option value="MS" <?= (@strtolower(trim($title)) == 'ms') ? 'selected' : '';?>>MS.</option>
								<option value="MRS" <?= (@strtolower(trim($title)) == 'mrs') ? 'selected' : '';?>>MRS.</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-6 col-xs-12 pull-left">
							<label for="exampleInputEmail1">First Name</label>
							<input type="text" class="form-control" id="data[f_name]" placeholder="First Name" name="data[f_name]" value="<?= @$f_name;?>" required />
						</div>
						<div class="form-group col-sm-6 col-xs-12 pull-left">
							<label for="exampleInputEmail1">Last Name</label>
							<input type="text" class="form-control" id="data[l_name]" placeholder="Last Name" name="data[l_name]" value="<?= @$l_name;?>" required/>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 pull-left">
							<label for="exampleInputEmail1">Contact No</label>
							<input type="text" class="form-control" id="data[mobile_no]" placeholder="Mobile No." name="data[mobile_no]" value="<?= @$mobile_no;?>" required/>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 pull-left">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" class="form-control" id="data[email]" placeholder="Email" name="data[email]" value="<?= @$email;?>" required/>
						</div>
					</div>
				</div>
			</div>	
		</div>
		<div class='response' style="display:none;"></div>
	</div>
	
	<div class="modal-footer">
		<div class="row">
			<input type="hidden" class="form-control" id="action" name="action" value="<?= @$action;?>" readonly />
			<input type="hidden" class="form-control" id="id" name="id" value="<?= @$id;?>" readonly />
			<input type="hidden" class="form-control" id="flag" name="flag" value="<?= @$flag;?>" readonly />
			<div class="form-group col-sm-12 col-xs-12 pull-left">
				<button type="submit" class="loginBtn btn_submit btn_submit_user">Save changes</button>
				<button type="button" class="cancel-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
			</div>
		</div>
	</div>
</form>
<script src="./../assets/js/lib/main.js"></script>
<script src="./../assets/js/lib/user.js"></script>
