<?php

	@extract($data['data']);
	$cnt_user = (isset($result)) ? count($result) : 'No ';
?>
<div class="header">
	<h1 class="page-header">User</h1>
	<div class="create-project-wrapper">
		<a href="\user/new" class="" data-toggle="modal" data-target="#myModal" onclick="$('.div_loading_image').show();">Add USER</a>
	</div>
	<ol class="breadcrumb">
		<li><a href="\" onclick="$('.div_loading_image').show();">Home</a></li>
		<li><a href="\user/list" onclick="$('.div_loading_image').show();">Users</a></li>
		<li class="active">Data</li>
	</ol>
</div>
<div id="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			   <div class="panel-heading">
					Found <?= $cnt_user; ?> User
				</div>

				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Company</th>
									<th>Designation</th>
									<th>Contact No</th>
									<th>Email</th>
									<th>Registration Date</th>
									<th>Activation Date</th>
									<th>Is Active</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
								if(isset($result)) {
									foreach($result as $key => $value) {
										@extract($value);
										$row_class = ($id%2 == 0) ? 'odd' : 'even';
										$active = @($is_active == 1) ? "Active" : "In";
							?>
										<tr class="<?= $row_class;?> ">
											<td><?= @++$key;;?></td>
											<td><?= @ucwords(strtolower($title.' '.trim($f_name).' '.trim($m_name).' '.trim($l_name)));?></td>
											<td><?= @ucwords(strtolower($company));?></td>
											<td><?= @ucwords(strtolower($designation));?></td>
											<td><?= @$mobile_no;?></td>
											<td><?= @$email;?></td>
											<td><?= @date('d-m-Y H:i:s', strtotime($created_at));?></td>
											<td><?= @($profile_activated_date != '') ? date('d-m-Y H:i:s', strtotime($profile_activated_date)) : '';?></td>
											<td>
												<?php if($is_active == 1) { ?>
													<i class="fa fa-check" aria-hidden="true" style="color:green;" title="Active"></i>
												<?php } else { ?>
														<i class="fa fa-times" aria-hidden="true" style="color:red;" title="In-active"></i>
												<?php } ?>
											</td>
											<td>
												<a class="" href="\user/edit?user=<?= $id;?>" data-toggle="modal" data-target="#myModal" onclick="$('.div_loading_image').show();">
													<i class="fa fa-pencil-square-o info" aria-hidden="true" title="Edit Company"></i>
												</a>
												&nbsp;|&nbsp;
												<?php if($is_active == 1) { ?>
													<a href="\user/active?id=<?= $id;?>&flag=0" onclick="return confirm('Are you sure, you want to disable?')">
														<i class="fa fa-times" aria-hidden="true" style="color:red;" title="Keep Disable"></i>
													</a>
												<?php } else { ?>
													<a href="\user/active?id=<?= $id;?>&flag=1" onclick="return confirm('Are you sure, you want to enable?')">
														<i class="fa fa-check" aria-hidden="true" style="color:green;" title="Keep Enable"></i>
													</a>
												<?php } ?>
											</td>
										</tr>
							<?php
									}
								}
							?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
			<!--  end  Context Classes  -->
		</div>
	</div>
	<!-- /. ROW  -->
</div>