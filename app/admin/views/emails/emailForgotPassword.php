<?php @extract($data['data']); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<!--[if gte mso 9]><xml>
<o:OfficeDocumentSettings>
<o:AllowPNG/>
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml><![endif]-->
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="date=no" />
<meta name="format-detection" content="address=no" />
<meta name="format-detection" content="telephone=no" />
<title> :: WELCOME TO AK Fitted Interiors ::  </title>

<style type="text/css" media="screen">
/* Linked Styles */
body { padding:0 !important; margin:0 !important; display:block !important; background:#1e1e1e; -webkit-text-size-adjust:none }
a { color:#a88123; text-decoration:none }
p { padding:0 !important; margin:0 !important } 

/* Mobile styles */
</style>
<style media="only screen and (max-device-width: 480px), only screen and (max-width: 480px)" type="text/css">
@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) { 
	div[class='mobile-br-5'] { height: 5px !important; }
	div[class='mobile-br-10'] { height: 10px !important; }
	div[class='mobile-br-15'] { height: 15px !important; }
	div[class='mobile-br-20'] { height: 20px !important; }
	div[class='mobile-br-25'] { height: 25px !important; }
	div[class='mobile-br-30'] { height: 30px !important; }

	th[class='m-td'], 
	td[class='m-td'], 
	div[class='hide-for-mobile'], 
	span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

	span[class='mobile-block'] { display: block !important; }

	div[class='wgmail'] img { min-width: 320px !important; width: 320px !important; }

	div[class='img-m-center'] { text-align: center !important; }

	div[class='fluid-img'] img,
	td[class='fluid-img'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }

	table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }
	td[class='td'] { width: 100% !important; min-width: 100% !important; }

	table[class='center'] { margin: 0 auto; }

	td[class='column-top'],
	th[class='column-top'],
	td[class='column'],
	th[class='column'] { float: left !important; width: 100% !important; display: block !important; }

	td[class='content-spacing'] { width: 15px !important; }

	div[class='h2'] { font-size: 44px !important; line-height: 48px !important; }
} 
</style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; background:#1e1e1e; -webkit-text-size-adjust:none">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background: #1e1e1e url(images/modular-kitchens2.jpg) no-repeat 0 0; background-size: cover; ">
		<tr>
			<td align="center" valign="top">
				<!-- Top -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#161616">
					<tr>
						<td align="center" valign="top">
							<table width="600" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tr>
									<td class="td" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; width:600px; min-width:600px; Margin:0" width="600">

									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END Top -->

				<table width="600" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
					<tr>
						<td class="td" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; width:600px; min-width:600px; Margin:0" width="600">
							<!-- Header -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

										<div class="img-center" style="font-size:15px; color: #fff; text-align:center"><a style="color:#fff; font-family:Arial, sans-serif; min-width:auto !important; font-size:30px;  line-height:22px; font-weight: normal; text-align:center; text-transform: uppercase;" href="index.html" target="_blank"><strong style="font-weight: 700;">AK</strong> Fitted Interiors</a></div>
										<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


									</td>
									<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
								</tr>
							</table>
							<!-- END Header -->

							<!-- Main -->
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<!-- Head -->

										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#765f4f">

											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="27"><img src="<?= @HOST;?>/assets/img/top_left.jpg" border="0" width="27" height="27" alt="" /></td>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>

																		<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" height="3" bgcolor="#765f4f">&nbsp;</td>

																	</tr>
																</table>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="24" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

															</td>
															<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="27"><img src="<?= @HOST;?>/assets/img/top_right.jpg" border="0" width="27" height="27" alt="" /></td>
														</tr>
													</table>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>

															<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="3" bgcolor="#765f4f"></td>

															<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="10"></td>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

																<div class="h3-2-center" style="color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:20px; line-height:26px; text-align:center; text-transform: uppercase; letter-spacing:5px">JUST ONE STEP AWAY</div>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="5" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


																<div class="h2" style="color:#ffffff; font-family:Georgia, serif; min-width:auto !important; font-size:36px; line-height:64px; text-align:center">
																	<em>Almost done!</em>
																</div>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="35" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

															</td>
															<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="10"></td>

															<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="3" bgcolor="#765f4f"></td>

														</tr>
													</table>
												</td>
											</tr>
										</table>
										<!-- END Head -->

										<!-- Body -->
										<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
															<td>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="35" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
																
<div class="h4-center" style="color: #1e1e1e; font-family: Helvetica,Arial,sans-serif; min-width: auto!important; font-size: 14px; line-height: 18px; text-align: center; margin-bottom: 15px;">Oh! You have forgotten your password..<br/>
Please notedown your temporary system generated password: <strong><?= @$password;?></strong>
																</div>
																<div class="h4-center" style="color: #1e1e1e; font-family: Helvetica,Arial,sans-serif; min-width: auto!important; font-size: 14px; line-height: 18px; text-align: center; margin-bottom: 15px;">
There's <strong>one last</strong> step until your account is <strong>100%</strong> ready.<br /> We need you to <strong>reset</strong> your password to access your account.</strong><br/> 
																</div>
																<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="40" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


																<!-- Button -->
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td align="center">
																			<table width="210" border="0" cellspacing="0" cellpadding="0" style="margin-left: 33px;">
																				<tr>

																					<td align="center" bgcolor="#765f4f">

																						<table border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="15"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="50" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
																							</td>

																							<td bgcolor="#765f4f">

																								<div class="text-btn" style="color:#ffffff; font-family:Arial, sans-serif; min-width:auto !important; font-size:15px; line-height:20px; text-align:center">
																									<a href="<?= @$activation_url;?>" target="_blank" class="link-white" style="color:#ffffff; text-decoration:none"><span class="link-white" style="color:#ffffff; text-decoration:none;font-weight: bold;">RESET PASSWORD</span></a>
																								</div>
																							</td>
																							<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="15"></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!-- END Button -->
															<!-- <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="40" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table> -->

														</td>
														<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>

													</tr>
												</table>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
														<td>
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


															<div class="text-2" style="color: #1e1e1e; font-family: Georgia,serif;  min-width: auto!important; font-size: 16px; line-height: 26px; text-align: center;">
																<em>If you did not forgot your password you can safely ignore this mail.
														</em>
															</div>
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="35" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

														</td>
														<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END Body -->

									<!-- Foot -->

									<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#765f4f">

										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>

														<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="3" bgcolor="#765f4f"></td>

														<td>
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

															<div class="h3-1-center" style="color:#1e1e1e; font-family:Georgia, serif; min-width:auto !important; font-size:20px; line-height:26px; text-align:center">
																<em>Follow Us</em>
															</div>
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


															<!-- Socials -->
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td align="center">
																		<table border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td class="img-center" style="font-size:0pt; line-height:0pt; text-align:center" width="38"><a href="#" target="_blank"><img src="<?= @HOST;?>/assets/img/ico_facebook.jpg" border="0" width="28" height="28" alt="" /></a></td>
																				<td class="img-center" style="font-size:0pt; line-height:0pt; text-align:center" width="38"><a href="#" target="_blank"><img src="<?= @HOST;?>/assets/img/ico_twitter.jpg" border="0" width="28" height="28" alt="" /></a></td>
																				<td class="img-center" style="font-size:0pt; line-height:0pt; text-align:center" width="38"><a href="#" target="_blank"><img src="<?= @HOST;?>/assets/img/ico_instagram.jpg" border="0" width="28" height="28" alt="" /></a></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<!-- END Socials -->
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="15" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

														</td>

														<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="3" bgcolor="#765f4f"></td>

													</tr>
												</table>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="27"><img src="<?= @HOST;?>/assets/img/bot_left.jpg" border="0" width="27" height="27" alt="" /></td>
														<td>
															<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="24" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>

																	<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" height="3" bgcolor="#765f4f">&nbsp;</td>

																</tr>
															</table>
														</td>
														<td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="27"><img src="<?= @HOST;?>/assets/img/bot_right.jpg" border="0" width="27" height="27" alt="" /></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<!-- END Foot -->
								</td>
							</tr>
						</table>
						<!-- END Main -->

						<!-- Footer -->
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

									<div class="m_-5121935551911200081m_6019193585529817646text-footer" style="color:#666666;font-family:Arial,sans-serif;min-width:auto!important;font-size:12px;line-height:18px;text-align:center">
											27/D, Laxmi Industrial Estate,<span class="m_-5121935551911200081m_6019193585529817646mobile-block"></span> New Link Road,<span class="m_-5121935551911200081m_6019193585529817646mobile-block"></span>  Andheri (West),<span class="m_-5121935551911200081m_6019193585529817646mobile-block"></span>
											Mumbai - 400 053.<span class="m_-5121935551911200081m_6019193585529817646mobile-block"></span> 
											Maharashtra, India
											<br>
											<a href="<?= @HOST;?>" class="m_-5121935551911200081m_6019193585529817646link-1" style="color:#666666;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?q=<?= @HOST;?>&amp;source=gmail&amp;ust=1545559333512000&amp;usg=AFQjCNGYupechjdy6RSZk3FhBCDwNHxMAA"><span class="m_-5121935551911200081m_6019193585529817646link-1" style="color:#666666;text-decoration:none">www.akfittedinteriors.in</span></a>
											<span class="m_-5121935551911200081m_6019193585529817646mobile-block"><span class="m_-5121935551911200081m_6019193585529817646hide-for-mobile">|</span></span>
											<a href="mailto:enquiry@akfittedinteriors.in" class="m_-5121935551911200081m_6019193585529817646link-1" style="color:#666666;text-decoration:none" target="_blank"><span class="m_-5121935551911200081m_6019193585529817646link-1" style="color:#666666;text-decoration:none">enquiry@akfittedinteriors.in</span></a>
											<span class="m_-5121935551911200081m_6019193585529817646mobile-block"><span class="m_-5121935551911200081m_6019193585529817646hide-for-mobile">|</span></span>
											Phone: <a href="tel:+9664873310" class="m_-5121935551911200081m_6019193585529817646link-1" style="color:#666666;text-decoration:none" target="_blank"><span class="m_-5121935551911200081m_6019193585529817646link-1" style="color:#666666;text-decoration:none">+91 80807 87815</span></a>
										</div>
									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

								</td>
								<td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
							</tr>
						</table>
						<!-- END Footer -->
					</td>
				</tr>
			</table>
			<div class="wgmail" style="font-size:0pt; line-height:0pt; text-align:center"><img src="https://d1pgqke3goo8l6.cloudfront.net/oD2XPM6QQiajFKLdePkw_gmail_fix.gif" width="600" height="1" style="min-width:600px" alt="" border="0" /></div>
		</td>
	</tr>
</table>
</body>
</html>
