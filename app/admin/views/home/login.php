<?php @extract($data);?>

<!-- MAIN NEW LOGIN SCREEN CODE STARTS HERE -->

<div class="col-xs-12 login_landing_screen">
	<!-- LEFT SIDE WRAPPER CODE STARTS HERE -->
	<div class="col-lg-3 col-md-4 col-sm-3 col-xs-12 login_left_wrapper">
	</div>
	<!-- LEFT SIDE WRAPPER CODE ENDS HERE -->

	<!-- RIGHT SIDE WRAPPER CODE STARTS HERE -->
	<div class="col-lg-9 col-md-8 col-sm-9 col-xs-12 login_right_wrapper">
		<!-- LOGO CIRCLE CODE STARTS HERE -->
		<div class="login_circle_wrapper">
			<!-- <span>AK FITTED INTERIORS</span> -->
			<img class="img-responsive logo-img" border="0" src="../../assets/img/login-logo-img.png" alt="AKFI" title="AKFI" />
		</div>
		<!-- LOGO CIRCLE CODE ENDS HERE -->

		<!-- LOGIN FORM FIELDS CODE STARTS HERE -->
		<div class="login_form_wrapper div_login">
			<form role="form" id='frm_admin_login' name="frm_admin_login" method='post' action='\home/validateLogin'>
				<!-- FORM FIELDS CODE STARTS HERE -->
				<div class="col-md-9 col-sm-8 col-xs-9 login-input-filed-wrapper">
					<div class="form-group input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="Enter Username" value="<?= @$username;?>" required />
					</div>
					<div class="form-group input-group last">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="password" class="form-control" id="password" name="password"  placeholder="Your Password" value="<?= @$password;?>" required />
					</div>
					<div class="col-xs-12 form-group remember_wrapper">
						<a href="#" class="a_forgot_pwd badge">
							Forgot Password 
						</a>
					</div>
					<?php if(isset($error)) { ?>
						<div class="col-xs-12 error_msg"><?= $error; ?></div>
					<?php } ?>
				</div>

				<!-- FORM FIELDS CODE ENDS HERE -->
				<!-- FORM FIELDS SUBMIT BUTTON CODE STARTS HERE -->
				<div class="col-md-3 col-sm-4 col-xs-3 form-group login-input-btn-wrapper">
					<button class="login_submit_btn" type="submit" value="Login Now" onclick="$('.div_loading_image').hide();"> <span class="only-small-devices">LogIn</span> <i class="fa fa-angle-right"></i></button>
				</div>
				
				<!-- FORM FIELDS SUBMIT BUTTON CODE ENDS HERE -->
			</form>
		</div>
		<!-- LOGIN FORM FIELDS CODE ENDS HERE -->
		
		<!-- Forgot PASSWORD FORM FIELDS CODE STARTS HERE -->
		<div class="login_form_wrapper div_forgot_pwd" style="display:none">
			<form role="form" id='frm_forgot_pwd' name="frm_forgot_pwd" method='post' action='#'>
				<!-- FORM FIELDS CODE STARTS HERE -->
				<div class="col-md-9 col-sm-8 col-xs-9 login-input-filed-wrapper">
					<div class="form-group input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" class="form-control" name="f_username" aria-describedby="emailHelp" placeholder="Enter Username" value="<?= @$username;?>" required />
					</div>
					<div class="col-xs-12 form-group remember_wrapper">
						<div class="col-xs-12 response" style='display:none'></div>
						<div class="col-xs-12 go_back"><a href="#" class="a_forgot_pwd badge go_back">Go Back </a></div>
					</div>
				</div>

				<!-- FORM FIELDS CODE ENDS HERE -->
				<!-- FORM FIELDS SUBMIT BUTTON CODE STARTS HERE -->
				<div class="col-md-3 col-sm-4 col-xs-3 form-group login-input-btn-wrapper">
					<button class="login_submit_btn" type="submit" value="Submit Now" > <span class="only-small-devices">Submit</span> <i class="fa fa-angle-right"></i></button>
				</div>
				
				<!-- FORM FIELDS SUBMIT BUTTON CODE ENDS HERE -->
			</form>
		</div>
		<!-- Forgot PASSWORD FORM FIELDS CODE ENDS HERE -->
	</div>
	<!-- RIGHT SIDE WRAPPER CODE ENDS HERE -->
</div>
<!-- MAIN NEW LOGIN SCREEN CODE ENDS HERE -->