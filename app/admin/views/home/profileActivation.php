<!-- MAIN PROFILE PAGE CODE STARTS HERE -->
<?php @extract($data['data']); ?>
<!-- MAIN DIV STARTS HERE -->
<div class="row">
	<div class="col-xs-12 activation-header-wrapper">
		<div class="container">
			<img class="img-responsive logo-img" border="0" src="../../assets/img/login-logo-img.png" alt="AKFI" title="AKFI" />
		</div>
	</div>
	<div class="col-xs-12 activation-content-wrapper">
	  <div class="container">
		  <div class="col-md-6 col-md-offset-3 col-sm-7 col-sm-offset-3 col-xs-12">
			<?php if(isset($error)) { ?>
				<strong class="col-xs-12 error-activation"> Error - <?= ucfirst($error); ?> </strong>
			<?php } else if(isset($result['profile_activated']) && $result['profile_activated'] == 0 && isset($result['id'])) { ?>
				<strong class="col-xs-12 success-activation"> Please change your password to activate the profile </strong>
			<div class="panel panel-default">
				<div class="panel-heading">
					
							<form role="form"  name="frm_newpwd" id="frm_newpwd" method="post" action="#">
								<div class="form-group">
									<div class="row">
										<div class="form-group col-xs-12 pull-left">
											<label for="exampleInputEmail1">Current Password</label>
											<input type="password" class="form-control" id="current_password" placeholder="Current Password" name="current_password" required />
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 pull-left">
											<label for="exampleInputEmail1">New Password</label>
											<input type="password" class="form-control reset_pwd" id="new_Password" placeholder="New Password" name="password" required />
										</div>
									</div>
									<div class="row">
										<div class="form-group col-xs-12 pull-left">
											<label for="exampleInputEmail1">Confirm Password</label>
											<input type="password" class="form-control reset_cnf_pwd" id="confirm_Password" placeholder="Confirm Password" name="cnf_password" required />
										</div>
									</div>
									<div class="row">
										<div class='response' style="display:none;"></div>
									</div>
									<div class="row">
										<div class="form-group col-sm-12 col-xs-12 pull-left">
											<input type='hidden' name='hdn_id' value="<?= @$result['id'];?>"  readonly />
											<input type='hidden' name='hdn_key' value="<?= @$result['activation_key'];?>"  readonly />						
											<button type="button" class="loginBtn btn_submit btn_submit_newpwd">Save changes</button>
											<button type="button" class="cancel-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
										</div>
									</div>
								</div>
							</form>
					<?php } ?>
				</div>
				<div class="col-xs-12 error-go-home-btn">
					<a class="navbar-brand hidden-xs" href="\"><strong>Go back to Home </strong></a>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<!-- MAIN DIV ENDS HERE -->