<?php @extract($data['data']);?>
<!-- MAIN NEW LOGIN SCREEN CODE STARTS HERE -->

<div class="col-xs-12 login_landing_screen">
	<!-- LEFT SIDE WRAPPER CODE STARTS HERE -->
	<div class="col-lg-3 col-md-4 col-sm-3 col-xs-12 login_left_wrapper">
	</div>
	<!-- LEFT SIDE WRAPPER CODE ENDS HERE -->

	<!-- RIGHT SIDE WRAPPER CODE STARTS HERE -->
	<div class="col-lg-9 col-md-8 col-sm-9 col-xs-12 login_right_wrapper">
		<!-- LOGO CIRCLE CODE STARTS HERE -->
		<div class="login_circle_wrapper">
			<!-- <span>AK FITTED INTERIORS</span> -->
			<img class="img-responsive logo-img" border="0" src="../../assets/img/login-logo-img.png" alt="AKFI" title="AKFI" />
		</div>
		<!-- LOGO CIRCLE CODE ENDS HERE -->

		<!-- LOGIN FORM FIELDS CODE STARTS HERE -->
		<div class="login_form_wrapper">
			<?php if(isset($error)) { ?>
					<strong> Error - <?= ucfirst($error); ?> </strong>
			<?php } else if(isset($result['profile_activated']) && $result['profile_activated'] == 1 && isset($result['id'])) { ?>
				<form role="form"  name="frm_resetpwd" id="frm_resetpwd" method="post" action="#">
					<!-- FORM FIELDS CODE STARTS HERE -->
					<h1>Reset Password</h1>
					<div class="col-md-9 col-sm-8 col-xs-9 login-input-filed-wrapper">
						<div class="form-group input-group last">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="password" class="form-control" id="curr_password" name="current_password"  placeholder="Your Current Password" value="" required />
						</div>
						<div class="form-group input-group last">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="password" class="form-control" id="password" name="password"  placeholder="New Password" value="" required />
						</div>
						<div class="form-group input-group last">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
							<input type="password" class="form-control" id="cnf_password" name="cnf_password"  placeholder="Confirm Password" value="" required />
						</div>
						
						<div class="form-group">
							<div class='response' style="display:none;"></div>
						</div>
					</div>

					<!-- FORM FIELDS CODE ENDS HERE -->
					<!-- FORM FIELDS SUBMIT BUTTON CODE STARTS HERE -->
					<div class="col-md-3 col-sm-4 col-xs-3 form-group login-input-btn-wrapper">
						<input type='hidden' name='hdn_id' value="<?= @$result['id'];?>"  readonly />
						<button class="btn_submit_resetpwd" type="button" value="Login Now" > <span class="only-small-devices">Save changes</span> <i class="fa fa-angle-right"></i></button>
					</div>
					<!-- FORM FIELDS SUBMIT BUTTON CODE ENDS HERE -->
				</form>
			<?php } ?>
		</div>
		<!-- LOGIN FORM FIELDS CODE ENDS HERE -->
	</div>
	<!-- RIGHT SIDE WRAPPER CODE ENDS HERE -->
</div>
<!-- MAIN NEW LOGIN SCREEN CODE ENDS HERE -->