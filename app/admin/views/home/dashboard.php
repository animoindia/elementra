<?php 
	@extract($data['data']); 
	$cnt_project = (isset($finalData)) ? count($finalData) : 'No ';
?>
<div class="header"> 
	<h1 class="page-header">Dashboard</h1>
	<ol class="breadcrumb">
		<li><a href="/" onclick="$('.div_loading_image').show();">Home</a></li>
		<li><a href="/dashboard" onclick="$('.div_loading_image').show();">Dashboard</a></li>
		<li class="active">Data</li>
	</ol> 
</div>
<div id="page-inner"> 
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			   <div class="panel-heading">
					Found <?= @$cnt_project; ?> Project
				</div>
				
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Project Id</th>
									<th>Client Name</th>
									<th>Account Approval</th>
									<th>Assigned Supervisor</th>
									<th>Technical Check</th>
									<th>Marking Of Points</th>
									<th>Final Measurement</th>
									<th>PDI</th>
									<th>Installation</th>
									<th>Finished</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$finalArr = [];
								if(isset($finalData) && count($finalData) > 0){
									foreach($finalData as $prj_key => $prj_arr) {
										
										$firstArr = reset($prj_arr);
										$finalArr[$prj_key]['sr'] 	  = @date('#Ymd', strtotime($firstArr['created_at'])).@$prj_key;
										$finalArr[$prj_key]['srtext'] = @$firstArr['project_for'].' - '.@$firstArr['type'];
										$finalArr[$prj_key]['client'] = @$firstArr['client_name'];
										
										$stepCnt = count($prj_arr);
										if($stepCnt == 8) {
											foreach($prj_arr as $key => $val) {
												@extract($val);
												$date 		 = (trim($tran_date) != '') ? date('d-m-Y (H:i)', strtotime($tran_date)) : '-';
												$status_txt  = (trim($tran_status) != '') ? $tran_status : '-';
												$assignee 	 = (trim($tran_assigned_to) != '') ? $tran_assigned_to : '-';
												$assignBy 	 = (trim($tran_created_by) != '') ? $tran_created_by : '-';
												
												$finalArr[$prj_key][$key]['date'] = $date;
												$finalArr[$prj_key][$key]['status'] = $status_txt;
												$finalArr[$prj_key][$key]['assigned_to'] = $assignee;
												$finalArr[$prj_key][$key]['assigned_by'] = $assignBy;
											}
										} else {
											for($i=1; $i <= ((8-$stepCnt)+1); $i++){
												foreach($prj_arr as $key => $val){
													@extract($val);
													
													$date 		 = (isset($prj_arr[$i]) && trim($tran_date) != '') ? date('d-m-Y (H:i)', strtotime($tran_date)) : '-';
													$status_txt  = (isset($prj_arr[$i]) && trim($tran_status) != '') ? $tran_status : '-';
													$assignee 	 = (isset($prj_arr[$i]) && trim($tran_assigned_to) != '') ? $tran_assigned_to : '-';
													$assignBy 	 = (isset($prj_arr[$i]) && trim($tran_created_by) != '') ? $tran_created_by : '-';
													
													$finalArr[$prj_key][$i]['date'] 		= $date;
													$finalArr[$prj_key][$i]['status'] 		= $status_txt;
													$finalArr[$prj_key][$i]['assigned_to'] 	= $assignee;
													$finalArr[$prj_key][$i]['assigned_by'] 	= $assignBy;
													
												}
											}
										}
									}
								}
								
								if(isset($finalArr) && count($finalArr) > 0){
									$i=0;
									foreach($finalArr as $prjKey=>$value){
										$row_class = ($i%2 == 0) ? 'odd' : 'even';
								?>
									<tr class="<?= $row_class;?> ">
										<td>
											<a href="\project/view?prj=<?= $prjKey;?>&page=dashboard" onclick="$('.div_loading_image').show();">
												<?= @$value['sr'];?><br/>
												<?= @$value['srtext'];?>
											</a>
										</td>
										<td><?= @$value['client'];?></td>
										<td><?= ($value[1]['date'] != '-') ? @$value[1]['date'].'<br />'.@$value[1]['status'] : '-';?></td>
										<td><?= ($value[2]['date'] != '-') ? @$value[2]['date'].'<br />'.@$value[2]['assigned_to'] : '-';?></td>
										<td><?= @$value[3]['date'];?></td>
										<td><?= @$value[4]['date'];?></td>
										<td><?= @$value[5]['date'];?></td>
										<td><?= @$value[6]['date'];?></td>
										<td><?= @$value[7]['date'];?></td>
										<td><?= @$value[8]['date'];?></td>
									</tr>
								<?php
										
										$i++;
									}
								}
								?>
							
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
			<!--  end  Context Classes  -->
		</div>
	</div>
	<!-- /. ROW  -->
</div>