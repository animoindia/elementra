<?php
	@extract($data);
	@extract($data['result']);
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="myModalLabel"><?= @($action == 'update') ? 'Edit '.$name : ' New Company'; ?> </h4>
</div>

<form role="form" name="frm_company" id="frm_company" action='#' method="post" class="frm_company" enctype="multipart/form-data">
	<div class="modal-body">
		<div class="row">
			<div class="form-group col-xs-12 pull-left">
				<!-- Company logo Wrapper Code Starts Here -->
				<div class="col-xs-12 profil-pic-wrapper modal-popup">
					<div class="row">
						<div class="small-12 medium-2 large-2 columns">
							<div class="circle">
								<!-- User Profile Image -->
								<?php if(isset($logo) && $logo != '') { ?>
									<img class="profile-pic" src="<?= IMAGE_VIEW_PATH;?>company/<?= $id;?>_<?= $logo;?>">
								<?php } else { ?>
									<img class="profile-pic" src="<?= IMG_PATH;?>default-logo.png">
								<?php } ?>

								<!-- Default Image -->
								<i class="fa fa-user fa-5x"></i>
							</div>
							<div class="p-image">
								<i class="fa fa-camera upload-button"></i>
								<input class="file-upload" type="file" accept="image/*" name="image"/>
							</div>
						</div>
					</div>
				</div>
				<!-- Profile Picture Wrapper Code Ends Here -->
				<div class="col-xs-12 profil-fields-wrapper  modal-popup">
					<div class="row">
						<div class="form-group col-xs-12 pull-left">
							<label for="exampleInputEmail1">Name</label>
							<input type="text" class="form-control" id="name" placeholder="Enter Name" name="data[name]" value="<?= @$name;?>" required />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 pull-left">
							<label for="exampleInputEmail1">GST No.</label>
							<input type="text" class="form-control" id="gst_no" placeholder="Enter GST No" name="data[gst_no]" value="<?= @$gst_no;?>" required />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 pull-left">
							<label for="exampleInputEmail1">Contact Person</label>
							<input type="text" class="form-control" id="contact_person" placeholder="Enter Contact Person" name="data[contact_person]" value="<?= @$contact_person;?>" required />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 pull-left">
							<label for="exampleInputEmail1">Contact No.</label>
							<input type="number" class="form-control" id="contact_no" placeholder="Enter Contact No" name="data[contact_no]" value="<?= @$contact_no;?>" required />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-12 pull-left">
							<label for="exampleInputEmail1">Email address</label>
							<input type="email" class="form-control" id="email" placeholder="Enter email" name="data[email]" value="<?= @$email;?>" required />
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12 col-xs-12 pull-left">
							<label for="exampleInputEmail1">Address</label>
							<textarea class="form-control" rows="3" id="address" placeholder="Enter Address" name="data[address]" required><?= @$address;?></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class='response' style="display:none;"></div>
	<div>
	<div class="modal-footer">
		<div class="row">
			<input type="hidden" class="form-control" id="action" name="action" value="<?= @$action;?>" readonly />
			<input type="hidden" class="form-control" id="id" name="id" value="<?= @$id;?>" readonly />
			<div class="form-group col-sm-12 col-xs-12 pull-left">
				<button type="submit" class="loginBtn btn_submit btn_submit_company">Save changes</button>
				<button type="button" class="cancel-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
			</div>
		</div>
	</div>
</form>
<script src="./../assets/js/lib/main.js"></script>
<script src="./../assets/js/lib/company.js"></script>