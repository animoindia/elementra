<?php 
	
	@extract($data['data']); 
	$cnt_company = (isset($result)) ? count($result) : 'No '; 
?>

<div class="header"> 
	<h1 class="page-header">Company</h1>
	<div class="create-project-wrapper">
		<a href="\company/new" class="" data-toggle="modal" data-target="#myModal" onclick="$('.div_loading_image').show();">Add Company</a>
	</div>
	<ol class="breadcrumb">
		<li class="active">Data</li>
		<li><a href="\" onclick="$('.div_loading_image').show();">Home</a></li>
		<li><a href="\company/list" onclick="$('.div_loading_image').show();">Company</a></li>
	</ol> 
</div>
<div id="page-inner"> 
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			   <div class="panel-heading">
					Found <?= $cnt_company; ?> Company
				</div>
				
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>GST No</th>
									<th>Contact Person</th>
									<th>Contact No</th>
									<th>Email</th>
									<th>Is Active</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								foreach($result as $key => $value) {
									@extract($value);
									$row_class = ($id%2 == 0) ? 'odd' : 'even';
									$active = @($is_active == 1) ? "Active" : "In";
							?>
									<tr class="<?= $row_class;?> ">
										<td><?= @$id;?></td>
										<td><?= @$name;?></td>
										<td><?= @$gst_no;?></td>
										<td><?= @$contact_person;?></td>
										<td><?= @$contact_no;?></td>
										<td><?= @$email;?></td>
										<td>
											<?php if($is_active == 1) { ?>
												<i class="fa fa-check" aria-hidden="true" style="color:green;" title="Active"></i>
											<?php } else { ?>
													<i class="fa fa-times" aria-hidden="true" style="color:red;" title="In-active"></i>
											<?php } ?>
										</td>
										<td>
											<a class="" href="\company/edit?cmpny=<?= $id;?>" data-toggle="modal" data-target="#myModal" onclick="$('.div_loading_image').show();">
												<i class="fa fa-pencil-square-o info" aria-hidden="true" title="Edit Company"></i>
											</a>
											&nbsp;|&nbsp;
											<?php if($is_active == 1) { ?>
												<a href="\company/active?id=<?= $id;?>&flag=0" onclick="return confirm('Are you sure, you want to disable?')">
													<i class="fa fa-times" aria-hidden="true" style="color:red;" title="Keep Disable"></i>
												</a>
											<?php } else { ?>
												<a href="\company/active?id=<?= $id;?>&flag=1" onclick="return confirm('Are you sure, you want to enable?')">
													<i class="fa fa-check" aria-hidden="true" style="color:green;" title="Keep Enable"></i>
												</a>
											<?php } ?>
										</td>
									</tr>
							<?php 
								} 
							?>
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
			<!--  end  Context Classes  -->
		</div>
	</div>
	<!-- /. ROW  -->
</div>