<?php @extract($data['data']); ?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">
		<?php if(@$arrData['user_dept'] == 3) { ?>
			Assgin Supervisor
		<?php } else { ?>
			Assgin Accountant for approval
		<?php } ?>			
		</h4>
	</div>
	<form role="form" name="frm_assignment" id="frm_assignment" method="post" action="#">
		<div class="modal-body">
			<div class="row form-group">
				<div class="col-xs-12">
					<div class="form-group col-xs-12 pull-left">
						<label for="exampleInputEmail1">
						<?php if(@$arrData['user_dept'] == 3) { ?>
							Site Supervisor
						<?php } else { ?>
							Accountant
						<?php } ?>						
						</label>
						<select class="form-control cls_offer" id='assignee_id' name='assignee_id'>
							<option value=''>Select</option>
							<?php foreach($result as $key=>$val) { ?>
								<option value="<?= $val['id']; ?>"><?= trim(ucfirst($val['f_name'])).' '.trim(ucfirst($val['l_name']));?></option>
							<?php } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12">
					<div class="form-group col-xs-12 pull-left">
						<input type="text" class="form-control" placeholder="Remarks" name="remarks" required="" aria-required="true"> <br />
					</div>
				</div>
			</div>
			<div class='response' style="display:none;"></div>
		</div>
		
		<div class="modal-footer">
			<div class="row form-group">
				<div class="form-group col-sm-12 col-xs-12 pull-left">
					<input type='hidden' name='hdn_project_ids' value="<?= $arrData['project_id'];?>" readonly />
					<input type='hidden' name='hdn_step_id' value="<?= $arrData['step'];?>" readonly />
					<button type="button" class="loginBtn btn_submit btn_submit_assignment">Submit</button>
					<button type="button" class="cancel-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
			</div>
		</div>
	</form>
	<script src="./../assets/js/lib/project.js"></script>
