<?php 
$objUtil = new Util();
@extract($data['data']); 
@extract($result);

$arrData = $objUtil->decodeData($data);

$arrStep1 = @$arrData['Client & Site Details'];
$arrStep2 = @$arrData['Base Material And Shutter Finish'];
$arrStep3 = @$arrData['Front And Internal Colors'];
$arrStep4 = @$arrData['Heights And Depths'];
$arrStep5 = @$arrData['Hardware Details'];
$arrStep6 = @$arrData['Accessories'];
$arrStep7 = @$arrData['Appliances And Counter Details'];
$arrStep8 = @$arrData['Special Remarks'];
$arrStep9 = @$arrData['Drawings With Markings'];

$arrSteps = ['',
'Client & Site Details',
'Base Material And Shutter Finish',
'Front And Internal Colors',
'Heights And Depths',
'Hardware Details',
'Accessories',
'Appliances And Counter Details',
'Special Remarks',
'Drawings With Markings',
];
?>
<div class="header"> 
	<h1 class="page-header">Project</h1>
	<ol class="breadcrumb">
		<li><a href="\">Home</a></li>
		<?php if($page == 'dashboard') { ?>
			<li><a href="\home/dashboard" onclick="$('.div_loading_image').show();">Dashboard</a></li>
		<?php } else { ?>
			<li><a href="\project/list" onclick="$('.div_loading_image').show();">Projects</a></li>
		<?php } ?>
		<li class="active">View</li>
	</ol>
</div>
<div id="page-inner"> 
	<div class="row">
		<div class="col-md-12">
			<!-- PROJECT VIEW PAGE CODEE STARTS HERE  -->
			<!-- BELOW PLUGIN CODE TAKEN FROM URL: https://www.bootply.com/oROUAMwsG1# -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="card-title">
						<div class="title">PROJECT VIEW DETAILS</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="col-xs-12 project-selected-view-wrapper">
						<div class="col-sm-4 col-xs-12 project-highlight">Project Id: <?= @date('#Ymd', strtotime($created_at)).@$id;?> </div>
						<div class="col-sm-4 col-xs-12 project-highlight">Project Type: <?= @$arrStep1['details']['project_type']; ?> </div>
						<div class="col-sm-4 col-xs-12 project-highlight">Project For: <?= @$arrStep1['details']['project_for']; ?> </div>
					</div>
					<?php if($page == 'dashboard') { ?>
						<ul id="tabs" class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#t1" class="cls-tab">Project Details</a></li>
							<li><a data-toggle="tab" href="#t2" class="cls-tab">Site Updates</a></li>
						</ul>
					<?php } else { ?>
						<ul id="tabs" class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#t1" class="cls-tab">Project Details</a></li>
						</ul>
					<?php } ?>	
					<div class="tab-content">
					
						<!-- PROJECT DETAILS CODE STARTS HERE -->
						<div id="t1" class="tab-pane fade in active">
							
							<!-- MAIN PROJECT VIEW DETAILS CODE STARTS HERE  -->
							<!-- ORIGINAL CODE URL: https://codepen.io/sagar_arora/pen/BRBopY  -->
							<div class="map-container tabs_wrapper">
								<div class="inner-basic division-map div-toggle divisiondetail" data-target=".division-details" id="divisiondetail">
								
									<button class="map-point-sm active" data-show=".step1">
									  <div class="content">
										<div class="centered-y">
										  <p>Client And Site Details</p>
										</div>
									  </div>
									</button>
									<button class="map-point-sm" data-show=".step2">
									  <div class="content">
										<div class="centered-y">
										  <p>Base Material And Shutter Finish</p>
										</div>
									  </div>
									</button>
									<button class="map-point-sm" data-show=".step3">
									  <div class="content">
										<div class="centered-y">
										  <p>Front And Internal Colors</p>
										</div>
									  </div>
									</button>
									<button class="map-point-sm" data-show=".step4">
									  <div class="content">
										<div class="centered-y">
										  <p>Heights And Depths</p>
										</div>
									  </div>
									</button>
									<button class="map-point-sm" data-show=".step5">
									  <div class="content">
										<div class="centered-y">
										  <p>Hardware Details</p>
										</div>
									  </div>
									</button>
									<button class="map-point-sm" data-show=".step6">
									  <div class="content">
										<div class="centered-y">
										  <p>Accessories</p>
										</div>
									  </div>
									</button>
									<button class="map-point-sm" data-show=".step7">
									  <div class="content">
										<div class="centered-y">
										  <p>Appliances And Counter Details</p>
										</div>
									  </div>
									</button>
									<button class="map-point-sm" data-show=".step8">
									  <div class="content">
										<div class="centered-y">
										  <p>Special Remarks</p>
										</div>
									  </div>
									</button>
									<button class="map-point-sm" data-show=".step9">
									  <div class="content">
										<div class="centered-y">
										  <p>Drawings With Markings</p>
										</div>
									  </div>
									</button>
									
								</div>
							</div>
							<!-- MAIN PROJECT VIEW DETAILS CODE ENDS HERE  -->
														
							<!-- MAIN TABS MAP CONTAINER WRAPPER CODE STARTS HERE  -->	
							<div class="map-container">
								<div class="inner-basic division-details">
							
									<!-- PROJECT DETAILS: STEP1 CLIENT / SITE CODE STARTS HERE -->
									<div class="step1 initialmsg form-box">
										<div class="form-bottom">
											<div class="form-row">
												<div class="form-holder">
													<label>Client's Name:</label>
													<div class="form-control view_only_wrapper"><?= @$arrStep1['details']['client_name'];?></div>
												</div>
												<div class="form-holder">
													<label>Phone:</label>
													<div class="form-control view_only_wrapper"><?= @$arrStep1['details']['client_phone'];?></div>
												</div>
											</div>
											<div class="form-row">
												<div class="form-holder w-100">
													<label>Site Address:</label>
													<div class="form-control view_only_wrapper"><?= @$arrStep1['details']['client_address'];?></div>
												</div>
											</div>
											<div class="form-row">
												<div class="form-holder w-100">
													<label>Details:</label>
													<div class="form-control view_only_wrapper"><?= @$arrStep1['details']['client_details'];?></div>
												</div>
											</div>
											<div class="form-row">
												<div class="form-holder w-100">
													<label>Remarks:</label>
													<div class="form-control view_only_wrapper"><?= @$arrStep1['details']['client_remarks'];?></div>
												</div>
											</div>
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP1 CLIENT / SITE CODE ENDS HERE -->

									<!-- PROJECT DETAILS: STEP2 BASE MATERIAL / SHUTTER CODE STARTS HERE -->
									<div class="step2 hide form-box">
										<div class="form-bottom">
											<!-- Section 1 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Core Material:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Core Material']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Core Material']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 1 Ends  -->
																						
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Base Shutters Finish:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Base Shutters Finish']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Base Shutters Finish']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 2 Ends  -->
											
											<!-- Section 3 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Wall Shutters Finish:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Wall Shutters Finish']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Wall Shutters Finish']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 3 Ends  -->
											
											<!-- Section 4 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tall Shutters Finish:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Tall Shutters Finish']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Tall Shutters Finish']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 4 Ends  -->
											
											<!-- Section 4 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Loft Unit Finish:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Loft Unit Finish']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep2['Loft Unit Finish']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep2['Loft Unit Finish']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[2]."/Loft Unit Finish/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[2]."/Loft Unit Finish/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive" title="<?= $imgname;?>"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE END HERE -->
											</div>
											<!-- Section 4 Ends  -->
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP2 BASE MATERIAL / SHUTTER CODE ENDS HERE -->

									<!-- PROJECT DETAILS: STEP3 FRONT / INTERNAL COLORS CODE STARTS HERE -->
									<div class="step3 hide form-box">
										<div class="form-bottom">
										
											<!-- Section 1 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Base Shutter Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Base Shutter Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Base Shutter Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Base Shutter Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Base Shutter Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Base Shutter Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive" title="<?= $imgname;?>"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE END HERE -->
											</div>
											<!-- Section 1 Ends  -->
											
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Base Shutter Back Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Base Shutter Back Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Base Shutter Back Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Base Shutter Back Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Base Shutter Back Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Base Shutter Back Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 2 Ends  -->
											
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Base Carcass Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Base Carcass Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Base Carcass Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Base Shutter Carcass Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Base Shutter Carcass Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Base Shutter Carcass Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 2 Ends  -->
											
											<!-- Section 3 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Wall Shutter Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Wall Shutter Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Wall Shutter Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Wall Shutter Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Wall Shutter Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Wall Shutter Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 3 Ends  -->
										
											<!-- Section 4 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Wall Shutter Back Colour :</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Wall Shutter Back Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Wall Shutter Back Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Wall Shutter Back Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Wall Shutter Back Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Wall Shutter Back Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->													
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 4 Ends  -->
																			
											<!-- Section 5 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Wall Carcass Colour :</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Wall Carcass Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Wall Carcass Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Wall Carcass Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Wall Carcass Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Wall Carcass Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 5 Ends  -->
											
											<!-- Section 6 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tall Shutter Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Tall Shutter Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Tall Shutter Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Tall Shutter Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Tall Shutter Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Tall Shutter Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 6 Ends  -->
											
											<!-- Section 7 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tall Carcass Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Tall Carcass Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Tall Carcass Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Tall Carcass Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Tall Carcass Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Tall Carcass Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 7 Ends  -->
											
											<!-- Section 8 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tall Cabinet Visible Side Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Tall Cabinet Visible Side Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Tall Cabinet Visible Side Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Tall Cabinet Visible Side Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Tall Cabinet Visible Side Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Tall Cabinet Visible Side Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 8 Ends  -->
																			
											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Drawer Bottom And Back Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Drawer Bottom And Back Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Drawer Bottom And Back Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Drawer Bottom And Back Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Drawer Bottom And Back Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Drawer Bottom And Back Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 9 Ends  -->
											
											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Semi Tall Finish:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Semi Tall Finish']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Semi Tall Finish']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Semi Tall Finish']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Semi Tall Finish/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Semi Tall Finish/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive" title="<?= $imgname;?>"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE END HERE -->
											</div>
											<!-- Section 9 Ends  -->
											
											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Semi Tall Shutter Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Semi Tall Shutter Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Semi Tall Shutter Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Semi Tall Shutter Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Semi Tall Shutter Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Semi Tall Shutter Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 9 Ends  -->
											
											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Semi Tall Carcass Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Semi Tall Carcass Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Semi Tall Carcass Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Semi Tall Carcass Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Semi Tall Carcass Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Semi Tall Carcass Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 9 Ends  -->
											
											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Loft Shutter Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Loft Shutter Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Loft Shutter Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Loft Shutter Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Loft Shutter Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Loft Shutter Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 9 Ends  -->
											
											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Loft Carcass Colour:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Loft Carcass Colour']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep3['Loft Carcass Colour']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep3['Loft Carcass Colour']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[3]."/Loft Carcass Colour/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[3]."/Loft Carcass Colour/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->	
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 9 Ends  -->
										
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP3 FRONT / INTERNAL COLORS CODE ENDS HERE -->
									
									<!-- PROJECT DETAILS: STEP4 HEIGHT / DEPTH CODE STARTS HERE -->
									<div class="step4 hide form-box">
										<div class="form-bottom">
										
											<!-- Section 1 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Base Cabinet Depth:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Base Cabinet Depth']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Base Cabinet Depth']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 1 Ends  -->
											
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Base Shutter Height:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Base Shutter Height']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Base Shutter Height']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 2 Ends  -->

											<!-- Section 3 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Wall Cabinet Height :</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Wall Cabinet Height']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Wall Cabinet Height']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 3 Ends  -->

											<!-- Section 4 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Wall Cabinet Depth:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Wall Cabinet Depth']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Wall Cabinet Depth']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 4 Ends  -->

											<!-- Section 5 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tall Cabinet Depth</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Tall Cabinet Depth']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Tall Cabinet Depth']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 5 Ends  -->
											
											<!-- Section 6 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tall Cabinet Height:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Tall Cabinet Height']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Tall Cabinet Height']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 6 Ends  -->
											
											<!-- Section 7 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Skirting Height:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Skirting Height']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Skirting Height']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 7 Ends  -->
											
											<!-- Section 8 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Skirting Finish:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Skirting Finish']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Skirting Finish']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep4['Skirting Finish']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[4]."/Skirting Finish/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[4]."/Skirting Finish/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->														
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 8 Ends  -->
											
											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Platform Depth:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Platform Depth']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Platform Depth']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 9 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Counter Height:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Counter Height']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Counter Height']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Loft Depth:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Loft Depth']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Loft Depth']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Loft Height:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Loft Height']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Loft Height']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Semi Tall Depth:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Semi Tall Depth']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Semi Tall Depth']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Roller Shutter Height:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Roller Shutter Height']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep4['Roller Shutter Height']['remarks'];?></div>
													</div>
												</div>							
											</div>
											<!-- Section 10 Ends  -->											
										
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP4 HEIGHT / DEPTH CODE ENDS HERE -->
									
									<!-- PROJECT DETAILS: STEP5 HARDWARE CODE STARTS HERE -->
									<div class="step5 hide form-box">
										<div class="form-bottom">
											
											<!-- Section 1 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Drawer Systems:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep5['Drawer Systems']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep5['Drawer Systems']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 1 Ends  -->
											
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Lift Up Systems:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep5['Lift Up Systems']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep5['Lift Up Systems']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep5['Lift Up Systems']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[5]."/Lift Up Systems/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[5]."/Lift Up Systems/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 2 Ends  -->
											
											<!-- Section 3 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Hinges:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep5['Hinges']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep5['Hinges']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 3 Ends  -->
										
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP5 HARDWARE CODE ENDS HERE -->
									
									<!-- PROJECT DETAILS: STEP6 ACCESSORIES CODE STARTS HERE -->
									<div class="step6 hide form-box">
										<div class="form-bottom">
										
											<!-- Section 1 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Dustbin:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Dustbin']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Dustbin']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Dustbin']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Dustbin/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Dustbin/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 1 Ends  -->
									
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Detergent :</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Detergent']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Detergent']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Detergent']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Detergent/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Detergent/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 2 Ends  -->
											
											<!-- Section 3 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Cutlery Tray:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Cutlery Tray']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Cutlery Tray']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Cutlery Tray']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Cutlery Tray/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Cutlery Tray/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->													
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 3 Ends  -->
										
											<!-- Section 4 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Dishrack:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Dishrack']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Dishrack']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Dishrack']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Dishrack/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Dishrack/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->													
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 4 Ends  -->
											
											<!-- Section 5 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Pullout Baskets:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Pullout Baskets']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Pullout Baskets']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Pullout Baskets']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Pullout Baskets/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Pullout Baskets/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 5 Ends  -->
											
											<!-- Section 6 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Handle (Base):</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Handle (Base)']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Handle (Base)']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Handle (Base)']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Handle (Base)/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Handle (Base)/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 6 Ends  -->
											
											<!-- Section 7 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Handle (1St Layer Wall Cabinet):</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Handle (1st Layer Wall Cabinet)']['details'];?>
														</div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Handle (1st Layer Wall Cabinet)']['remarks'];?>
														</div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Handle (1st Layer Wall Cabinet)']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Handle (1st Layer Wall Cabinet)/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Handle (1st Layer Wall Cabinet)/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 7 Ends  -->
											
											<!-- Section 8 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Handle (Tall):</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Handle (Tall)']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Handle (Tall)']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Handle (Tall)']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Handle (Tall)/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Handle (Tall)/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 8 Ends  -->
										
											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Profile Details:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Profile Details']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Profile Details']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Profile Details']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Profile Details/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Profile Details/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 9 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tall Units:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Tall Units']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Tall Units']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Tall Units']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Tall Units/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Tall Units/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Corner Accessories:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Corner Accessories']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Corner Accessories']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Corner Accessories']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Corner Accessories/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Corner Accessories/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Bottle Trolly:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Bottle Trolly']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Bottle Trolly']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Bottle Trolly']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Bottle Trolly/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Bottle Trolly/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Semi Tall Units:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Semi Tall Units']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Semi Tall Units']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Semi Tall Units']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Semi Tall Units/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Semi Tall Units/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Corner Units:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Corner Units']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Corner Units']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Corner Units']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Corner Units/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Corner Units/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Handle For Loft:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Handle For Loft']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Handle For Loft']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Handle For Loft']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Handle For Loft/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Handle For Loft/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Vegetable Drawer:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Vegetable Drawer']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Vegetable Drawer']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Vegetable Drawer']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Vegetable Drawer/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Vegetable Drawer/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 10 Ends  -->
											
											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Additional Accessories:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Additional Accessories']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep6['Additional Accessories']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep6['Additional Accessories']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[6]."/Additional Accessories/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[6]."/Additional Accessories/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 10 Ends  -->
											
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP6 ACCESSORIES CODE ENDS HERE -->

									<!-- PROJECT DETAILS: STEP7 APPLIANCE & COUNTER CODE STARTS HERE -->
									<div class="step7 hide form-box">
										<div class="form-bottom">
											
											<!-- Section 1 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Chimney:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Chimney']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Chimney']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 1 Ends  -->
											
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Hob:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Hob']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Hob']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 2 Ends  -->
											
											<!-- Section 3 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Sink:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Sink']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Sink']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep7['Sink']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[7]."/Sink/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[7]."/Sink/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 3 Ends  -->

											<!-- Section 4 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tap:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Tap']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Tap']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep7['Tap']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[7]."/Tap/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[7]."/Tap/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 4 Ends  -->

											<!-- Section 5 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Micro:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Micro']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Micro']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep7['Micro']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[7]."/Micro/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[7]."/Micro/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 5 Ends  -->

											<!-- Section 6 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Oven:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Oven']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Oven']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 6 Ends  -->

											<!-- Section 7 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Dishwasher:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Dishwasher']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Dishwasher']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 7 Ends  -->

											<!-- Section 8 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Fridge:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Fridge']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Fridge']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep7['Fridge']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[7]."/Fridge/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[7]."/Fridge/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 8 Ends  -->

											<!-- Section 9 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Any Other, Appliance:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Any Other, Appliance']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Any Other, Appliance']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 9 Ends  -->

											<!-- Section 10 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Counter Top:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Counter Top']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Counter Top']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 10 Ends  -->

											<!-- Section 11 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Granite Edge Type For Counter:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Granite Edge Type For Counter']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep7['Granite Edge Type For Counter']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep7['Granite Edge Type For Counter']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[7]."/Granite Edge Type For Counter/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[7]."/Granite Edge Type For Counter/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 11 Ends  -->
										
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP7 APPLIANCE & COUNTER CODE ENDS HERE -->

									<!-- PROJECT DETAILS: STEP8 APPLIANCE & COUNTER CODE STARTS HERE -->
									<div class="step8 hide form-box">
										<div class="form-bottom">
											
											<!-- Section 1 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Led Light Details:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Led Light Details']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Led Light Details']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 1 Ends  -->
											
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Led Light Placements:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Led Light Placements']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Led Light Placements']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 2 Ends  -->
											
											<!-- Section 3 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Base Cabinet Shelves:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Base Cabinet Shelves']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Base Cabinet Shelves']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 3 Ends  -->
											
											<!-- Section 4 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Wall Cabinet Shelves:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Wall Cabinet Shelves']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Wall Cabinet Shelves']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 4 Ends  -->
											
											<!-- Section 5 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Tall Cabinet Shelves:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Tall Cabinet Shelves']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Tall Cabinet Shelves']['remarks'];?></div>
													</div>
												</div>
											</div>
											<!-- Section 5 Ends  -->
											
											<!-- Section 5 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> Aqua guard Cabinet:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Aqua guard Cabinet']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep8['Aqua guard Cabinet']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep8['Aqua guard Cabinet']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[8]."/Aqua guard Cabinet/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[8]."/Aqua guard Cabinet/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 5 Ends  -->
										
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP8 APPLIANCE & COUNTER CODE ENDS HERE -->
									
									<!-- PROJECT DETAILS: STEP9 APPLIANCE & COUNTER CODE STARTS HERE -->
									<div class="step9 hide form-box">
										<div class="form-bottom">
										
											<!-- Section 1 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> 2D Autocad With Measurements and Detials:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep9['2D Autocad With Measurements and Detials']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep9['2D Autocad With Measurements and Detials']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>	
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep9['2D Autocad With Measurements and Detials']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[9]."/2D Autocad With Measurements and Detials/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[9]."/2D Autocad With Measurements and Detials/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->													
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 1 Ends  -->
											
											<!-- Section 2 Starts -->
											<div class="content_main_title_wrapper col-xs-12">
												<label class="main_title_label col-xs-12"><i class="fa fa-asterisk" aria-hidden="true"></i> 3D Drawings:</label>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Details:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep9['3D Drawings']['details'];?></div>
													</div>
												</div>
												<div class="form-row clearBoth">
													<div class="form-holder w-100">
														<label>Remarks:</label>
														<div class="form-control view_only_wrapper"><?= @$arrStep9['3D Drawings']['remarks'];?></div>
													</div>
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE STARTS HERE -->
												<div class="col-xs-12 img-uploads-wrapper">
													<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>					
													<!-- IMAGE GALLERY CODE STARTS HERE -->
													<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
													<section id="gallery" class="gallery">
														<div id="image-gallery" class="image-gallery">
															<?php
																$filesArr = [];
																$filesArr = explode(',', @$arrStep9['3D Drawings']['files']);
																if(count($filesArr) >= 1){
																	foreach($filesArr as $val){
																		$imgname = trim($val);
																		$imgpath = IMAGE_VIEW_PATH ."projects/$id/main/".$arrSteps[9]."/3D Drawings/$imgname";
																		$imglink = IMG_UPLOAD_PATH ."/projects/$id/main/".$arrSteps[9]."/3D Drawings/$imgname";
																		if(file_exists($imglink) && $imgname != ''){
															?>				
																			<div class="image">
																				<div class="img-wrapper">
																					<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																					<div class="img-overlay">
																						<i class="fa fa-plus-circle" aria-hidden="true"></i>
																					</div>
																				</div>
																			</div>
															<?php
																		}
																	}
																}
															?>
														</div><!-- End image gallery -->
													</section>
													<!-- IMAGE GALLERY CODE ENDS HERE -->
												</div>
												<!-- UPLOADED IMAGES DISPLAY CODE ENDS HERE -->
											</div>
											<!-- Section 2 Ends  -->
										</div>
									</div>
									<!-- PROJECT DETAILS: STEP9 APPLIANCE & COUNTER CODE ENDS HERE -->
								</div>
							</div>
							<!-- MAIN TABS MAP CONTAINER WRAPPER CODE ENDS HERE  -->	
							
						</div>
						<!-- PROJECT DETAILS CODE ENDS HERE -->

						
						<!-- SITE UPDATES CODE STARTS HERE -->
						<div id="t2" class="tab-pane fade">
							<!-- MAIN PROJECT VIEW DETAILS CODE STARTS HERE  -->	
							<!-- ORIGINAL CODE URL: https://codepen.io/sagar_arora/pen/BRBopY  -->
							<?php
								if($page == 'dashboard'){ 
							?>
								<div class="col-xs-12 project-selected-view-wrapper" sytle="clear: both">
									<div class="col-sm-4 col-xs-12 project-highlight">Account Approval: <?= @$progressArr[1]['status'];?> </div>
									<div class="col-sm-4 col-xs-12 project-highlight">Date: <?= @$progressArr[1]['tran_date']; ?> </div>
									<div class="col-sm-4 col-xs-12 project-highlight">Remarks: <?= @$progressArr[1]['tran_remarks']; ?> </div>
								</div>
								<div class="col-xs-12 project-selected-view-wrapper">
									<div class="col-sm-4 col-xs-12 project-highlight">Site Supervisor: <?= @$progressArr[2]['tran_assigned_to'];?> </div>
									<div class="col-sm-4 col-xs-12 project-highlight">Date: <?= @$progressArr[2]['tran_date']; ?> </div>
									<div class="col-sm-4 col-xs-12 project-highlight">Remarks: <?= @$progressArr[2]['tran_remarks']; ?> </div>
								</div>
								
								<!-- MAIN TABS WRAPPER VIEW DETAILS CODE STARTS HERE  -->	
								<div class="map-container tabs_wrapper">
									<div class="inner-basic division-map div-toggle divisiondetail1" data-target=".division-details" id="divisiondetail">
										<button class="map-point-sm active" data-show=".step1">
											<div class="content">
												<div class="centered-y">
													<p>STEP1: Technical Check</p>
												</div>
											</div>
										</button>
										<button class="map-point-sm" data-show=".step2">
											<div class="content">
												<div class="centered-y">
													<p>STEP2: Marking of Points</p>
												</div>
											</div>
										</button>
										<button class="map-point-sm" data-show=".step3">
											<div class="content">
												<div class="centered-y">
													<p>STEP3: Final Measurements</p>
												</div>
											</div>
										</button>
										<button class="map-point-sm" data-show=".step4">
											<div class="content">
												<div class="centered-y">
													<p>STEP4: PDI</p>
												</div>
											</div>
										</button>
										<button class="map-point-sm" data-show=".step5">
											<div class="content">
												<div class="centered-y">
													<p>STEP5: Installation</p>
												</div>
											</div>
										</button>
										<button class="map-point-sm" data-show=".step6">
											<div class="content">
												<div class="centered-y">
													<p>STEP6: Finished</p>
												</div>
											</div>
										</button>
									</div>									
								</div>
								<!-- MAIN TABS WRAPPER VIEW DETAILS CODE ENDS HERE  -->	
								
								<?php
									$stepData = [];
									if(isset($progressArr) and count($progressArr) > 0){
										foreach($progressArr as $k=>$v){
											if(trim($v['tran_data']) != '' && trim($v['tran_data']) != '-')
												$stepData[$k] = $objUtil->decodeData(@$v['tran_data']);
										}
									}
									?>
								
								<!-- MAIN TABS MAP CONTAINER WRAPPER CODE STARTS HERE  -->	
								<div class="map-container tabs_wrapper">
									<div class="inner-basic division-details">
								
										<!-- SITE UPDATES: STEP1 CODE STARTS HERE  -->	
										<div class="step1 initialmsg form-box">
											<div class="form-bottom">
												<!-- Section 1 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Site Measurement:</label>
													<div class="col-xs-12 site-view-only-wrapper">
													<?= (isset($stepData[3]) && isset($stepData[3]['Site Measurement'])) ? $stepData[3]['Site Measurement'] : '-';?>
													</div>
												</div>
												<!-- Section 1 Ends  -->
												<!-- Section 2 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Hardware Technical Check:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[3]) && isset($stepData[3]['Hardware Technical Check'])) ? $stepData[3]['Hardware Technical Check'] : '-';?>
													</div>
												</div>
												<!-- Section 2 Ends  -->
												<!-- Section 3 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Appliances Check:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[3]) && isset($stepData[3]['Applinaces Check'])) ? $stepData[3]['Applinaces Check'] : '-';?>
													</div>
												</div>
												<!-- Section 3 Ends  -->
												<!-- Section 4 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Sanitary Check:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[3]) && isset($stepData[3]['Sanitary Check'])) ? $stepData[3]['Sanitary Check'] : '-';?>
													</div>
												</div>
												<!-- Section 4 Ends  -->
												<!-- Section 5 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Electrical Points:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[3]) && isset($stepData[3]['Electrical Points'])) ? $stepData[3]['Electrical Points'] : '-';?>
													</div>
												</div>
												<!-- Section 5 Ends  -->
												<!-- Section 6 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Plumbing Points:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[3]) && isset($stepData[3]['Plumbing Points'])) ? $stepData[3]['Plumbing Points'] : '-';?>
													</div>
												</div>
												<!-- Section 6 Ends  -->
												<!-- Section 7 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Civil Points:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[3]) && isset($stepData[3]['Civil Points'])) ? $stepData[3]['Civil Points'] : '-';?>
													</div>
												</div>
												<!-- Section 7 Ends  -->
												<!-- Section 8 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Any Other Points:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[3]) && isset($stepData[3]['Any Other Points Points'])) ? $stepData[3]['Any Other Points Points'] : '-';?>
													</div>
												</div>
												<div class="content_main_title_wrapper col-xs-12">
													<label>Remarks:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($progressArr[3]) && isset($progressArr[3]['tran_remarks'])) ? $progressArr[3]['tran_remarks'] : '-';?>
													</div>
												</div>
												<!-- Section 8 Ends  -->
												<!-- Section Uploaded Images Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<div class="col-xs-12 img-uploads-wrapper">
														<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
														<!-- IMAGE GALLERY CODE STARTS HERE -->
														<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
														<section id="gallery">
															<div id="image-gallery">
																<?php
																	$filesArr = [];
																	$filesArr = explode(',', @$stepData[3]['image']);
																	if(count($filesArr) >= 1){
																		foreach($filesArr as $val){
																			$imgname = trim($val);
																			$imgpath = IMAGE_VIEW_PATH ."projects/$id/progress/Technical Check/$imgname";
																			$imglink = IMG_UPLOAD_PATH ."/projects/$id/progress/Technical Check/$imgname";
																			if(file_exists($imglink) && $imgname != ''){
																?>				
																				<div class="image">
																					<div class="img-wrapper">
																						<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																						<div class="img-overlay">
																							<i class="fa fa-plus-circle" aria-hidden="true"></i>
																						</div>
																					</div>
																				</div>
																<?php
																			}
																		}
																	}
																?>
															</div><!-- End image gallery -->
														</section>
														<!-- IMAGE GALLERY CODE ENDS HERE -->
													</div>
												</div>
											</div>
										</div>
										<!-- SITE UPDATES: STEP1 CODE ENDS HERE  -->	

										<!-- SITE UPDATES: STEP2 CODE STARTS HERE  -->
										<div class="step2 hide form-box">
											<div class="form-bottom">
												<div class="content_main_title_wrapper col-xs-12">
													<label>Remarks:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($progressArr[4]) && isset($progressArr[4]['tran_remarks'])) ? $progressArr[4]['tran_remarks'] : '-';?>
													</div>
												</div>
												<!-- Section Uploaded Images Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<div class="col-xs-12 img-uploads-wrapper">
														<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
														<!-- IMAGE GALLERY CODE STARTS HERE -->
														<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
														<section id="gallery">
															<div id="image-gallery">
																<?php
																	$filesArr = [];
																	$filesArr = explode(',', @$stepData[4]['image']);
																	if(count($filesArr) >= 1){
																		foreach($filesArr as $val){
																			$imgname = trim($val);
																			$imgpath = IMAGE_VIEW_PATH ."projects/$id/progress/Marking of Points/$imgname";
																			$imglink = IMG_UPLOAD_PATH ."/projects/$id/progress/Marking of Points/$imgname";
																			if(file_exists($imglink) && $imgname != ''){
																?>				
																				<div class="image">
																					<div class="img-wrapper">
																						<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																						<div class="img-overlay">
																							<i class="fa fa-plus-circle" aria-hidden="true"></i>
																						</div>
																					</div>
																				</div>
																<?php
																			}
																		}
																	}
																?>
															</div><!-- End image gallery -->
														</section>
														<!-- IMAGE GALLERY CODE ENDS HERE -->
													</div>
												</div>
												<!-- Section Uploaded Images Ends -->
											</div>
										</div>
										<!-- SITE UPDATES: STEP2 CODE ENDS HERE  -->

										<!-- SITE UPDATES: STEP3 CODE STARTS HERE  -->
										<div class="step3 hide form-box">
											<div class="form-bottom">
												<div class="content_main_title_wrapper col-xs-12">
													<label>Marking As per Drawing:</label>
													<div class="col-xs-12 site-view-only-wrapper">
													<?= (isset($stepData[5]) && isset($stepData[5]['Marking As per Drawing'])) ? $stepData[5]['Marking As per Drawing'] : '-';?>
													</div>
												</div>
												<!-- Section 1 Ends  -->
												<!-- Section 2 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Markings on site:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[5]) && isset($stepData[5]['Markings on site'])) ? $stepData[5]['Markings on site'] : '-';?>
													</div>
												</div>
												<!-- Section 2 Ends  -->
												<!-- Section 3 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
														<label>Remarks:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($progressArr[5]) && isset($progressArr[5]['tran_remarks'])) ? $progressArr[5]['tran_remarks'] : '-';?>
													</div>
												</div>
												<!-- Section Uploaded Images Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<div class="col-xs-12 img-uploads-wrapper">
														<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
														<!-- IMAGE GALLERY CODE STARTS HERE -->
														<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
														<section id="gallery">
															<div id="image-gallery">
																<?php
																	$filesArr = [];
																	$filesArr = explode(',', @$stepData[5]['image']);
																	if(count($filesArr) >= 1){
																		foreach($filesArr as $val){
																			$imgname = trim($val);
																			$imgpath = IMAGE_VIEW_PATH ."projects/$id/progress/Final Measurements/$imgname";
																			$imglink = IMG_UPLOAD_PATH ."/projects/$id/progress/Final Measurements/$imgname";
																			if(file_exists($imglink) && $imgname != ''){
																?>				
																				<div class="image">
																					<div class="img-wrapper">
																						<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																						<div class="img-overlay">
																							<i class="fa fa-plus-circle" aria-hidden="true"></i>
																						</div>
																					</div>
																				</div>
																<?php
																			}
																		}
																	}
																?>
															</div><!-- End image gallery -->
														</section>
														<!-- IMAGE GALLERY CODE ENDS HERE -->
													</div>
												</div>
											</div>
										</div>								
										<!-- SITE UPDATES: STEP3 CODE ENDS HERE  -->

										<!-- SITE UPDATES: STEP4 CODE STARTS HERE  -->
										<div class="step4 hide form-box">
											<div class="form-bottom">
												<div class="content_main_title_wrapper col-xs-12">
													<label>Painting Points:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[6]) && isset($stepData[6]['Painting Points'])) ? $stepData[6]['Painting Points'] : '-';?>
													</div>
												</div>
												<!-- Section 1 Ends  -->
												<!-- Section 2 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Floor Polishing Points:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[6]) && isset($stepData[6]['Floor Polishing Points'])) ? $stepData[6]['Floor Polishing Points'] : '-';?>
													</div>
												</div>
												<!-- Section 2 Ends  -->
												<!-- Section 3 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Cleanliness on the site:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[6]) && isset($stepData[6]['Cleanliness on the site'])) ? $stepData[6]['Cleanliness on the site'] : '-';?>
													</div>
												</div>
												<!-- Section 3 Ends  -->
												<!-- Section 4 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Doors and Windows Fitted:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[6]) && isset($stepData[6]['Doors and Windows Fitted'])) ? $stepData[6]['Doors and Windows Fitted'] : '-';?>
													</div>
												</div>
												<!-- Section 4 Ends  -->
												<!-- Section 5 Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<label>Spraying on sites:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($stepData[6]) && isset($stepData[6]['Spraying on sites'])) ? $stepData[6]['Spraying on sites'] : '-';?>
													</div>
												</div>
												<!-- Section 5 Ends  -->

												<div class="content_main_title_wrapper col-xs-12">
													<label>Remarks:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($progressArr[6]) && isset($progressArr[6]['tran_remarks'])) ? $progressArr[6]['tran_remarks'] : '-';?>
													</div>
												</div>
												<!-- Section Uploaded Images Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<div class="col-xs-12 img-uploads-wrapper">
														<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
														<!-- IMAGE GALLERY CODE STARTS HERE -->
														<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
														<section id="gallery">
															<div id="image-gallery">
																<?php
																	$filesArr = [];
																	$filesArr = explode(',', @$stepData[6]['image']);
																	if(count($filesArr) >= 1){
																		foreach($filesArr as $val){
																			$imgname = trim($val);
																			$imgpath = IMAGE_VIEW_PATH ."projects/$id/progress/PDI/$imgname";
																			$imglink = IMG_UPLOAD_PATH ."/projects/$id/progress/PDI/$imgname";
																			if(file_exists($imglink) && $imgname != ''){
																?>				
																				<div class="image">
																					<div class="img-wrapper">
																						<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																						<div class="img-overlay">
																							<i class="fa fa-plus-circle" aria-hidden="true"></i>
																						</div>
																					</div>
																				</div>
																<?php
																			}
																		}
																	}
																?>
															</div><!-- End image gallery -->
														</section>
														<!-- IMAGE GALLERY CODE ENDS HERE -->
													</div>
												</div>
											</div>
										</div>								
										<!-- SITE UPDATES: STEP4 CODE ENDS HERE  -->

										<!-- SITE UPDATES: STEP5 CODE STARTS HERE  -->
										<div class="step5 hide form-box">
											<div class="form-bottom">
												<div class="content_main_title_wrapper col-xs-12">
														<label>Remarks:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($progressArr[7]) && isset($progressArr[7]['tran_remarks'])) ? $progressArr[7]['tran_remarks'] : '-';?>
													</div>
												</div>
												<!-- Section Uploaded Images Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<div class="col-xs-12 img-uploads-wrapper">
														<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
														<!-- IMAGE GALLERY CODE STARTS HERE -->
														<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
														<section id="gallery">
															<div id="image-gallery">
																<?php
																	$filesArr = [];
																	$filesArr = explode(',', @$stepData[7]['image']);
																	if(count($filesArr) >= 1){
																		foreach($filesArr as $val){
																			$imgname = trim($val);
																			$imgpath = IMAGE_VIEW_PATH ."projects/$id/progress/Installation/$imgname";
																			$imglink = IMG_UPLOAD_PATH ."/projects/$id/progress/Installation/$imgname";
																			if(file_exists($imglink) && $imgname != ''){
																?>				
																				<div class="image">
																					<div class="img-wrapper">
																						<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																						<div class="img-overlay">
																							<i class="fa fa-plus-circle" aria-hidden="true"></i>
																						</div>
																					</div>
																				</div>
																<?php
																			}
																		}
																	}
																?>
															</div><!-- End image gallery -->
														</section>
														<!-- IMAGE GALLERY CODE ENDS HERE -->
													</div>
												</div>
											</div>
										</div>
										<!-- SITE UPDATES: STEP5 CODE ENDS HERE  -->

										<!-- SITE UPDATES: STEP6 CODE STARTS HERE  -->
										<div class="step6 hide form-box">
											<div class="form-bottom">
												<div class="content_main_title_wrapper col-xs-12">
													<label>Remarks:</label>
													<div class="col-xs-12 site-view-only-wrapper">
														<?= (isset($progressArr[8]) && isset($progressArr[8]['tran_remarks'])) ? $progressArr[8]['tran_remarks'] : '-';?>
													</div>
												</div>
												<!-- Section Uploaded Images Starts -->
												<div class="content_main_title_wrapper col-xs-12">
													<div class="col-xs-12 img-uploads-wrapper">
														<label class="main_title_label col-xs-12"><i class="fa fa-image" aria-hidden="true"></i> Uploaded Images:</label>
														<!-- IMAGE GALLERY CODE STARTS HERE -->
														<!-- ORIGINAL CODE LINK: https://bootsnipp.com/snippets/ZXyEz  -->
														<section id="gallery">
															<div id="image-gallery">
																<?php
																	$filesArr = [];
																	$filesArr = explode(',', @$stepData[8]['image']);
																	if(count($filesArr) >= 1){
																		foreach($filesArr as $val){
																			$imgname = trim($val);
																			$imgpath = IMAGE_VIEW_PATH ."projects/$id/progress/Finished/$imgname";
																			$imglink = IMG_UPLOAD_PATH ."/projects/$id/progress/Finished/$imgname";
																			if(file_exists($imglink) && $imgname != ''){
																?>				
																				<div class="image">
																					<div class="img-wrapper">
																						<a href="<?= $imgpath;?>" title="<?= $imgname;?>"><img src="<?= $imgpath;?>" class="img-responsive"></a>
																						<div class="img-overlay">
																							<i class="fa fa-plus-circle" aria-hidden="true"></i>
																						</div>
																					</div>
																				</div>
																<?php
																			}
																		}
																	}
																?>
															</div><!-- End image gallery -->
														</section>
														<!-- IMAGE GALLERY CODE ENDS HERE -->
													</div>
												</div>
											</div>
										</div>
										<!-- SITE UPDATES: STEP6 CODE ENDS HERE  -->
								
									</div>
								</div>
								<!-- MAIN TABS MAP CONTAINER WRAPPER CODE ENDS HERE  -->	
							
						<?php 	} ?>
							<!-- MAIN PROJECT VIEW DETAILS CODE ENDS HERE  -->
						</div>
						<!-- SITE UPDATES CODE ENDS HERE -->
						
					</div>
					<!-- Bootstrap Accordion -->
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>
					<!-- /Bootstrap Accordion -->
					<!-- PROJECT VIEW PAGE CODEE ENDS HERE  -->
				</div>
			</div>
		</div>
	</div>
</div>