<?php @extract($data['data']); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="myModalLabel"><?= ucwords($action); ?> Project</h4>
</div>
<form role="form" name="frm_approval" id="frm_approval" method="post" action="#">
	<div class="modal-body">
		<div class="row form-group">
			<div class="col-xs-12">
				<div class="form-group col-xs-12 pull-left">
					<input type="text" class="form-control" placeholder="Remarks" name="remarks" required="" aria-required="true"> <br />
				</div>
			</div>
		</div>
		<div class='response' style="display:none;"></div>
	</div>
	
	<div class="modal-footer">
		<div class="row form-group">
			<div class="form-group col-sm-12 col-xs-12 pull-left">
				<input type='hidden' name='hdn_project_ids' value="<?= $project_id;?>" readonly />
				<input type='hidden' name='hdn_status_ids' value="<?= (strtolower(trim($action)) == 'approve') ? 7 : 8 ;?>" readonly />
				<button type="button" class="loginBtn btn_submit btn_submit_approval">Submit</button>
				<button type="button" class="cancel-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
			</div>
		</div>
	</div>
</form>
<script src="./../assets/js/lib/project.js"></script>