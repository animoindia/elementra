<?php 
	@extract($data['data']); 
	$cnt_project = (isset($result)) ? count($result) : 'No '; 
?>
<div class="header"> 
	<h1 class="page-header">Projects</h1>
	<!-- DROPDOWN MODAL WRAPPER CODE STARTS HERE -->
	<?php if($_SESSION['COMPANY_ID'] == 1 && $cnt_project > 0 ) { ?>
		<div class="dropdown-selectwrapper">
			<select name='project_action' id='project_action' class="form-control custom-modal-dropdown">
				<option value='' >Select</option>
				<?php if ($_SESSION['DESIG'] == 2) { ?>
					<option value='approve'>Approve</option>
					<option value='reject'>Reject</option>
				<?php } else if ($_SESSION['DESIG'] == 3) { ?>
					<option value='tc'>Technical Check</option>
					<option value='mp'>Marking of Points</option>
					<option value='fm'>Final Measurements</option>
					<option value='pdi'>PDI</option>
					<option value='installation'>Installation</option>
					<option value='finished'>Finished</option>
				<?php } else {?>
					<!-- option value='req_approval'>Send for Approval</option -->
					<option value='assignment'>Assign Supervisor</option>
				<?php } ?>
			</select>
		</div>
	<?php } ?>
	<!-- DROPDOWN MODAL WRAPPER CODE ENDS HERE -->
	<?php if($_SESSION['COMPANY_ID'] != 1) { ?>
		<div class="create-project-wrapper">
			<a href="\project/new" class="" onclick="$('.div_loading_image').show();">ADD PROJECT</a>
		</div>
	<?php } ?>
	<ol class="breadcrumb">
		<li><a href="\" onclick="$('.div_loading_image').show();">Home</a></li>
		<li><a href="\project/list" onclick="$('.div_loading_image').show();">Projects</a></li>
		<li class="active">Data</li>
	</ol> 
</div>
<div id="page-inner"> 
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			   <div class="panel-heading">
					Found <?= $cnt_project; ?> Project
				</div>
				
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
								<?php if($_SESSION['COMPANY_ID'] == 1) { ?>
									<th>.</th>
								<?php } ?>
									<th>#</th>
									<th>Type</th>
									<th>For</th>
									<th>Client Details</th>
									<th>Created By</th>
									<th>Created Date</th>
									<th>Progress</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								if($cnt_project > 0) { 
									foreach($result as $key => $value) {
										@extract($value);
										
										$row_class = ($id%2 == 0) ? 'odd' : 'even';
										$curr_status_id = (isset($log_status_id) && ($log_status_id > 0) && $status_id != 1) ? $log_status_id : $status_id;
										//$curr_status = (isset($log_status_id) && $log_status_id > 0) ? ucwords($log_status) : ucwords($status);
										$curr_status = ucwords($status);
										$curr_step = (isset($log_step)) ? ucwords($log_step).'<br/>'.ucwords($log_status) : '-';
										$curr_step_id = (isset($log_step_id)) ? $log_step_id : 0;
							?>
										<tr class="<?= $row_class;?> ">
											<?php if($_SESSION['COMPANY_ID'] == 1) { ?>
												<td>
													<?php if($curr_status_id != 1 && $curr_step_id != 8) { ?>
														<input type='checkbox' class='prj_chkbox' value="<?= $id;?>" data-status="<?= $curr_status_id;?>" data-step="<?= $curr_step_id;?>"/>
													<?php } ?>
												</td>
											<?php } ?>
											<td><a href="\project/view?prj=<?= $id;?>" onclick="$('.div_loading_image').show();"><?= @date('#Ymd', strtotime($created_at)).@$id;?></a></td>
											<td><?= @$type;?></td>
											<td><?= @$project_for;?></td>
											<td>
												<?= @$client_name;?>
												<?php if($_SESSION['COMPANY_ID'] != 1) {  ?>
													<br/> <?= @$client_contact;?>
												<?php } ?>
											</td>
											<td><?= @$user_name;?><br/><?= @$company;?></td>
											<td><?= @date('d-m-Y', strtotime($created_at));?></td>
											<td><?= @$curr_step;?></td>
											<td>
												<?php 
													if($curr_status_id == 8) {
														echo ucwords($log_status);
													} else if($curr_step_id == 8) {
														echo 'Completed';
													} else if($curr_step_id >= 1 && $curr_step_id < 7) {
														echo 'In Process';
													} else {
														echo $curr_status;
													}
													
													if(trim(strtolower($curr_status)) == 'drafted' && $created_by == $_SESSION['USER_ID']) { ?>
														<a class="" href="\project/edit?prj=<?= $id; ?>" onclick="$('.div_loading_image').show();">
															<i class="fa fa-pencil-square-o info" aria-hidden="true" title="Edit Project"></i>
														</a>
												<?php } ?>
											</td>
											
										</tr>
							<?php 
									} 
								}
							?>
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
			<!--  end  Context Classes  -->
		</div>
	</div>
	<!-- /. ROW  -->
</div>