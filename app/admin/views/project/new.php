<?php 
	@extract($data['data']); 
?>
<!-- Breadcrumbs-->
<div class="header"> 
	<h1 class="page-header">Projects</h1>
	<ol class="breadcrumb">
		<li><a href="\" onclick="$('.div_loading_image').show();">Home</a></li>
		<li><a href="\project/list" onclick="$('.div_loading_image').show();">Projects</a></li>
		<li class="active" onclick="$('.div_loading_image').show();">New</li>
	</ol> 
</div>
<div id="page-inner">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="card-title">
						<div class="title">PROJECT FORM DETAILS</div>
					</div>
				</div>
				<div class="panel-body">
					<!-- CREATE PAGE CODE WRAPPER STARTS HERE -->
					<form role="form" name="frm_project_new" id="frm_project_new" method="post" action="#" enctype="multipart/form-data" class="registration-form">
						<div class="panel-body">
							<div class="col-xs-12 form-box"> 
								<fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 1 / 9</h4>
											<p class="form-title">Client & Site Details</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-check-square-o"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-row">
											<div class="form-holder">
												<select class="form-control" id="project_type" name="data[1][project_type]" required >
													<option value=""> Project Type </option>
													<?php foreach(unserialize(ARR_CATEGORY) as $key=>$val) { ?>
														<option value="<?= $key; ?>"><?= $val;?></option>
													<?php } ?>
												</select>
											</div>
											<div class="form-holder">
												<select class="form-control" id="project_for" name="data[1][project_for]" required >
													<option value=""> Project For </option>
													<?php foreach(unserialize(ARR_ADDRESSTYPE) as $key=>$val) { ?>
														<option value="<?= $key; ?>"><?= $val;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder">
												<input type="text" class="form-control" value="" name="data[1][client_name]" placeholder="Client's Name :" required />
											</div>
											<div class="form-holder">
												<input type="text" class="form-control" value="" name="data[1][client_phone]" placeholder="Phone :" required />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<input type="text" class="form-control" value="" name="data[1][client_address]" placeholder="Site Address :" required />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<input type="text" class="form-control" value="" name="data[1][client_details]" placeholder="Details*" required />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<input type="text" class="form-control" value="" name="data[1][client_remarks]" placeholder="Remarks:" />
											</div>
										</div>                                    
									
										<button type="button" class="btn btn-next">Next</button>
									</div>
								</fieldset>
								<!-- SECTION 1 - ENDS -->


								<!-- SECTION 2 - STARTS -->
								<fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 2 / 9</h4>
											<p class="form-title">Base Material & Shutter Finish</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-check-square-o"></i>
										</div>
									</div>
									<div class="form-bottom">
								
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Core Material</label>
												<input type="text" class="form-control marb15" value="" name="data[2][1][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[2][1][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Base Shutters Finish</label>
												<input type="text" class="form-control marb15" value="" name="data[2][2][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[2][2][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Wall Shutters Finish</label>
												<input type="text" class="form-control marb15" value="" name="data[2][3][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[2][3][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tall Shutters Finish</label>
												<input type="text" class="form-control marb15" value="" name="data[2][4][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[2][4][remarks]" placeholder="Remarks" />
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Loft Unit Finish</label>
												<input type="text" class="form-control marb15" value="" name="data[2][5][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[2][5][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link"   name="image[2][5][]" multiple="">
											</div>
										</div>										
									
										<button type="button" class="btn btn-next">Next</button>
										<button type="button" class="btn btn-previous">Previous</button>
									</div>
								</fieldset>
								<!-- SECTION 2 - ENDS -->


								<!-- SECTION 3 - STARTS -->
							   <fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 3 / 9</h4>
											<p class="form-title">Front & Internal Colors</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-check-square-o"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Base Shutter Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][1][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][1][remarks]" placeholder="Remarks" />
												
												<input type="file" class="upload-file-link"   name="image[3][1][]" multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Base Shutter Back Colour </label>
												<input type="text" class="form-control marb15" value="" name="data[3][2][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][2][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][2][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Base Carcass Colour </label>
												<input type="text" class="form-control marb15" value="" name="data[3][3][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][3][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][3][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Wall Shutter Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][4][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][4][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][4][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Wall Shutter Back Colour </label>
												<input type="text" class="form-control marb15" value="" name="data[3][5][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][5][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][5][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Wall Carcass Colour </label>
												<input type="text" class="form-control marb15" value="" name="data[3][6][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][6][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][6][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tall Shutter Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][7][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][7][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][7][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tall Carcass Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][8][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][8][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][8][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tall Cabinet Visible Side Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][9][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][9][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][9][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Drawer Bottom And Back Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][10][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][10][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][10][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Semi Tall Finish</label>
												<input type="text" class="form-control marb15" value="" name="data[3][11][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][11][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][11][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Semi Tall Shutter Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][12][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][12][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][12][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Semi Tall Carcass Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][13][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][13][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][13][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Loft Shutter Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][14][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][14][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][14][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Loft Carcass Colour</label>
												<input type="text" class="form-control marb15" value="" name="data[3][15][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[3][15][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[3][15][]"  multiple="">
											</div>
										</div>
										
										<button type="button" class="btn btn-next">Next</button>
										<button type="button" class="btn btn-previous">Previous</button>
										
									</div>
								</fieldset>
								<!-- SECTION 3 - ENDS -->


								<!-- SECTION 4 - STARTS -->
								<fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 4 / 9</h4>
											<p class="form-title">Heights & Depths</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-handshake-o"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Base Cabinet Depth</label>
												<input type="text" class="form-control marb15" value="" name="data[4][1][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][1][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Base Shutter Height</label>
												<input type="text" class="form-control marb15" value="" name="data[4][2][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][2][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Wall Cabinet Height </label>
												<input type="text" class="form-control marb15" value="" name="data[4][3][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][3][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Wall Cabinet Depth</label>
												<input type="text" class="form-control marb15" value="" name="data[4][4][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][4][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tall Cabinet Depth</label>
												<input type="text" class="form-control marb15" value="" name="data[4][5][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][5][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tall Cabinet Height</label>
												<input type="text" class="form-control marb15" value="" name="data[4][6][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][6][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Skirting Height</label>
												<input type="text" class="form-control marb15" value="" name="data[4][7][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][7][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Skirting Finish</label>
												<input type="text" class="form-control marb15" value="" name="data[4][8][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][8][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[4][8][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Platform Depth</label>
												<input type="text" class="form-control marb15" value="" name="data[4][9][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][9][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Counter Height</label>
												<input type="text" class="form-control marb15" value="" name="data[4][10][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][10][remarks]" placeholder="Remarks" />
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Loft Depth</label>
												<input type="text" class="form-control marb15" value="" name="data[4][11][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][11][remarks]" placeholder="Remarks" />
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Loft Height</label>
												<input type="text" class="form-control marb15" value="" name="data[4][12][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][12][remarks]" placeholder="Remarks" />
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Semi Tall Depth</label>
												<input type="text" class="form-control marb15" value="" name="data[4][13][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][13][remarks]" placeholder="Remarks" />
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Roller Shutter Height</label>
												<input type="text" class="form-control marb15" value="" name="data[4][14][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[4][14][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<button type="button" class="btn btn-next">Next</button>
										<button type="button" class="btn btn-previous">Previous</button>
									</div>
								</fieldset>
								<!-- SECTION 4 - ENDS -->


								<!-- SECTION 5 - STARTS -->
							   <fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 5 / 9</h4>
											<p class="form-title">Hardware Details</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-handshake-o"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Drawer Systems</label>
												<input type="text" class="form-control marb15" value="" name="data[5][1][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[5][1][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Lift Up Systems</label>
												<input type="text" class="form-control marb15" value="" name="data[5][2][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[5][2][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[5][2][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Hinges</label>
												<input type="text" class="form-control marb15" value="" name="data[5][3][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[5][3][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<button type="button" class="btn btn-next">Next</button>
										<button type="button" class="btn btn-previous">Previous</button>
									</div>
								</fieldset>
								<!-- SECTION 5 - ENDS -->


								<!-- SECTION 6 - STARTS -->
								<fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 6 / 9</h4>
											<p class="form-title">Accessories</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-handshake-o"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Dustbin</label>
												<input type="text" class="form-control marb15" value="" name="data[6][1][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][1][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][1][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Detergent </label>
												<input type="text" class="form-control marb15" value="" name="data[6][2][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][2][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][2][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Cutlery Tray</label>
												<input type="text" class="form-control marb15" value="" name="data[6][3][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][3][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][3][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Dishrack </label>
												<input type="text" class="form-control marb15" value="" name="data[6][4][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][4][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][4][]"  multiple="">
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Pullout Baskets</label>
												<input type="text" class="form-control marb15" value="" name="data[6][5][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][5][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][5][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Handle (Base) </label>
												<input type="text" class="form-control marb15" value="" name="data[6][6][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][6][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][6][]"  multiple="">
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Handle (1st Layer Wall Cabinet)</label>
												<input type="text" class="form-control marb15" value="" name="data[6][7][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][7][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][7][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Handle (Tall) </label>
												<input type="text" class="form-control marb15" value="" name="data[6][8][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][8][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][8][]"  multiple="">
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Profile Details</label>
												<input type="text" class="form-control marb15" value="" name="data[6][9][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][9][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][9][]"  multiple="">
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tall Units </label>
												<input type="text" class="form-control marb15" value="" name="data[6][10][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][10][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][10][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Corner Accessories </label>
												<input type="text" class="form-control marb15" value="" name="data[6][11][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][11][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][11][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Bottle Trolly </label>
												<input type="text" class="form-control marb15" value="" name="data[6][12][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][12][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][12][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Semi Tall Units</label>
												<input type="text" class="form-control marb15" value="" name="data[6][13][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][13][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][13][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Corner Units </label>
												<input type="text" class="form-control marb15" value="" name="data[6][14][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][14][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][14][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Handle For Loft </label>
												<input type="text" class="form-control marb15" value="" name="data[6][15][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][15][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][15][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Vegetable Drawer </label>
												<input type="text" class="form-control marb15" value="" name="data[6][16][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][16][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][16][]"  multiple="">
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Additional Accessories </label>
												<input type="text" class="form-control marb15" value="" name="data[6][17][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[6][17][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[6][17][]"  multiple="">
											</div>
										</div>

										<button type="button" class="btn btn-next">Next</button>
										<button type="button" class="btn btn-previous">Previous</button>
									</div>
								</fieldset>
								<!-- SECTION 6 - ENDS -->


								<!-- SECTION 7 - STARTS -->
								<fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 7 / 9</h4>
											<p class="form-title">Appliances And Counter Details</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-handshake-o"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Chimney</label>
												<input type="text" class="form-control marb15" value="" name="data[7][1][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][1][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Hob</label>
												<input type="text" class="form-control marb15" value="" name="data[7][2][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][2][remarks]" placeholder="Remarks" />
											</div>
										</div>

										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Sink</label>
												<input type="text" class="form-control marb15" value="" name="data[7][3][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][3][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[7][3][]"  multiple="">
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tap</label>
												<input type="text" class="form-control marb15" value="" name="data[7][4][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][4][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[7][4][]"  multiple="">
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Micro</label>
												<input type="text" class="form-control marb15" value="" name="data[7][5][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][5][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Oven</label>
												<input type="text" class="form-control marb15" value="" name="data[7][6][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][6][remarks]" placeholder="Remarks" />
											 </div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Dishwasher</label>
												<input type="text" class="form-control marb15" value="" name="data[7][7][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][7][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Fridge</label>
												<input type="text" class="form-control marb15" value="" name="data[7][8][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][8][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[7][8][]"  multiple="">
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Any Other, Appliance</label>
												<input type="text" class="form-control marb15" value="" name="data[7][9][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][9][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Counter Top</label>
												<input type="text" class="form-control marb15" value="" name="data[7][10][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][10][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Granite Edge Type For Counter</label>
												<input type="text" class="form-control marb15" value="" name="data[7][11][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[7][11][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[7][11][]"  multiple="">
											</div>
										</div>
										<button type="button" class="btn btn-next">Next</button>
										<button type="button" class="btn btn-previous">Previous</button>
									</div>
								</fieldset>
								<!-- SECTION 7 - ENDS -->


								<!-- SECTION 8 - STARTS -->
							   <fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 8 / 9</h4>
											<p class="form-title">Special Remarks</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-handshake-o"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Led Light Details</label>
												<input type="text" class="form-control marb15" value="" name="data[8][1][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[8][1][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Led Light Placements</label>
												<input type="text" class="form-control marb15" value="" name="data[8][2][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[8][2][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Base Cabinet Shelves</label>
												<input type="text" class="form-control marb15" value="" name="data[8][3][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[8][3][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Wall Cabinet Shelves</label>
												<input type="text" class="form-control marb15" value="" name="data[8][4][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[8][4][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Tall Cabinet Shelves</label>
												<input type="text" class="form-control marb15" value="" name="data[8][5][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[8][5][remarks]" placeholder="Remarks" />
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>Aqua guard Cabinet</label>
												<input type="text" class="form-control marb15" value="" name="data[8][6][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[8][6][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[8][6][]"  multiple="">
											</div>
										</div>
										
										<button type="button" class="btn btn-next">Next</button>
										<button type="button" class="btn btn-previous">Previous</button>
									</div>
								</fieldset>
								<!-- SECTION 8 - ENDS -->


								<!-- SECTION 9 - STARTS -->
								<fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h4 class="steps-number">Step 9 / 9</h4>
											<p class="form-title">Drawings With Markings</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-handshake-o"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>2D Autocad With Measurements and Detials</label>
												<input type="text" class="form-control marb15" value="" name="data[9][1][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[9][1][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[9][1][]"  multiple="">
											</div>
										</div>
										<div class="form-row">
											<div class="form-holder w-100">
												<i class="fa fa-asterisk" aria-hidden="true"></i> &nbsp; 
												<label>3D Drawings</label>
												<input type="text" class="form-control marb15" value="" name="data[9][2][details]" placeholder="Details*" required />
												<input type="text" class="form-control marb15" value="" name="data[9][2][remarks]" placeholder="Remarks" />
												<input type="file" class="upload-file-link" name="image[9][2][]"  multiple="">
											</div>
										</div>
										<div class='response' style="display:none;"></div>
										<button type="submit" class="btn btn-submit" name="submit" value="2">Submit</button>
										<button type="submit" class="loginBtn btn btn-submit" name="submit" value="1">Save</button>
										<button type="button" class="btn btn-previous">Previous</button>
									</div>
								</fieldset>
								<!-- SECTION 9 - ENDS -->
							</div>
						</div>
					</div> 
				</form>
			</div>
		</div>
	</div>
</div>
