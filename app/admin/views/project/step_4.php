<?php @extract($data['data']); ?>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">
			PDI
		</h4>
	</div>
	<form role="form" name="frm_step" id="frm_step" action='#' method="post" class="frm_step" enctype="multipart/form-data">
		<div class="modal-body">
			<!-- FORM STEP 1 CODE STARTS HERE  -->
			<fieldset>
				<div class="form-top">
					<div class="form-top-left">
						<h4 class="steps-number"></h4>
						
					</div>
					<div class="form-top-right">
						<i class="fa fa-check-square-o"></i>
					</div>
				</div>
				<div class="form-bottom">
					<div class="form-group">
						<label class="sr-only" for="form-about-yourself">Painting Points</label>
						<textarea placeholder="Painting..." class="form-about-yourself form-control"  name="data[Painting Points]" required="" aria-required="true"></textarea>
					</div>
					<div class="form-group">
						<label class="sr-only" for="form-site-measurement">Floor Polishing Points</label>
						<textarea placeholder="Floor Polishing..." class="form-about-yourself form-control"  name="data[Floor Polishing Points]" required="" aria-required="true"></textarea>
					</div>
					<div class="form-group">
						<label class="sr-only" for="form-about-yourself">Cleanliness on the site</label>
						<textarea placeholder="Cleanliness on the site..." class="form-about-yourself form-control"  name="data[Cleanliness on the site]" required="" aria-required="true"></textarea>
					</div>
					<div class="form-group">
						<label class="sr-only" for="form-about-yourself">Doors and Windows Fitted</label>
						<textarea placeholder="Doors and Windows Fitted..." class="form-about-yourself form-control"  name="data[Doors and Windows Fitted]" required="" aria-required="true"></textarea>
					</div>
					<div class="form-group">
						<label class="sr-only" for="form-about-yourself">Spraying on sites</label>
						<textarea placeholder="Spraying on sites..." class="form-about-yourself form-control" name="data[Spraying on sites]" required="" aria-required="true"></textarea>
					</div>
					<div class="form-group">
						<label class="sr-only" for="form-about-yourself">Remarks</label>
						<textarea placeholder="Any Other Points..." class="form-about-yourself form-control"  name="remarks" required="" aria-required="true"></textarea>
					</div>
					<input type="file" class="upload-file-link color-pink" name="image[]" id="fileToUpload" multiple="">
					
				</div>
			</fieldset>	
			<div class='response' style="display:none;"></div>
		</div>
	
		<div class="modal-footer">
			<div class="row form-group">
				<div class="form-group col-sm-12 col-xs-12 pull-left">
					<input type='hidden' name='hdn_project_id' value="<?= @$project_id;?>" readonly />
					<input type='hidden' name='hdn_step_id' value="6" readonly />
					<button type="submit" class="loginBtn btn_submit btn_submit_step">Submit</button>
					<button type="button" class="cancel-btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				</div>
			</div>
		</div>
	</form>
	<script src="./../assets/js/lib/project.js"></script>
