﻿<?php
	@extract($data);
?>
<!-- Breadcrumbs-->
<div class="header"> 
	<h1 class="page-header">Feedback</h1>
	<ol class="breadcrumb">
		<li><a href="/" onclick="$('.div_loading_image').show();">Home</a></li>
		<li><a href="\feedback/new" onclick="$('.div_loading_image').show();">Feedback</a></li>
		<li class="active">New</li>
	</ol> 
</div>
<div id="page-inner"> 
	<div class="row">
		<div class="col-md-12">
			 <!--    Context Classes  -->
			<div class="panel panel-default">
			   <div class="panel-heading">
					New Feedback
				</div>
                <!-- FEEDBACK QUESTIONS CODE STARTS HERE  -->
                <div class="col-xs-12 feedback-wrapper">
                    <div class="panel panel-default">
                        <div class="panel-body">
							<form role='form' class="" id="frm_feedback" name="frm_feedback" method="post" action="#" enctype="multipart/form-data">
								<div class='response col-xs-12 feedback-question-wrapper' style="display:none;"></div>
								<!-- SELECT PROJECT FOR FEEDBACK CODE STARTS HERE -->
								<div class="col-xs-12 project-list-wrapper">
									<label class="project-title">Please select your project from the list: </label>
									
									<!-- ANIMATED PROJECT LIST DROPDOWN CODE STARTS HERE  -->
									<div class="navbar navbar-default navbar-static-top" role="navigation">
										<select name='project_id' id="project_id" class="form-control" required>
											<option value="">Select</option>
											<?php if(isset($data['result']) && count($data['result']) > 0) { ?>
												<?php foreach($data['result'] as $key=>$val) { ?>
														<option value="<?= @date('Ymd', strtotime($val['created_at'])).'#'.@$val['id'];?>"><?= @date('#Ymd', strtotime($val['created_at'])).@$val['id'];?></option>
												<?php } 
												} ?>
										</select>
												
									</div>
									<!-- ANIMATED PROJECT LIST DROPDOWN CODE ENDS HERE  -->
								</div>
								<!-- SELECT PROJECT FOR FEEDBACK CODE ENDS HERE -->

								<!-- QUESTIONS & STAR RATINGS FOR FEEDBACK CODE STARTS HERE -->
								<div class="col-xs-12 feedback-question-wrapper">
									<ol class="col-xs-12 ordered-list">
										
										
										
										<!-- QUESTION 1 NEW *Code Starts Here -->
										<li class="col-xs-12 ordered-question-list">
										
											<div class="col-xs-12 question-ratings-wrapper">
												<label class="project-inner-title">Technical know-how of the site supervisor ? </label>
												<!-- STAR RATING NEW CODE URL: http://demo.expertphp.in/bootstrap-star-rating-plugin -->
												<input class="rating rating-loading" value="0" data-min="0" data-max="5" data-step="0.5" data-size="xs" id="data[1][feedback]" name="data[1][feedback]" required />
												<textarea class="feedbackCommentBox form-control" rows="3" id="data[1][comment]" name="data[1][comment]" value="" required></textarea>
											</div>										
										
										</li>
										<!-- QUESTION 1 NEW *Code Ends Here -->
										
										
										
										
										<!-- QUESTION 2 NEW *Code Starts Here -->
										<li class="col-xs-12 ordered-question-list">
										
											<div class="col-xs-12 question-ratings-wrapper">
												<label class="project-inner-title">Communication of the site supervisor with Designer ? </label>
												<!-- STAR RATING NEW CODE URL: http://demo.expertphp.in/bootstrap-star-rating-plugin -->
												<input class="rating rating-loading" value="0" data-min="0" data-max="5" data-step="0.5" data-size="xs" id="data[2][feedback]" name="data[2][feedback]" required />
												<textarea class="feedbackCommentBox form-control" rows="3" id="data[2][comment]" name="data[2][comment]" value="" required></textarea>
											</div>										
										
										</li>
										<!-- QUESTION 2 NEW *Code Ends Here -->
										
										
										
										
										<!-- QUESTION 3 NEW *Code Starts Here -->
										<li class="col-xs-12 ordered-question-list">
										
											<div class="col-xs-12 question-ratings-wrapper">
												<label class="project-inner-title">Alignment and Handover with cleaning ? </label>
												<!-- STAR RATING NEW CODE URL: http://demo.expertphp.in/bootstrap-star-rating-plugin -->
												<input class="rating rating-loading" value="0" data-min="0" data-max="5" data-step="0.5" data-size="xs" id="data[3][feedback]" name="data[3][feedback]" required />
												<textarea class="feedbackCommentBox form-control" rows="3" id="data[3][comment]" name="data[3][comment]" value="" required></textarea>
											</div>										
										
										</li>
										<!-- QUESTION 3 NEW *Code Ends Here -->
										
										
										
										
										<!-- QUESTION 4 NEW *Code Starts Here -->
										<li class="col-xs-12 ordered-question-list">
										
											<div class="col-xs-12 question-ratings-wrapper">
												<label class="project-inner-title">Overall Site Handling of the site supervisor ? </label>
												<!-- STAR RATING NEW CODE URL: http://demo.expertphp.in/bootstrap-star-rating-plugin -->
												<input class="rating rating-loading" value="0" data-min="0" data-max="5" data-step="0.5" data-size="xs" id="data[4][feedback]" name="data[4][feedback]" required />
												<textarea class="feedbackCommentBox form-control" rows="3" id="data[4][comment]" name="data[4][comment]" value="" required></textarea>
											</div>										
										
										</li>
										<!-- QUESTION 4 NEW *Code Ends Here -->
										
										
										
										<!-- QUESTION 5 NEW *Code Starts Here -->
										<li class="col-xs-12 ordered-question-list last_entry">
											<label class="project-inner-title" style="width:100%;">Other Comments: </label>
											<textarea name="data[5][comment]" class="feedbackCommentBox form-control" rows="3"></textarea>
										</li>
										<!-- QUESTION 5 NEW *Code Ends Here -->
									</ol>
									<div class="col-xs-12 feedback-submit">
										<input type='hidden' name="flag" value="feedback" readonly />
										<button type="submit" class="loginBtn btn_submit btn_submit_feedback">SUBMIT</button>
										<button type="button" class="cancel-btn feedback-cancel-btn-mobile" data-dismiss="modal" aria-hidden="true">Cancel</button>
									</div>
									
								</div>
							</form>
                            <!-- QUESTIONS & STAR RATINGS FOR FEEDBACK CODE ENDS HERE -->

                        </div>
                    </div>
                </div>
                <!-- FEEDBACK QUESTIONS CODE ENDS HERE  -->
			</div>
		</div>	
	</div>
</div>