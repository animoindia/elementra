<?php @extract($data['data']); ?>
<div class="header"> 
	<h1 class="page-header">Feedback</h1>
	<div class="create-project-wrapper">
		<a href="\feedback/new" class="" onclick="$('.div_loading_image').show();">New Feedback</a>
	</div>
	<ol class="breadcrumb">
		<li><a href="/" onclick="$('.div_loading_image').show();">Home</a></li>
		<li><a href="\feedback/list" onclick="$('.div_loading_image').show();">Feedback</a></li>
		<li class="active">Data</li>
	</ol> 
</div>
<div id="page-inner"> 
	<div class="row">
		<div class="col-md-12">
			 <!--    Context Classes  -->
			<div class="panel panel-default">
			   
				<div class="panel-heading">
					Feedback
				</div>
				
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Username</th>
								</tr>
							</thead>
							<tbody>
								<tr class="success">
									<td>1</td>
									<td>Mark</td>
									<td>Otto</td>
									<td>@mdo</td>
								</tr>
								<tr class="info">
									<td>2</td>
									<td>Jacob</td>
									<td>Thornton</td>
									<td>@fat</td>
								</tr>
								<tr class="warning">
									<td>3</td>
									<td>Larry</td>
									<td>the Bird</td>
									<td>@twitter</td>
								</tr>
								<tr class="danger">
									<td>4</td>
									<td>John</td>
									<td>Smith</td>
									<td>@jsmith</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--  end  Context Classes  -->
		</div>
	</div>
	<!-- /. ROW  -->
</div>