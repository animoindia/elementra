<?php

class BaseFeedback extends DBManager
{
	protected $objDBManager;
	protected $objUtil;
	protected $dateTime = '';
	protected $table    = 'tbl_user_master';
	
	public function __construct() {
		$this->objDBManager = new DBManager(); 
		$this->objUtil 		= new Util();
		$this->userID 		= isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : 0;
		$this->dateTime 	= date("Y-m-d H:i:s");
	}	
	
	public function retrieveProjectList($type){ 

		$query = "SELECT T1.project_id, T2.created_at, T2.id 
					from tbl_project_transaction_log T1 
					INNER JOIN tbl_project_master T2 on T2.id = T1.project_id
					LEFT JOIN tbl_feedback_master T3 on T3.project_id = T2.id AND T3.type = :type
					WHERE T1.status_id = 5 AND T2.created_by = :created_by AND T3.id IS NULL ";
		
		$query .= " group by T1.project_id ORDER BY `T2`.`id` ASC";
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'all';
		
		$params['bindValues'][":type"]  = $type;
		$params['bindValues'][":created_by"]  = $this->userID;
		
		return $this->objDBManager->executeSQL($params);
	}
	
	public function create( $data ){
		@extract($data);
		
		$tbl_data = [];
		$tbl_data['project_id']     = $projectId;
		$tbl_data['type']			= $type;
		$tbl_data['data']			= $this->objUtil->encodeData($data);
		$tbl_data['created_at'] 	= $this->dateTime;
		$tbl_data['created_by'] 	= $this->userID;
		
		/** Code to insert data tbl_user_master */
		$columns 	= implode(',', array_keys($tbl_data));
		$values 	= $this->objUtil->arrayKeyPrefix($tbl_data);
		$values 	= implode(',', array_keys($values));
		
		$query 	= "INSERT INTO tbl_feedback_master ($columns) values ($values)";
		
		$params = [];
		$params['action'] = 'INSERT';
		$params['query']  = $query;
		
		foreach($tbl_data as $key => $value){
			$params['bindValues'][":$key"]   = $value;
		}
		
		return $this->objDBManager->executeSQL($params);
		
	}
	
}