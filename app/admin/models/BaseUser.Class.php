<?php

class BaseUser extends DBManager
{
	protected $objDBManager;
	protected $objUtil;
	protected $dateTime = '';
	protected $table    = 'tbl_user_master';
	
	public function __construct() {
		$this->objDBManager = new DBManager();
		$this->objUtil 		= new Util();
		$this->userID 		= isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : 0;
		$this->dateTime 	= date("Y-m-d H:i:s");
	}
	
	public function retrieve($params = array()){ 

		@extract($params);
		
		$query = "SELECT T1.*, T2.name as company, T3.type as user_type, T4.name as designation
					from $this->table T1
					LEFT JOIN tbl_company_master T2 ON T2.id = T1.company_id
					LEFT JOIN tbl_user_type_master T3 ON T3.id = T1.user_type_id
					LEFT JOIN tbl_designation_master T4 ON T4.id = T1.designation_id
					WHERE `T1`.`id` != 1 
				";
		
		if(isset($id))
			$query .= " AND T1.id = :id";
		
		if($_SESSION['USER_TYPE'] != 1 && $flag == 'list'){
			$query .= " AND T1.created_by = :created_by";
		} 
		
		$query .= " ORDER BY `T1`.`created_at`, `T1`.`f_name` ASC";
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = isset($id) ? 'one' : 'all';
		
		if($_SESSION['USER_TYPE'] != 1 && $flag == 'list')
			$params['bindValues'][":created_by"] = $this->userID;
		
		if(isset($id))
			$params['bindValues'][":id"]  = $id;
		
		return $this->objDBManager->executeSQL($params);
	}
	
	public function validateUser( $data ){
		
		@extract($data);
		
		$query = "SELECT * from $this->table where (email = :email OR mobile_no = :mobile_no)";
		
		if(isset($id) && $id > 0) {
			$query .= " AND id <> :user_id";
		}
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'one';
		$params['bindValues'][':email']     = $data['email'];
		$params['bindValues'][':mobile_no'] = $data['mobile_no'];
		
		if(isset($id) && $id > 0) {
			$params['bindValues'][':user_id']  = $id;
		}
		
		$arrResult = $this->objDBManager->executeSQL($params);
		
		if(isset($arrResult['result']) && isset($arrResult['result']['id'])) {
			$arrResult['result'] = [];
			$arrResult['error']  = 'User is already registerd with given email or mobile number';
		} 
		
		return $arrResult;
	}	
	
	
	public function create( $data ){
		
		@extract($data);
		
		$preFix = 'avtar_'.date('dmyhid').'_';
		$files 	= (isset($files) && count($files) > 0) ? $files : '';
		
		if($files != '')
			$data['avtar'] 	= isset($files['name'][0]) ? $preFix.$files['name'][0] : '';
		
		$data = $this->objUtil->sanitizeData($data);
		
		$random_password 	= (mt_rand(strlen($data['f_name'])+2, strlen($data['l_name'])+10).time());
		$activation_key 	= sha1(mt_rand(10000,99999).time().($data['email'].$data['mobile_no']));
		
		$data['password'] 		= $this->objUtil->encryptPassword($random_password);
		$data['activation_key'] = $this->objUtil->encodeData($activation_key);
		$data['created_at'] 	= $this->dateTime;
		$data['created_by'] 	= $this->userID;
		
		/** Code to insert data tbl_user_master */ 
		$columns 	= implode(',', array_keys($data));
		$values 	= $this->objUtil->arrayKeyPrefix($data);
		$values 	= implode(',', array_keys($values));
		
		$query 	= "INSERT INTO $this->table ($columns) values ($values)";
		
		$params = [];
		$params['action'] = 'INSERT';
		$params['query']  = $query;
		
		foreach($data as $key => $value){
			$params['bindValues'][":$key"]   = $value;
		}
		
		$arrResult = $this->objDBManager->executeSQL($params);
		
		$arrResult['activation_key'] = $data['activation_key'];
		$arrResult['random_password'] = $random_password;
		
		if(isset($arrResult['success']) && isset($arrResult['lastInsertedId']) && !empty($files) && isset($files['name'][0])) {
			$arrParams = [];
			$arrParams['directory'][]	= 'users';
			$arrParams['name_prefix']	= $arrResult['lastInsertedId'].'_'.$preFix;
			$arrParams['files']			= $files;
			$uploadedFiles	= $this->objUtil->uploadFiles($arrParams);
		}
		
		return $arrResult;		
	}
	
	public function update( $data ) {
		
		@extract($data);
		
		$preFix = 'avtar_'.date('dmyhid').'_';
		$files 	= (isset($files) && count($files) > 0) ? $files : '';
		
		if(isset($files['name']) && !empty($files['name']))
			$data['avtar'] = $preFix.$files['name'];
		
		$data 	= $this->objUtil->sanitizeData($data);
		
		$data['updated_at'] = $this->dateTime;
		$data['updated_by'] = $this->userID;
		
		$query 	= "UPDATE $this->table SET ";
		
		foreach($data as $key=>$value) {
			$query .= " $key = :$key ,";
		}
		
		$query = rtrim($query, ',');
		
		$query .= " WHERE id = :id";
	
		$params = [];
		$params['action'] = 'UPDATE';
		$params['query']  = $query;
		$params['bindValues'][":id"]  = $id;
		
		foreach($data as $key => $value){
			$params['bindValues'][":$key"]  = $value;
		}
		
		$arrResult = $this->objDBManager->executeSQL($params);
		
		if(isset($arrResult['success']) && isset($arrResult['lastInsertedId']) && !empty($files) && isset($files['name'])) {
			$arrParams = [];
			$arrParams['directory'][]	= 'users';
			$arrParams['name_prefix']	= $id.'_'.$preFix;
			$arrParams['files']			= $files;
			$uploadedFiles	= $this->objUtil->uploadFiles($arrParams);
		}
		
		return $arrResult;
	}
	
	public function setIsActive( $data ) { 
		
		$id   = $data['id'];
		$is_active = $data['is_active'];
		
		$data = [];
		$data['is_active']  = $is_active;
		$data['updated_at'] = $this->dateTime;
		$data['updated_by'] = $this->userID;
		
		$query 	= "UPDATE $this->table SET ";
		
		foreach($data as $key=>$value) {
			$query .= " $key = :$key ,";
		}
		
		$query = rtrim($query, ',');
		
		$query .= " WHERE id = :id";
		
		$params = [];
		$params['action'] = 'UPDATE';
		$params['query']  = $query;
		
		$params['bindValues'][":id"]  = $id;
		foreach($data as $key => $value){
			$params['bindValues'][":$key"]  = $value;
		}
				
		return $this->objDBManager->executeSQL($params);
	}
	
	public function updatePassword( $password ){
		
		$data['password'] 	= $this->objUtil->encryptPassword($password);
		$data['updated_by'] = $this->userID;
		$data['updated_at'] = $this->dateTime;
				
		$query 	= "UPDATE tbl_user_master SET ";
		
		foreach($data as $key=>$value) {
			$query .= " $key = :$key ,";
		}
		
		$query = rtrim($query, ',');
		
		$query .= " WHERE id = :id";
	
		$params = [];
		$params['action'] = 'UPDATE';
		$params['query']  = $query;
		$params['bindValues'][":id"] = $this->userID;
		
		foreach($data as $key => $value){
			$params['bindValues'][":$key"]  = $value;
		}
		
		return $this->objDBManager->executeSQL($params);
	}
	
	public function retrieveUserByType( $params ) {
		
		@extract($params);
		
		$query = "SELECT id, f_name, l_name from tbl_user_master 
					where designation_id = :user_desg 
					AND user_type_id = :user_type  AND company_id = :company_id
					AND is_active = 1";
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'all';
		
		$params['bindValues'][':user_desg'] = $user_desg;
		$params['bindValues'][':user_type'] = 3;
		$params['bindValues'][':company_id'] = 1;
		
		return $this->objDBManager->executeSQL($params);
	}
}