<?php

class BaseCompany extends DBManager
{
	protected $objDBManager;
	protected $objUtil;
	protected $dateTime = '';
	protected $table    = 'tbl_company_master';

	public function __construct() {
		$this->objDBManager = new DBManager();
		$this->objUtil 		= new Util();
		$this->userID 		= isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : 0;
		$this->dateTime 	= date("Y-m-d H:i:s");
	}

	public function create( $data ){

		$preFix = 'logo_'.date('dmyhid').'_';
		$files = (isset($data['files']) && count($data['files']) > 0) ? $data['files'] : '';
		
		$data = $this->objUtil->sanitizeData($data['data']);
		
		$data['logo'] = isset($files['name'][0]) ? $preFix.$files['name'][0] : '';
		$data['created_at'] 	= $this->dateTime;
		$data['created_by'] 	= $this->userID;
		
		/** Code to insert data tbl_company_master */ 
		$columns 	= implode(',', array_keys($data));
		$values 	= $this->objUtil->arrayKeyPrefix($data);
		$values 	= implode(',', array_keys($values));

		$query 	= "INSERT INTO $this->table ($columns) values ($values)";

		$params = [];
		$params['action'] = 'INSERT';
		$params['query']  = $query;

		foreach($data as $key => $value){
			$params['bindValues'][":$key"] = $value;
		}
		
		$arrResult = $this->objDBManager->executeSQL($params);
		
		if(isset($arrResult['success']) && isset($arrResult['lastInsertedId'])){ 
			if(isset($files['name']) && !empty($files['name'])){
				$arrParams = [];
				$arrParams['directory'][]	= 'company';
				$arrParams['name_prefix']	= $arrResult['lastInsertedId'].'_'.$preFix;
				$arrParams['files']			= $files;
				
				$uploadedFiles	= $this->objUtil->uploadFiles($arrParams);

			}
		}

		return $arrResult;
	}

	public function validateCompany( $data ){
		
		@extract($data);

		$query = "SELECT * from $this->table where ( name = :name OR gst_no = :gst_no OR email = :email OR contact_no = :contact_no ) ";

		if(isset($id) && $id > 0) {
			$query .= " AND id <> :company_id";
		}
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'one';
		$params['bindValues'][':name']  = $data['name'];
		$params['bindValues'][':gst_no']  = $data['gst_no'];
		$params['bindValues'][':email']   = $data['email'];
		$params['bindValues'][':contact_no']  = $data['contact_no'];

		if(isset($id) && $id > 0) {
			$params['bindValues'][':company_id']  = $id;
		}
		
		$arrResult = $this->objDBManager->executeSQL($params);

		if(isset($arrResult['result']) && isset($arrResult['result']['id'])) {
			$arrResult['result'] = [];
			$arrResult['error']  = 'Company is already registerd with given company name or email or mobile number';
		}

		return $arrResult;
	}

	public function retrieve($params = array()){

		@extract($params);

		$query = "SELECT * from $this->table";

		if(isset($id)) {
			if($_SESSION['USER_TYPE'] != 1){
			    $query .= " WHERE id = :id AND created_by = :created_by";
			} else {
			    $query .= " WHERE id = :id";
			}
		}
        
        
		$query .= " ORDER BY `$this->table`.`name` ASC";

		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = isset($id) ? 'one' : 'all';

		if(isset($id))
			$params['bindValues'][":id"]  = $id;

        if($_SESSION['USER_TYPE'] != 1){
		    $params['bindValues'][":created_by"]  = $this->userID;
        }
        
		return $this->objDBManager->executeSQL($params);
	}

	public function update( $data ) {

		$preFix = 'logo_'.date('dmyhid').'_';
		$files = (isset($data['files']) && count($data['files']) > 0) ? $data['files'] : '';
		
		$id 	= $data['id'];
		$data 	= $this->objUtil->sanitizeData($data['data']);

		if(isset($files['name']) && !empty($files['name']))
			$data['logo'] = $preFix.$files['name'];
		
		$data['updated_at'] = $this->dateTime;

		$query 	= "UPDATE $this->table SET ";

		foreach($data as $key=>$value) {
			$query .= " $key = :$key ,";
		}

		$query = rtrim($query, ',');

		$query .= " WHERE id = :id";

		$params = [];
		$params['action'] = 'UPDATE';
		$params['query']  = $query;
		$params['bindValues'][":id"]  = $id;

		foreach($data as $key => $value){
			$params['bindValues'][":$key"]  = $value;
		}
		
		$arrResult = $this->objDBManager->executeSQL($params);
		
		if(isset($arrResult['success']) && isset($arrResult['lastInsertedId'])){ 
			if(isset($files['name']) && !empty($files['name'])){
				$arrParams = [];
				$arrParams['directory'][]	= 'company';
				$arrParams['name_prefix']	= $id.'_'.$preFix;
				$arrParams['files']			= $files;
				if(isset($files['name']) && !empty($files['name']))
					$uploadedFiles	= $this->objUtil->uploadFiles($arrParams);

			}
		}
		return $arrResult;
	}

	public function setIsActive( $data ) {

		$id   	   = $data['id'];
		$is_active = $data['is_active'];

		$data = [];
		$data['is_active']  = $is_active;
		$data['updated_at'] = $this->dateTime;
		$data['updated_by'] = $this->userID;

		$query 	= "UPDATE $this->table SET ";

		foreach($data as $key=>$value) {
			$query .= " $key = :$key ,";
		}

		$query = rtrim($query, ',');

		$query .= " WHERE id = :id";

		$params = [];
		$params['action'] = 'UPDATE';
		$params['query']  = $query;

		$params['bindValues'][":id"]  = $id;
		foreach($data as $key => $value){
			$params['bindValues'][":$key"]  = $value;
		}

		return $this->objDBManager->executeSQL($params);
	}

	public function getDropDownList($params = array()){

		@extract($params);

		$query = "SELECT id, name from $table";

		if(isset($id))
			$query .= " WHERE id = :id";

		$query .= " ORDER BY name ASC";

		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = isset($id) ? 'one' : 'all';

		if(isset($id))
			$params['bindValues'][":id"]  = $id;

		return $this->objDBManager->executeSQL($params);
	}

}