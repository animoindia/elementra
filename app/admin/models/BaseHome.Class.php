<?php

class BaseHome extends DBManager
{
	protected $objDBManager;
	protected $objUtil;
	protected $dateTime = '';

	public function __construct() {
		
		$this->objDBManager = new DBManager();
		
		$this->objUtil 		= new Util();
		$this->dateTime 	= date("Y-m-d H:i:s");
	}

	public function validateLogin( $data ) {

		//@extract($params);

		$query = "SELECT T1.*, T2.name as company_name
					FROM tbl_user_master T1
					INNER JOIN tbl_company_master T2 ON T1.company_id = T2.id AND T2.is_active = 1
					WHERE (T1.email = :email OR T1.mobile_no = :mobile)";

		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'one';
		$params['bindValues'][':email']   = $data[0];
		$params['bindValues'][':mobile']  = $data[0];
		//$params['bindValues'][':passwrd'] = MD5($param[1]);
		$arrResult = $this->objDBManager->executeSQL($params);

		if(isset($arrResult['result']) && isset($arrResult['result']['id'])){
			$hash 	    = $arrResult['result']['password'];
			$is_active 	= $arrResult['result']['is_active'];
			$password 	= $data[1];

			if($is_active == 0){
				$arrResult['error'] = 'Profile activation is pending';
			}
			else if(!$this->objUtil->verifyPasssword($password, $hash)){
				$arrResult['error'] = 'Wrong Password';
			}
			
		} else {
			$arrResult['error'] = 'User is not registerd';
		}

		if(isset($arrResult['error'])) {
			$arrResult['result'] = [];
		}

		return $arrResult;
	}
	
	public function validateKey( $params ){
		
		@extract($params);
		
		$query = "SELECT id, is_active, profile_activated, activation_key from tbl_user_master where activation_key = :key";
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'one';
		$params['bindValues'][':key']   = $key;
		
		$arrResult = $this->objDBManager->executeSQL($params);
		
		if(isset($arrResult['result']) && isset($arrResult['result']['id'])) {
			$is_active_user = $arrResult['result']['is_active'];
			$is_active_profile = $arrResult['result']['profile_activated'];
			
			if(isset($flag) && $flag == 'resetPassword'){
				if($is_active_profile == 0 || $is_active_user == 0){
					$arrResult['error'] = 'Use is inactive';
				} 
			} else {
				if($is_active_profile == 1 && $is_active_user == 0){
					$arrResult['error'] = 'Use is inactive';
				} else if($is_active_profile == 1){
					$arrResult['error'] = 'Profile is already activated';
				} 
			}
		} else {
			$arrResult['error'] = 'unregisterd key';
		}
		
		if(isset($arrResult['error'])) {
			$arrResult['result'] = [];
		}
		
		return $arrResult;
	}
	
	public function validatePassword( $params ) {
		
		@extract($params);

		$query = "SELECT id, password FROM tbl_user_master T1 WHERE id = :user_id";

		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'one';
		$params['bindValues'][':user_id'] = $id;
		$arrResult = $this->objDBManager->executeSQL($params);

		if(isset($arrResult['result']) && isset($arrResult['result']['id'])){
			$hash = $arrResult['result']['password'];
			
			if(!$this->objUtil->verifyPasssword($password, $hash)){
				$arrResult['error'] = 'Wrong Password';
			}
		} 

		if(isset($arrResult['error'])) {
			$arrResult['result'] = [];
		}

		return $arrResult;
	}
	
	public function activateUser( $params ) {
		
		@extract($params);
		
		$data = [];
		$data['id'] = $id;
		$data['data']['password']  		    	= $this->objUtil->encryptPassword(trim($password));
		$data['data']['is_active']  			= 1;
		$data['data']['profile_activated']  	= 1;
		$data['data']['profile_activated_date'] = $this->dateTime;
		$data['data']['updated_by'] 			= $id;
		$data['data']['updated_at']				= $this->dateTime;
		
		return $this->objDBManager->updateTableData('tbl_user_master', $data);
	}
	
	public function validateUser( $data ) {

		$query = "SELECT T1.*, T2.name as company_name
					FROM tbl_user_master T1
					INNER JOIN tbl_company_master T2 ON T1.company_id = T2.id AND T2.is_active = 1
					WHERE (T1.email = :email OR T1.mobile_no = :mobile)";

		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'one';
		$params['bindValues'][':email']   = $data[0];
		$params['bindValues'][':mobile']  = $data[0];
		$arrResult = $this->objDBManager->executeSQL($params);

		if(isset($arrResult['result']) && isset($arrResult['result']['id'])){
			$is_active 	= $arrResult['result']['is_active'];
			
			if($is_active == 0){
				$arrResult['error'] = 'Profile activation is pending';
			}

		} else {
			$arrResult['error'] = 'User is not registerd';
		}

		if(isset($arrResult['error'])) {
			$arrResult['result'] = [];
		}

		return $arrResult;
	}
	
	public function updatePassword( $params ) {
		
		@extract($params);
		
		$data = [];
		$data['id'] = $id;
		$data['data']['password']	= $this->objUtil->encryptPassword(trim($password));
		$data['data']['updated_by'] = $id;
		$data['data']['updated_at']	= $this->dateTime;
		
		return $this->objDBManager->updateTableData('tbl_user_master', $data);
	}

}