<?php

class BaseProject extends DBManager
{
	protected $objDBManager;
	protected $objUtil;
	protected $userID 	= '';
	protected $dateTime = '';
	protected $table 	= 'tbl_project_master';
	
	public function __construct() {
		$this->objDBManager = new DBManager();
		$this->objUtil 		= new Util();
		$this->userID 		= isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : 0;
		$this->dateTime 	= date("Y-m-d H:i:s");
	}
	
	public function retrieveDashboard( $params = array() ) {
		
		@extract($params);
		
		$query = "SELECT T1.*, CONCAT (T2.f_name,' ',T2.l_name) as project_created_by, T3.name as company, T4.status, T4.id as status_id, T5.project_id, 
					T5.id as tran_id, T5.data as tran_data, T5.remarks as tran_remarks, COALESCE(T6.id, 1) as tran_step_id, T6.name as tran_step, T7.status as tran_status,
					T5.created_at as tran_date, CONCAT (T8.f_name,' ',T8.l_name) as tran_assigned_to ,CONCAT (T9.f_name,' ',T9.l_name) as tran_created_by 
					from $this->table T1 
					INNER JOIN tbl_user_master T2 ON T2.id = T1.created_by 
					INNER JOIN tbl_company_master T3 ON T3.id = T2.company_id 
					INNER JOIN tbl_status_master T4 ON T4.id = T1.status ";
					 
			if(!isset($id) && $_SESSION['USER_TYPE'] == 3){
				$query .= "LEFT JOIN (
									SELECT A.* FROM `tbl_project_transaction_log` A 
									INNER JOIN ( SELECT project_id 
												 FROM `tbl_project_transaction_log` 
												 WHERE created_by = :user_id OR assigned_to = :assigned_to
												 GROUP by project_id 
												) B ON A.project_id = B.project_id ) T5 ON T5.project_id = T1.id ";
			} else {
				$query .= "LEFT JOIN tbl_project_transaction_log T5 ON T5.project_id = T1.id ";
			}									
				$query .=  "LEFT JOIN tbl_step_master T6 ON T6.id = T5.step_id 
							LEFT JOIN tbl_status_master T7 ON T7.id = T5.status_id 
							LEFT JOIN tbl_user_master T8 ON T8.id = T5.assigned_to 
							LEFT JOIN tbl_user_master T9 ON T9.id = T5.created_by WHERE T1.status <> :status AND T1.is_active = :is_active ";
					
		if(isset($id)){
			$query .= ' AND T1.id = :id';
		} else if ($_SESSION['USER_TYPE'] == 2) {
			$query .= ' AND T2.company_id = :company_id';
		} else if($_SESSION['USER_TYPE'] == 3){
			$query .= ' AND (T1.id IN(T5.project_id))';
		}
		
		$query .= " ORDER BY `T1`.`created_at`, `T1`.`updated_at` DESC";
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'all';
		$params['bindValues'][':status']  	 = 1;
		$params['bindValues'][':is_active']  = 1;
		
		if(isset($id))
			$params['bindValues'][':id']  = $id;
		else if ($_SESSION['USER_TYPE'] == 2) 
			$params['bindValues'][':company_id']  = $_SESSION['COMPANY_ID'];
		else if($_SESSION['USER_TYPE'] == 3) {
			$params['bindValues'][':user_id']  = $this->userID;
			$params['bindValues'][':assigned_to']  = $this->userID;
		}
		
		return $this->objDBManager->executeSQL($params);
	}
	
	public function retrieve( $params = array() ) {
		
		@extract($params);
		
		$query = "SELECT T1.*, concat(T2.f_name,' ',T2.l_name) as user_name, 
					T3.name as company, T4.status, T4.id as status_id, 
					T5.log_status_id, T5.log_step_id,
					COALESCE(T6.status, '') as log_status, COALESCE(T7.name, '') as log_step
					from $this->table T1
					INNER JOIN tbl_user_master T2 ON T2.id = T1.created_by
					INNER JOIN tbl_company_master T3 ON T3.id = T2.company_id 
					INNER JOIN tbl_status_master T4 ON T4.id = T1.status 
					LEFT JOIN ( SELECT A.project_id,
                   				COALESCE(A.status_id, 0) as log_status_id, 
								COALESCE(A.step_id, 0) as log_step_id,
								A.created_by, A.assigned_to
					            FROM `tbl_project_transaction_log` A 
				                INNER JOIN ( SELECT MAX(`id`) id ,project_id 
					                         FROM `tbl_project_transaction_log` 
								             GROUP by project_id 
						                  	) B ON A.Id = B.ID ";
					if(!isset($id) && $_SESSION['USER_TYPE'] == 3)
						$query .= " Where A.created_by = :user_id OR A.assigned_to = :user_id OR A.project_id = B.project_id ";
					
						 $query .= " ) T5 on  T5.project_id = T1.id  
					LEFT JOIN tbl_status_master T6 ON T6.id = T5.log_status_id 
					LEFT JOIN tbl_step_master T7 ON T7.id = T5.log_step_id";
		
		if(isset($id)){
			$query .= ' WHERE T1.id = :id';
		} else if ($_SESSION['USER_TYPE'] == 2) {
			$query .= ' WHERE T2.company_id = :company_id';
		} else if($_SESSION['USER_TYPE'] == 3){
			$query .= ' WHERE T1.created_by = :user_id OR T5.created_by = :user_id OR T5.assigned_to = :user_id';
		}
		
		$query .= " ORDER BY `T1`.`created_at`, `T1`.`updated_at` DESC";
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = isset($id) ? 'one' : 'all';
				
		if(isset($id))
			$params['bindValues'][':id']  = $id;
		else if ($_SESSION['USER_TYPE'] == 2) 
			$params['bindValues'][':company_id']  = $_SESSION['COMPANY_ID'];
		else if($_SESSION['USER_TYPE'] == 3) {
			$params['bindValues'][':user_id']  = $this->userID;
			$params['bindValues'][':assigned_to']  = $this->userID;
		}
		
		return $this->objDBManager->executeSQL($params);
	}
	
	public function create( $data ) {
		
		$files = (isset($data['files']) && count($data['files']) > 0) ? $data['files'] : '';
		$status = $data['status'];
		
		$tbl_data 	= [];
		$arrData					= $data['finalArr'];
		$tbl_data['type']			= $arrData['Client & Site Details']['details']['project_type'];
		$tbl_data['project_for']	= $arrData['Client & Site Details']['details']['project_for'];
		$tbl_data['client_name']	= $arrData['Client & Site Details']['details']['client_name'];
		$tbl_data['client_contact'] = $arrData['Client & Site Details']['details']['client_phone'];
		$tbl_data['data']			= $this->objUtil->encodeData($arrData);
		$tbl_data['remarks']    	= $arrData['Client & Site Details']['details']['client_remarks'];
		$tbl_data['status']     	= $status;
		$tbl_data['created_by'] 	= $this->userID;
		$tbl_data['created_at'] 	= $this->dateTime;
	
		$columns 	= implode(',', array_keys($tbl_data));
		$values 	= $this->objUtil->arrayKeyPrefix($tbl_data);
		$values 	= implode(',', array_keys($values));
		
		$query 	= "INSERT INTO $this->table ($columns) values ($values)";
		$params = [];
		$params['action'] = 'INSERT';
		$params['query']  = $query;
		foreach($tbl_data as $key => $value){
			$params['bindValues'][":$key"]   = $value;
		}
		
		$arrResult =  $this->objDBManager->executeSQL($params);
			
		return $arrResult;
	}
	
	public function update( $data ) {
		
		$files = (isset($data['files']) && count($data['files']) > 0) ? $data['files'] : '';
		$status = $data['status'];
		
		$tbl_data 	= [];
		$arrData					= $data['finalArr'];
		$tbl_data['type']			= $arrData['Client & Site Details']['details']['project_type'];
		$tbl_data['project_for']	= $arrData['Client & Site Details']['details']['project_for'];
		$tbl_data['client_name']	= $arrData['Client & Site Details']['details']['client_name'];
		$tbl_data['client_contact'] = $arrData['Client & Site Details']['details']['client_phone'];
		$tbl_data['data']			= $this->objUtil->encodeData($arrData);
		$tbl_data['remarks']    	= $arrData['Client & Site Details']['details']['client_remarks'];
		$tbl_data['status']     	= $status;
		$tbl_data['updated_by'] 	= $this->userID;
		$tbl_data['updated_at'] 	= $this->dateTime;
	
		$columns 	= implode(',', array_keys($tbl_data));
		$values 	= $this->objUtil->arrayKeyPrefix($tbl_data);
		$values 	= implode(',', array_keys($values));
		
		$query 	= "UPDATE $this->table SET ";
		foreach($tbl_data as $key=>$value) {
			$query .= " $key = :$key ,";
		}
		$query = rtrim($query, ',');
		$query .= " WHERE id = :id";
		
		$params = [];
		$params['action'] = 'UPDATE';
		$params['query']  = $query;
		$params['bindValues'][":id"]  = $data['project_id'];
		
		foreach($tbl_data as $key => $value){
			$params['bindValues'][":$key"]  = $value;
		}
		$arrResult 	= $this->objDBManager->executeSQL($params);
		
		return $arrResult;
	}
	
	public function remove ($data) { 
		
		$id   = $data['id'];
		
		$data = [];
		$data['is_active']  = 0;
		$data['updated_by'] = $this->userID;
		$data['updated_at'] = $this->dateTime;
		
		$query 	= "UPDATE $this->table SET ";
		
		foreach($data as $key=>$value) {
			$query .= " $key = :$key ,";
		}
		
		$query = rtrim($query, ',');
		
		$query .= " WHERE id = :id";
		
		$params = [];
		$params['action'] = 'UPDATE';
		$params['query']  = $query;
		$params['bindValues'][":id"]  = $id;
		
		foreach($data as $key => $value){
			$params['bindValues'][":$key"]  = $value;
		}
				
		return $this->objDBManager->executeSQL($params);
	}
	
	public function retrieveFormFields( $params = array() ) {
		
		@extract($params);
		
		$query = "SELECT T1.id as field_id, T1.name as field_name, T2.id as section_id, T2.header as section 
					FROM `tbl_form_field_master` T1 
				    JOIN tbl_form_section_master T2 On T1.form_id = T2.id AND T2.form= :form_type AND T2.is_active = 1
					WHERE T1.is_active = 1 ";
		
		$params = [];
		$params['action'] = 'SELECT';
		$params['query']  = $query;
		$params['fetch']  = 'all';
		
		$params['bindValues'][':form_type']  = $form_type;
		
		return $this->objDBManager->executeSQL($params);
	}
	
	public function addProjectTransaction($data) {
		
		@extract($data);
		$arrProjectId = explode(',', $project_id);
		
		$tbl_data = [];
		$tbl_data = $this->objUtil->sanitizeData($data);
		$tbl_data['created_by']  = isset($created_by) ? $created_by : $this->userID;
		$tbl_data['created_at']  = $this->dateTime;
		
		foreach($arrProjectId as $key => $val){
			
			$tbl_data['project_id'] = $val;
			
			$columns 	= implode(',', array_keys($tbl_data));
			$values 	= $this->objUtil->arrayKeyPrefix($tbl_data);
			$values 	= implode(',', array_keys($values));
			
			$query 	= "INSERT INTO tbl_project_transaction_log ($columns) values ($values)";
			
			$params = [];
			$params['action'] = 'INSERT';
			$params['query']  = $query;
			
			foreach($tbl_data as $key => $value){
				$params['bindValues'][":$key"]   = $value;
			}
			
			return $this->objDBManager->executeSQL($params);
		}
	}
	
	public function addProjectSteps($data) {
		
		$files = (isset($data['files']) && count($data['files']) > 0) ? $data['files'] : '';
		$arrProjectId 	= explode(',', $data['project_id']);
		unset($data['files']);
		unset($data['project_id']);
		
		@extract($data);
		
		$tbl_data = $data;
		$tbl_data['data'] = $this->objUtil->encodeData($this->objUtil->sanitizeData($tbl_data['data']));
		$tbl_data['created_by']  = $this->userID;
		$tbl_data['created_at']  = $this->dateTime;
				
		foreach($arrProjectId as $key => $val){
			
			$tbl_data['project_id'] = $val;
			
			$columns 	= implode(',', array_keys($tbl_data));
			$values 	= $this->objUtil->arrayKeyPrefix($tbl_data);
			$values 	= implode(',', array_keys($values));
			
			$query 	= "INSERT INTO tbl_project_transaction_log ($columns) values ($values)";
			
			$params = [];
			$params['action'] = 'INSERT';
			$params['query']  = $query;
			
			foreach($tbl_data as $key => $value){
				$params['bindValues'][":$key"]   = $value;
			}
			
			$arrResult =  $this->objDBManager->executeSQL($params);
			
			$project_id = ($arrResult['lastInsertedId']) ? $arrResult['lastInsertedId'] : 0;

			//Upload Files in folder 
			if(isset($files) && !empty($files)){
				if(isset($files['name']) && !empty($files['name'])) {
					switch($tbl_data['step_id']){
						case 3:
							$directory = 'Technical Check';
						break;
						case 4:
							$directory = 'Marking of Points';
						break;
						case 5:
							$directory = 'Final Measurements';
						break;
						case 6:
							$directory = 'PDI';
						break;
						case 7:
							$directory = 'Installation';
						break;
						case 8:
							$directory = 'Finished';
						break;
						default:
							$directory = 'Technical Check';
						break;
					}
			
					$arrParams = [];
					$arrParams['directory'][]	= 'projects';
					$arrParams['directory'][]	= $val;
					$arrParams['directory'][]	= 'progress';
					$arrParams['directory'][]	= $directory;
					
					$arrParams['files']			= $files;
					$uploadedFiles	= $this->objUtil->uploadFiles($arrParams);
				}
			}
		}
		return $arrResult;
	}
}