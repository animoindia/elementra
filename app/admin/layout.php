<?php 
$cls_footer = "out_footer";
$cls_content = "container";

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'))  {
	require_once('header.php');

	if ( $objSessionManager::validateUserSession('admin') ){

		$cls_footer = "in_footer";
		
		require_once('navbar.php');
		
		echo "<div class='row page_container' id = 'page-wrapper'>";
		
	} else {
		echo "<div id='cont_full_screen' class='col-xs-12'>";
		echo "<div class='col-xs-12' id = 'login'>";
	}

}

$app = new App;

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'))  {
	if ( $objSessionManager::validateUserSession('admin') ){
		
		require_once('footer.php'); 
		
		echo "</div>";
		
	} else {
		
		echo "</div>";
		
		require_once('footer.php'); 
		
		echo "</div>";
	}

}
?>