<!-- NAV TOP START -->
 <nav class="navbar navbar-default top-navbar" role="navigation">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
        <a class="navbar-brand hidden-xs" href="\"><strong><!-- <i class="icon fa fa-plane"></i> --> AK FITTED INTERIORS </strong></a>
		<a class="navbar-brand visible-xs" href="\"><strong><!-- <i class="icon fa fa-plane"></i> --> AKFI </strong></a>
		<div id="sideNav" href="">
			<i class="fa fa-bars icon"></i>
		</div>
    </div>
	<?php
		$active_dashboard 	= (MODULE == 'home' && (ACTION == 'index' || ACTION == 'dashboard')) ? 'active-menu' : '';
		$active_projects  	= (MODULE == 'project') ? 'active-menu' : '';
		$active_user  		= (MODULE == 'user') ? 'active-menu' : '';
		$active_company  	= (MODULE == 'company') ? 'active-menu' : '';
		$active_feedback  	= (MODULE == 'feedback') ? 'active-menu' : '';
		$active_complaint  	= (MODULE == 'feedback') ? 'active-menu' : '';
	 ?>
	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
				<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
				<font><?= $_SESSION['USER_NAME'];?></font>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li>
					<a href="\user/edit?user=<?= $_SESSION['USER_ID']?>&flag=profile" data-toggle="modal" data-target="#myModal" onclick="$('.div_loading_image').show();"><i class="fa fa-user fa-fw"></i> My Profile</a>
				</li>
				<li>
					<a href="\user/changePassword" data-toggle="modal" data-target="#myModal" onclick="$('.div_loading_image').show();"><i class="fa fa-gear fa-fw"></i> Change Password</a>
				</li>
				<li class="divider"></li>
				<li class="logout-btn">
					<a href="\logout" onclick="$('.div_loading_image').show();"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
				</li>
			</ul>
			<!-- /.dropdown-user --> 
		</li>
		<!-- /.dropdown -->
	</ul>
 </nav>
 <!--/. NAV TOP END-->

 <!-- NAV SIDE START -->
<nav class="navbar-default navbar-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="main-menu">
			<?php if($_SESSION['USER_TYPE'] == 1 || $_SESSION['USER_TYPE'] == 2) { ?>
				<li>
					<a class="<?= $active_dashboard; ?>" href="\dashboard" onclick="$('.div_loading_image').show();"><i class="fa fa-dashboard"></i> Dashboard</a>
				</li>
			<?php } ?>
				<li>
					<a class="<?= $active_projects; ?>" href="\project/list" onclick="$('.div_loading_image').show();"><i class="fa fa-desktop"></i> Project</a>
				</li>
			<?php if($_SESSION['USER_TYPE'] == 1 && $_SESSION['COMPANY_ID'] == 1) { ?>
				<li>
					<a class="<?= $active_company; ?>" href="\company/list" onclick="$('.div_loading_image').show();"><i class="fa fa-building-o"></i> Company </a>
				</li>
			<?php } ?>
			<?php if($_SESSION['USER_TYPE'] == 1 || $_SESSION['USER_TYPE'] == 2) { ?>
				<li>
					<a class="<?= $active_user; ?>" href="\user/list" onclick="$('.div_loading_image').show();"><i class="fa fa-user"></i> User </a>
				</li>
			<?php } ?>
			<?php if($_SESSION['COMPANY_ID'] != 1) { ?>
				<li>
					<a class="<?= $active_feedback; ?>" href="\feedback/new" onclick="$('.div_loading_image').show();"><i class="fa fa-star-half-o"></i> Feedback</a>
				</li>
			<?php } ?>

		</ul>
	</div>
</nav>
<!-- /. NAV SIDE END -->