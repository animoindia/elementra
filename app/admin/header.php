<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="akfi" name="author" />
		<title>AK Fitted Interiors</title>
		
		<!-- Bootstrap Styles-->
		<link href="./../assets/css/bootstrap.css" rel="stylesheet" />
		<link href="./../assets/css/bootstrap-theme.min.css" rel="stylesheet" />		
		<link href="./../assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
		
		<!-- FontAwesome Styles-->
		<link href="./../assets/css/font-awesome.css" rel="stylesheet" />
		
		<!-- Star Ratings Styles-->
		<link href="./../assets/css/project-step-form.css" rel="stylesheet" />

		<!-- Star Ratings Styles-->
		<link href="./../assets/css/star-ratings.css" rel="stylesheet" />
    	
		<!-- FEEDBACK Star Ratings Styles-->
		<link href="./../assets/css/star-rating-feedback.min.css" rel="stylesheet" />
		
		<link rel="stylesheet" href="./../assets/fonts/linearicons/style.css">
		<link rel="stylesheet" href="./../assets/fonts/material-design-iconic-font/css/material-design-iconic-font.css">
		
		<!-- TABLE STYLES-->
		<link href="./../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
		
		<!-- Custom Styles-->
		<link href="./../assets/css/custom-styles.css" rel="stylesheet" />
		
		<!-- Favicon-->
		<link rel="shortcut icon" href="./../assets/img/favicon.ico" type="image/x-icon">
	</head>
	<?php $class_body = (isset($_SESSION['USER_ID'])) ? '' : 'full_screen_main'; ?>
	<body class="<?= $class_body;?>">
	
	<div class="cls_loading_image div_loading_image" id="div_loading_image" style="display:none;"></div>
	
	<!-- ORIGINAL CODE LINK: https://codepen.io/ruigewaard/pen/CtnsJ  --> 
	<div class="col-xs-12 css-preloader-wrapper" style="display:none;">
		<div class="container">
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		  <div class="block"></div>
		</div>
	</div>
	<!-- CSS PRE LOADER CODE ENDS HERE -->
	
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
	</div>
	<div id="cnfmModal" class="modal fade" aria-hidden="true" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-body"> Are you sure? </div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Proceed</button>
			<button type="button" data-dismiss="modal" class="btn">Cancel</button>
		</div>
	</div>
	<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div id="exitButton" class="close" data-dismiss="modal"><i class="fa fa-times"></i></div>
		<div id="image_name"></div>
      </div>
      <div class="modal-body">
        <img class="img-responsive" src="" id="imagepreview" border="0" />
      </div>
    </div>
  </div>
</div>