<?php

/**
* The Company controller, called when no controller/method has been passed  
* to the application.
*/
class Company extends Controller
{
	
	protected $objCompany = '';
	protected $objUser = '';
	protected $objUtil;
	
	public function __construct() {
		
		$obj_sessionManager = new SessionManager('admin');
		
		if($obj_sessionManager::validateUserSession(1)) {
			$this->objCompany 	= $this->setModel('BaseCompany');
			$this->objUser 		= $this->setModel('BaseUser');
			$this->objUtil 	  	= new Util();
		} else {
			header('Location: \login');
		}
	}

	public function indexAction($arrParams = array()) {
		if(isset($_SESSION['USER_ID']))
			$this->listAction();
		else
			$this->renderView('home/login');
		
	}
	
	public function listAction($arrParams = array()) {
		if(isset($_SESSION['USER_ID'])) {
			$arrResult = [];
			$arrResult = $this->objCompany->retrieve();
			
			$this->renderView('company/list', [ 'data' => $arrResult, 'pagination' => $this->objUtil->getPagination(count($arrResult['result']), 1, 1) ]);
		} else {
			$this->renderView('home/login');
		}			
	}
	
	public function newAction($arrParams = array()){
		if(isset($_SESSION['USER_ID'])) {
			$arrResult = [];
			$arrResult['result'] = isset($arrParams['result']) ? $arrParams['result'] : '';
			$arrResult['error'] = isset($arrParams['error']) ? $arrParams['error'] : '';
			
			$this->renderView('company/new', [ 'action' => 'create', 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function editAction($arrParams = array()){
		
		if(isset($_SESSION['USER_ID'])) {
			$data = [];
			$data['id'] = $_REQUEST['cmpny'];
			$arrResult = [];
			$arrResult = $this->objCompany->retrieve($data);
			
			$this->renderView('company/new', [ 'action' => 'update', 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function saveAction($arrParams = array()){
		if(isset($_SESSION['USER_ID']) && isset($_POST)) {
			@extract($arrParams['requestObj']);

			$arrData = $objCompanyResult = $res = [];
			
			$action 		 = isset($action) ? $action : (isset($_POST['action']) ? $_POST['action'] : '');
			$arrData['data'] = isset($data) ? $data : (isset($_POST['data']) ? $_POST['data'] : '');
			
			if(isset($flag)){
				$action = 'setIsActive';
				$arrData['is_active']= isset($flag) ? $flag : 0;
			}
			
			if($action != 'create')
				$arrData['id'] 	= isset($id) ? $id : (isset($_POST['id']) ? $_POST['id'] : '');
			
			$arrValidateCompany = $this->objCompany->validateCompany($arrData);
						
			if(!isset($arrValidateCompany['error'])){
				
				if(isset($_FILES['image']))
					$arrData['files'] = $_FILES['image'];
				
				$objCompanyResult = $this->objCompany->$action($arrData);
				
				/********************** START CODE User Registration with Email ********************************/
				if($action == 'create' && isset($objCompanyResult['success']) && $objCompanyResult['success'] == 'success'){
					
					$arrName = explode(' ', $arrData['data']['contact_person']);
					
					$arrNewUser = [];
					$arrNewUser['company_id'] = $objCompanyResult['lastInsertedId'];
					$arrNewUser['user_type_id'] = 2;
					$arrNewUser['designation_id'] = 1;
					$arrNewUser['mobile_no'] = $arrData['data']['contact_no'];
					$arrNewUser['email'] = $arrData['data']['email'];
					$arrNewUser['f_name'] = isset($arrName[0]) ? $arrName[0] : ' ';
					$arrNewUser['l_name'] = isset($arrName[1]) ? $arrName[1] : ' ';
					
					$objUserResult = $this->objUser->create($arrNewUser);
					
					/********************** START CODE Email ********************************/
					if(isset($objUserResult['success']) && $objUserResult['success'] == 'success') {
						
						$arrEmailData = [];
						$arrEmailData['password'] 		= $objUserResult['random_password'];
						$arrEmailData['userName'] 		= $arrData['data']['contact_person'];
						$arrEmailData['companyName'] 	= $arrData['data']['name'];
						$arrEmailData['activation_url']	= HOST."/home/activateProfile?key=".$objUserResult['activation_key'];
						
						$emailData = [];
						$emailData['emailFrom'] = EMAIL_FROM_ADMIN;
						
						$emailData['emailTo'] 	= $arrData['data']['email'];
						
						$emailData['subject'] 	= EMAIL_SUBJECT_REGISTRATION;
						
						$emailData['body']	  	= $this->getTemplateData('emails/emailRegistration', ['data' => $arrEmailData]);
						
						$this->objUtil->sendEmail($emailData);
					}
					/********************** END CODE Email ********************************/
				}
				/********************** START CODE User Registration Email ********************************/
			}
			
			
			if(isset($objCompanyResult['success']) && isset($objCompanyResult['lastInsertedId'])){
				if($action == 'create')
					$res['successMsg'] = "Company added Successfully";
				else 
					$res['successMsg'] = "Company updated Successfully";
			} else if(isset($arrValidateCompany['error']))
				$res['error'] = $arrValidateCompany['error'];
			else if(isset($objCompanyResult['error']))
				$res['error'] = 'Oops some error occured while transaction, please try again';
	
			print $this->objUtil->setAjaxResponse($res);
		
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function activeAction( $params ) {
		
		if(isset($_SESSION['USER_ID'])) {
			@extract($arrParams['requestObj']);

			$arrData = array();
			$arrData['id'] 	= (isset($_GET['id'])) ? $_GET['id'] : 0;
			$arrData['is_active'] = (isset($_GET['flag'])) ? $_GET['flag'] : 0;
			
			$arrResult = $this->objCompany->setIsActive($arrData);	
			
			$this->listAction();	
		} else {
			$this->renderView('home/login');
		}
	}
}
$objCompany = new Company();
?>