<?php

/**
* The Product controller, called when no controller/method has been passed 
* to the application. 
*/
class Project extends Controller
{
	
	protected $objProject = '';
	protected $objUtil;
	
	public function __construct() {

		$obj_sessionManager = new SessionManager('admin');

		if($obj_sessionManager::validateUserSession(1)) {
			$this->objProject = $this->setModel('BaseProject');
			$this->objUtil 	= new Util();
		} else {
			header('Location: \login');
		}		
	}

	public function indexAction($arrParams = array()) {
		if(isset($_SESSION['USER_ID']))
			$this->listAction();
		else 
			$this->renderView('home/login');
	}
	
	public function listAction($arrParams = array()) {
		if(isset($_SESSION['USER_ID'])) {
			$arrResult = [];
			$arrResult = $this->objProject->retrieve();
			
			$this->renderView('project/list', [ 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function viewAction($arrParams = array()) {
		
		if(isset($_SESSION['USER_ID'])) {
			$data['id'] = $_REQUEST['prj'];
			
			$arrResult = [];
			$arrResult = $this->objProject->retrieve($data);
			$arrResult['page'] = isset($_REQUEST['page']) ? $_REQUEST['page'] : 'view';
			
			if($arrResult['page'] == 'dashboard'){
				$arrProgress = $this->objProject->retrieveDashboard($data);
				if(isset($arrProgress['success']) && isset($arrProgress['result']) && count($arrProgress['result']) > 0) {
					$prj_arr 	 = $arrProgress['result'];
					$progressArr = [];
						
					$stepCnt = count($prj_arr);
					if($stepCnt == 10) {
						foreach($prj_arr as $key => $val) {
							@extract($val);
							$trans_id	= (trim($tran_id) != '') ? $tran_id : '-';
							$date 		= (trim($tran_date) != '') ? date('d-m-Y (H:i)', strtotime($tran_date)) : '-';
							$step_id  	= (trim($tran_step_id) != '') ? $tran_step_id : '-';
							$step_txt  	= (trim($tran_step) != '') ? $tran_step : '-';
							$status_txt = (trim($tran_status) != '') ? $tran_status : '-';
							$tran_data  = (trim($tran_data) != '') ? $tran_data : '-';
							$remarks  	= (trim($tran_remarks) != '') ? $tran_remarks : '-';
							$assignee 	= (trim($tran_assigned_to) != '') ? $tran_assigned_to : '-';
							$assignBy 	= (trim($tran_created_by) != '') ? $tran_created_by : '-';
							
							$progressArr[$step_id]['tran_id'] 		  = $trans_id;
							$progressArr[$step_id]['tran_data'] 	  = $tran_data;
							$progressArr[$step_id]['tran_remarks']	  = $remarks;
							$progressArr[$step_id]['tran_step_id']    = $step_id;
							$progressArr[$step_id]['tran_step'] 	  = $step_txt;
							$progressArr[$step_id]['tran_status'] 	  = $status_txt;
							$progressArr[$step_id]['tran_date'] 	  = $date;
							$progressArr[$step_id]['tran_assigned_to']= $assignee;
							$progressArr[$step_id]['tran_created_by'] = $assignBy;
					
						}
					} else {
						for($i=1; $i <= ((10-$stepCnt)+1); $i++){
							foreach($prj_arr as $key => $val){
								@extract($val);
								$trans_id	 = (isset($prj_arr[$i]) && trim($tran_id) != '') ? $tran_id : '-';
								$date 		 = (isset($prj_arr[$i]) && trim($tran_date) != '') ? date('d-m-Y (H:i)', strtotime($tran_date)) : '-';
								$step_id  	 = (isset($prj_arr[$i]) && trim($tran_step_id) != '') ? $tran_step_id : $i;
								$step_txt  	 = (isset($prj_arr[$i]) && trim($tran_step) != '') ? $tran_step : '-';
								$status_txt  = (isset($prj_arr[$i]) && trim($tran_status) != '') ? $tran_status : '-';
								$tran_data   = (isset($prj_arr[$i]) && trim($tran_data) != '') ? $tran_data : '-';
								$remarks  	 = (isset($prj_arr[$i]) && trim($tran_remarks) != '') ? $tran_remarks : '-';
								$assignee 	 = (isset($prj_arr[$i]) && trim($tran_assigned_to) != '') ? $tran_assigned_to : '-';
								$assignBy 	 = (isset($prj_arr[$i]) && trim($tran_created_by) != '') ? $tran_created_by : '-';
								
								$progressArr[$step_id]['tran_id'] 		= $trans_id;
								$progressArr[$step_id]['tran_data'] 	    = $tran_data;
								$progressArr[$step_id]['tran_remarks']	= $remarks;
								$progressArr[$step_id]['tran_step_id']    = $step_id;
								$progressArr[$step_id]['tran_step'] 		= $step_txt;
								$progressArr[$step_id]['tran_status'] 	= $status_txt;
								$progressArr[$step_id]['tran_date'] 		= $date;
								$progressArr[$step_id]['tran_assigned_to']= $assignee;
								$progressArr[$step_id]['tran_created_by'] = $assignBy;								
							}
						}
					}
					$arrResult['progressArr'] = $progressArr;
				}
			}
			
			$this->renderView('project/view', [ 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function newAction($arrParams = array()){
		
		if(isset($_SESSION['USER_ID'])) {
			$arrTemp = array();
			
			$this->renderView('project/new', [ 'action' => 'create', 'data' => $arrTemp ]);
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function editAction($arrParams = array()) {
		
		if(isset($_SESSION['USER_ID'])) {
		
			$data['id'] = $_REQUEST['prj'];
			
			$arrResult = [];
			$arrResult = $this->objProject->retrieve($data);
			
			$this->renderView('project/edit', [ 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function saveAction($arrParams = array()){
		
		if(isset($_SESSION['USER_ID']) && isset($_POST)) {
			
			$params = $tempArray = $res  = $arrResult = [];
			$params['form_type'] = 'enquiry';
			$arrFormFields = $this->objProject->retrieveFormFields($params);
			
			$newKey = 1;
			$prvSectionId = 0;
			foreach($arrFormFields['result'] as $key => $val) {
				
				if($val['section_id'] != $prvSectionId){
					$newKey = 1; 
				}
				$tempArray['formSections'][$val['section_id']] = $val['section'];
				$tempArray['formFields'][$val['section_id']][$newKey] = $val['field_name'];
				
				$prvSectionId = $val['section_id'];
				$newKey++;
			}
			
			foreach($_POST['data'] as $dataKey => $dataVal){
				$sectionVal = $tempArray['formSections'][$dataKey];
				if($dataKey > 1){
					foreach($dataVal as $fieldKey => $fieldVal){
						$fileName = '';
						
						$fieldValue = $tempArray['formFields'][$dataKey][$fieldKey];
						
						$tempArray['finalArr'][$sectionVal][$fieldValue] = $fieldVal;
						
						if(isset($_FILES['image']['name'][$dataKey][$fieldKey])){
							$fileName = implode(', ', $_FILES['image']['name'][$dataKey][$fieldKey]);
							
							if(trim($fileName) != "") {
								$tempArray['files'][$sectionVal][$fieldValue]['name']		= $_FILES['image']['name'][$dataKey][$fieldKey];
								$tempArray['files'][$sectionVal][$fieldValue]['size']		= $_FILES['image']['size'][$dataKey][$fieldKey];
								$tempArray['files'][$sectionVal][$fieldValue]['tmp_name']	= $_FILES['image']['tmp_name'][$dataKey][$fieldKey];
								$tempArray['files'][$sectionVal][$fieldValue]['error']		= $_FILES['image']['error'][$dataKey][$fieldKey];
								
							}
						}
						
						$tempArray['finalArr'][$sectionVal][$fieldValue]['files'] = $fileName;
					}
				} else{
					$tempArray['finalArr'][$sectionVal]['details'] = $dataVal;
				}
			}
			$tempArray['status'] = $_POST['status'];
			
			$arrResult = $this->objProject->create($tempArray);
						
			if(isset($arrResult['lastInsertedId']) && $arrResult['lastInsertedId'] > 0) {
				
				$projectId = $arrResult['lastInsertedId'];
				/****************************************************/
				if(isset($_POST['status']) && $_POST['status'] == 2) {
				
					$objUser 	= $this->setModel('BaseUser');
					
					$data = [];
					$data['user_desg'] = 2;
					$arrAccountantUser  = $objUser->retrieveUserByType($data);
					
					if(isset($arrAccountantUser['result']) && count($arrAccountantUser['result']) > 0) {
						foreach($arrAccountantUser['result'] as $key=>$val) {
							$data = [];
							$data['project_id'] = $projectId;
							$data['remarks'] 	= 'Check advance payment and Approve';
							$data['step_id']    = 1;
							$data['status_id']	= 9;
							$data['assigned_to']= $val['id'];
							$data['created_by'] = 1;
							
							$arrAssignment  = $this->objProject->addProjectTransaction($data);
						}
					}
				}
				/****************************************************/
				
				if(isset($tempArray['files'])){
					foreach($tempArray['files'] as $section_key => $section_value){
						foreach($section_value as $fields_key=>$fieldValue){
							$arrParams = [];
							$arrParams['directory'][] = 'projects';
							$arrParams['directory'][] = $projectId;
							$arrParams['directory'][] = 'main';
							$arrParams['directory'][] = $section_key;
							$arrParams['directory'][] = $fields_key;
							$arrParams['files']		  = $fieldValue;
							
							$uploadedFiles	= $this->objUtil->uploadFiles($arrParams);
						}
					}
				}
			}
			
			if(isset($arrResult['success']) && isset($arrResult['lastInsertedId'])) {
				$res['successMsg'] = "Successfully Added";
				
			} else if(isset($arrResult['error'])){
				$res['error'] = 'Error occured while adding project';
			} 
			
			print $this->objUtil->setAjaxResponse($res);
			
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function updateAction($arrParams = array()){
		
		if(isset($_SESSION['USER_ID']) && isset($_POST)) {
			
			$params = $tempArray = [];
			$tempArray['project_id'] = $_POST['hdn_project_id'];
			$params['form_type'] = 'enquiry';
			$arrFormFields = $this->objProject->retrieveFormFields($params);
			
			$newKey = 1;
			$prvSectionId = 0;
			foreach($arrFormFields['result'] as $key => $val) {
				
				if($val['section_id'] != $prvSectionId){
					$newKey = 1; 
				}
				$tempArray['formSections'][$val['section_id']] = trim($val['section']);
				$tempArray['formFields'][$val['section_id']][$newKey] = trim($val['field_name']);
				
				$prvSectionId = $val['section_id'];
				$newKey++;
			}
			
			foreach($_POST['data'] as $dataKey => $dataVal){
				$sectionVal = $tempArray['formSections'][$dataKey];
				if($dataKey > 1){
					foreach($dataVal as $fieldKey => $fieldVal){
						$fileName = '';
						
						$fieldValue = $tempArray['formFields'][$dataKey][$fieldKey];
						
						$tempArray['finalArr'][$sectionVal][$fieldValue] = $fieldVal;
						
						if(isset($_FILES['image']['name'][$dataKey][$fieldKey])){
							$fileName = implode(', ', $_FILES['image']['name'][$dataKey][$fieldKey]);

							if(trim($fileName) != "") {
								$tempArray['files'][$sectionVal][$fieldValue]['name']		= $_FILES['image']['name'][$dataKey][$fieldKey];
								$tempArray['files'][$sectionVal][$fieldValue]['size']		= $_FILES['image']['size'][$dataKey][$fieldKey];
								$tempArray['files'][$sectionVal][$fieldValue]['tmp_name']	= $_FILES['image']['tmp_name'][$dataKey][$fieldKey];
								$tempArray['files'][$sectionVal][$fieldValue]['error']		= $_FILES['image']['error'][$dataKey][$fieldKey];
								
							}
						}
						
						$tempArray['finalArr'][$sectionVal][$fieldValue]['files'] = $fileName;
					}
				} else{
					$tempArray['finalArr'][$sectionVal]['details'] = $dataVal;
				}
			}
			$tempArray['status'] = $_POST['status'];
			
			$arrResult = $this->objProject->update($tempArray);
			
			if(isset($arrResult['lastInsertedId']) && isset($arrResult['success'])) {
				
				$projectId = $_POST['hdn_project_id'];
				
				/****************************************************/
				if(isset($_POST['status']) && $_POST['status'] == 2) {
				
					$objUser 	= $this->setModel('BaseUser');
					
					$data = [];
					$data['user_desg'] = 2;
					$arrAccountantUser  = $objUser->retrieveUserByType($data);
					
					if(isset($arrAccountantUser['result']) && count($arrAccountantUser['result']) > 0) {
						foreach($arrAccountantUser['result'] as $key=>$val) {
							$data = [];
							$data['project_id'] = $projectId;
							$data['remarks'] 	= 'Check advance payment and Approve';
							$data['step_id']    = 1;
							$data['status_id']	= 9;
							$data['assigned_to']= $val['id'];
							$data['created_by'] = 1;
							
							$arrAssignment  = $this->objProject->addProjectTransaction($data);
						}
					}
				}
				/****************************************************/
				
				if(isset($tempArray['files'])){
					foreach($tempArray['files'] as $section_key => $section_value){
						foreach($section_value as $fields_key=>$fieldValue){
							$arrParams = [];
							$arrParams['directory'][] = 'projects';
							$arrParams['directory'][] = $projectId;
							$arrParams['directory'][] = 'main';
							$arrParams['directory'][] = $section_key;
							$arrParams['directory'][] = $fields_key;
							$arrParams['files']		  = $fieldValue;
							
							$uploadedFiles	= $this->objUtil->uploadFiles($arrParams);
						}
					}
				}
			}
			
			if(isset($arrResult['success']) && isset($arrResult['lastInsertedId'])) {
				$res['successMsg'] = "Successfully updated";
				
			} else if(isset($arrResult['error'])){
				$res['error'] = 'Error occured while update';
			} 
			
			print $this->objUtil->setAjaxResponse($res);			
			
			
		} else {
			$this->renderView('home/login');
		}
	}
	
	public function assignmentAction($arrParams = array()) {

		if(isset($_SESSION['USER_ID']) && isset($_POST)) {
			$data = [];
			$data['action'] 	= trim(strtolower($_POST['action']));
			$data['user_desg']  = ($data['action'] == 'req_approval') ? 2 : 3 ;
			$data['step']  		= ($data['action'] == 'req_approval') ? 1 : 2 ;
			$data['project_id'] = implode(',', $_POST['project_ids']);
		
			$arrResult 	= [];
			$objUser 	= $this->setModel('BaseUser');
			$arrResult  = $objUser->retrieveUserByType($data);
			$arrResult['arrData'] = $data;
					
			$this->renderView('project/assignment', [ 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}
	public function saveAssignmentAction($arrParams = array()) {

		if(isset($_SESSION['USER_ID']) && isset($_POST)) {
			$data = [];
			
			$data['project_id'] = trim($_POST['hdn_project_ids']);
			$data['remarks'] 	= trim($_POST['remarks']);
			$data['step_id']    = trim($_POST['hdn_step_id']);
			$data['status_id']	= 9;
			$data['assigned_to']= trim($_POST['assignee_id']);
		
			$arrResult 	= $res = [];
			$arrResult  = $this->objProject->addProjectTransaction($data);
			
			if(isset($arrResult['success']) && isset($arrResult['lastInsertedId'])) {
				$res['successMsg'] = "Successfully Assigned";
				
			} else if(isset($arrResult['error'])){
				$res['error'] = 'Error occured while Assigned';
			} 
			
			print $this->objUtil->setAjaxResponse($res);
			
		} else {
			$this->renderView('home/login');
		}
	}
	public function approvalAction($arrParams = array()) {

		if(isset($_SESSION['USER_ID']) && isset($_POST)) {
			$data = [];
			$data['action'] 	= trim(strtolower($_POST['action']));
			$data['project_id'] = implode(',', $_POST['project_ids']);
		
			$this->renderView('project/approval', [ 'data' => $data ]);
		} else {
			$this->renderView('home/login');
		}
	}
	public function saveApprovalAction($arrParams = array()) {

		if(isset($_SESSION['USER_ID']) &&  isset($_POST)) {
			$data = [];
			
			$data['project_id'] = trim($_POST['hdn_project_ids']);
			$data['remarks'] 	= trim($_POST['remarks']);
			$data['status_id']	= trim($_POST['hdn_status_ids']);
			$data['step_id']    = 1;
			$data['assigned_to']= 1;
		
			$arrResult 	= $res = [];
			$arrResult  = $this->objProject->addProjectTransaction($data);
			
			if(isset($arrResult['success']) && isset($arrResult['lastInsertedId'])) {
				$res['successMsg'] = "Successfully Updated";
				
			} else if(isset($arrResult['error'])){
				$res['error'] = 'Error occured while Approval';
			} 
			
			print $this->objUtil->setAjaxResponse($res);
			
		} else {
			$this->renderView('home/login');
		}
	}
	public function progressAction($arrParams = array()) {

		if(isset($_SESSION['USER_ID']) && isset($_POST)) {
			$data = [];
			$data['action'] 	= trim(strtolower($_POST['action']));
			$data['project_id'] = implode(',', $_POST['project_ids']);
			
			switch($data['action']){
				case 'tc':
					$partial = 'step_1';
					break;
				case 'mp':
					$partial = 'step_2';
					break;
				case 'fm':
					$partial = 'step_3';
					break;
				case 'pdi':
					$partial = 'step_4';
					break;
				case 'installation':
					$partial = 'step_5';
					break;
				case 'finished':
					$partial = 'step_6';
					break;
				default:
					$partial = 'step_1';
					break;
			}
							
			$this->renderView("project/$partial", [ 'data' => $data ]);
		} else {
			$this->renderView('home/login');
		}
	}
	public function saveProgressAction($arrParams = array()) {

		if(isset($_SESSION['USER_ID']) && isset($_POST)) {
			$data = [];
			
			$data['project_id'] 		= trim($_POST['hdn_project_id']);
			$data['data']['data'] 		= isset($_POST['data']) ? $_POST['data'] : '';
			$data['data']['remarks']	= trim($_POST['remarks']);
			$data['data']['step_id'] 	= trim($_POST['hdn_step_id']);
			$data['data']['status_id']	= 5;
			$data['data']['assigned_to']= $_SESSION['USER_ID'];
		
			if(isset($_FILES['image'])) {
				$data['files'] 	= $_FILES['image'];
				$data['data']['data']['image'] = implode(', ', $_FILES['image']['name']);
			}
			
			$arrResult 	= $res = [];
			$arrResult  = $this->objProject->addProjectSteps($data);
			
			if(isset($arrResult['success']) && isset($arrResult['lastInsertedId'])) {
				$res['successMsg'] = "Successfully updated";
				
			} else if(isset($arrResult['error'])){
				$res['error'] = 'Error occured while update';
			} 
			
			print $this->objUtil->setAjaxResponse($res);
			
		} else {
			$this->renderView('home/login');
		}
	}
	
}
$objProject = new Project();
?>