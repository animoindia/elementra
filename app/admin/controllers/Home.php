<?php

/**
* The default home controller, called when no controller/method has been passed
* to the application.
*/
class Home extends Controller
{
	protected $objHome;
	protected $objUtil;

	public function __construct() {

		$this->objHome	= $this->setModel('BaseHome');
		$this->objUtil 	= new Util();
	}

	public function indexAction($params = '') {

		if(isset($_SESSION['USER_ID']) && $_SESSION['USER_ID'] > 0)
			$this->dashboardAction();
		else
			$this->renderView('home/login');
	}

	public function loginAction($params = '') {
		
		if(isset($_SESSION['USER_ID']) && $_SESSION['USER_ID'] > 0)
			$this->renderView('home/dashboard');
		else 
			$this->logOutAction();

	}

	public function validateLoginAction($params) {
		@extract($params['requestObj']);
		
		if(isset($_POST) && isset($username) && isset($password) && trim($username) != '' &&  trim($password) != ''){
			$params = array();
			$params[0] = $username;
			$params[1] = $password;

			$arrResult  = $this->objHome->validateLogin($params);

			if(isset($arrResult['result']) && isset($arrResult['result']['id']) && $arrResult['result']['id'] > 0){

				$_SESSION['USER_ID'] 		= $arrResult['result']['id'];
				$_SESSION['USER_TYPE'] 		= $arrResult['result']['user_type_id'];
				$_SESSION['COMPANY_ID'] 	= $arrResult['result']['company_id'];
				$_SESSION['COMPANY_NAME'] 	= $arrResult['result']['company_name'];
				$_SESSION['DESIG'] 			= $arrResult['result']['designation_id'];
				$_SESSION['USER_NAME'] 		= ucwords(strtolower(trim($arrResult['result']['title']).' '.trim($arrResult['result']['f_name']).' '.trim($arrResult['result']['l_name'])));
				$_SESSION['USER_EMAIL']	 	= $arrResult['result']['email'];
				$_SESSION['CONTACT'] 		= $arrResult['result']['mobile_no'];
				
				if($_SESSION['USER_TYPE'] == 3) {
					header("location:\project/list");
				} else {
					header("location:/dashboard");
				}

			} else {
				$this->renderView('home/login', [ 'error' => @$arrResult['error'], 'username' => $username, 'password' => $password ]);
			}
		} else {
			$this->renderView('home/login', [ 'error' => 'Username or Passwored should not blank', 'username' => $username, 'password' => $password ]);
		}

	}

	public function dashboardAction($params = '') {
		if(isset($_SESSION['USER_ID'])) {
			$objProject	= $this->setModel('BaseProject');
			$arrResult = [];
			$arrResult = $objProject->retrieveDashboard();
			
			if(isset($arrResult['success']) && isset($arrResult['result']) && count($arrResult['result']) > 0) {
				foreach($arrResult['result'] as $key=>$val) {
					$arrResult['finalData'][$val['id']][$val['tran_step_id']] = $val;
				}
			}
			
			$this->renderView('home/dashboard', [ 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}

	public function activateProfileAction( $params ){
		@extract($params['requestObj']);
		
		$obj_sessionManager = new SessionManager('admin');
		$obj_sessionManager::destroySession('AKFIttedinteriors-admin');
		
		if(isset($key)){
			$data = [];
			$data['key']  = $key;
			$data['flag'] = 'activateProfile';
			$arrResult  = $this->objHome->validateKey($data);
		} else {
			$arrResult = [];
			$arrResult['error'] = 'Invalid Request';
		}
		
		$this->renderView('home/profileActivation', [ 'data' => $arrResult]);
	}
	
	public function saveNewPasswordAction ( $params ) {

		if(isset($_POST) && isset($_POST['hdn_id'])) {

			@extract($params['requestObj']);
						
			$arrResult = $param = $res = [];
			
			$param['password'] 	= $current_password;
			$param['id'] 		= $hdn_id;
			$arrPWDResult 		= $this->objHome->validatePassword($param);
			
			if(isset($arrPWDResult['error'])){
				$res['error'] = 'Current Password is worng';
			} else if (trim($password) != trim($cnf_password)){
				$res['error'] = 'New Password doesn\'t match with confirm password';
			} else if (isset($arrPWDResult['result']['id'])){
				$param = [];
				$param['id'] = $hdn_id;
				$param['password'] = trim($password);
				$arrResult  = $this->objHome->activateUser($param);
				
				if(isset($arrResult['lastInsertedId']) && isset($arrResult['success'])) {
					$res['successMsg'] = "Profile activated and Password Changed Successfully";
				} else {
					$res['error'] = 'Ooops error occured while profile activation and changing password!';
				}
			}
			
			print $this->objUtil->setAjaxResponse($res);
		} else {
			header('Location: \login');
		}
	}

	public function validateUserAction( $params ){
		@extract($params['requestObj']);
		
		if(isset($_POST) && isset($f_username) && trim($f_username) != ''){
			$param = $res = [];
			$param[0] 	  = $f_username;
			$arrValidate  = $this->objHome->validateUser($param);

			if(isset($arrValidate['result']) && isset($arrValidate['result']['id']) && $arrValidate['result']['id'] > 0){
				$userData = $arrValidate['result'];
				$data = [];
				$random_password 	= (mt_rand(strlen($userData['f_name'])+2, strlen($userData['l_name'])+10).time());
				$data['password'] 	= $random_password;
				$data['id'] 		= $userData['id'];
				$arrPWDResult  		= $this->objHome->updatePassword($data);
				/********************** START CODE Email ********************************/	
				$arrEmailData = [];
				$arrEmailData['password'] 		= $random_password;
				$arrEmailData['userName'] 		= $userData['f_name'].' '.$userData['l_name'];
				
				$arrEmailData['activation_url']	= HOST."/resetPassword?key=".$userData['activation_key'];
				
				$emailData = [];
				$emailData['emailFrom'] = EMAIL_FROM_ADMIN;
				
				$emailData['emailTo'] 	= $userData['email'];
				
				$emailData['subject'] 	= EMAIL_SUBJECT_RESET_PWD;
				
				$emailData['body']	  	= $this->getTemplateData('emails/emailForgotPassword', ['data' => $arrEmailData]);
				
				$this->objUtil->sendEmail($emailData);
				/********************** END CODE Email ********************************/
				
				$res['successMsg']  = "Please check your email! Temporary Password has been mailed you.";
			} else {
				$res['error'] 		= $arrValidate['error'];
			}
			print $this->objUtil->setAjaxResponse($res);
			
		} else {
			header('Location: \login');
		}
	}
	
	public function resetPasswordAction( $params ){
		@extract($params['requestObj']);
		
		$obj_sessionManager = new SessionManager('admin');
		$obj_sessionManager::destroySession('AKFIttedinteriors-admin');
		
		if(isset($key)){
			$data = [];
			$data['key']  = $key;
			$data['flag'] = 'resetPassword';
			$arrResult  = $this->objHome->validateKey($data);
			
		} else {
			$arrResult = [];
			$arrResult['error'] = 'Invalid Request';
		}
		
		$this->renderView('home/resetPassword', [ 'data' => $arrResult]);
	}
	
	public function updatePasswordAction( $params ){
		
		if(isset($_POST) && isset($_POST['hdn_id'])) {

			@extract($params['requestObj']);
						
			$arrResult = $param = [];
			
			$param['password'] 	= $current_password;
			$param['id'] 		= $hdn_id;
			$arrPWDResult 		= $this->objHome->validatePassword($param);
			
			if(isset($arrPWDResult['error'])){
				$arrResult['error'] = 'Current Password is worng';
			} else if (trim($password) != trim($cnf_password)){
				$arrResult['error'] = 'New Password doesn\'t match with confirm password';
			} else if (isset($arrPWDResult['result']['id'])){
				$param = [];
				$param['id'] = $hdn_id;
				$param['password'] = trim($password);
				$arrResult  = $this->objHome->updatePassword($param);
				
				if(isset($arrResult['lastInsertedId']) && isset($arrResult['success'])) {
					$arrResult['successMsg'] = "Password Changed Successfully";
				} else {
					$arrResult['error'] = 'Ooops error occured while changing password!';
				}
			}
			
			print $this->objUtil->setAjaxResponse($arrResult);
			
		} else {
			header('Location: \login');
		}		
	}

	public function logOutAction() {

		$obj_sessionManager = new SessionManager('admin');
		$obj_sessionManager::destroySession('AKFIttedinteriors-admin');

		header("location:/");

	}

	public function errorAction($params = '') {

		$this->renderView('home/error404');

	}

}

$objHome = new Home();
?>