<?php

/**
* The default home controller, called when no controller/method has been passed
* to the application. 
*/
class User extends Controller
{

	protected $objUser;
	protected $objUtil;
	protected $objCompany;
	
	public function __construct() {

		$obj_sessionManager = new SessionManager('admin');

		if($obj_sessionManager::validateUserSession(1)) {
			$this->objUser = $this->setModel('BaseUser');
			$this->objCompany = $this->setModel('BaseCompany');
			$this->objUtil 	= new Util();
		} else {
			header('Location: \login');
		}
	}

	public function indexAction($params) {

		if(isset($_SESSION['USER_ID']) && $_SESSION['USER_TYPE'] == 1 || $_SESSION['USER_TYPE'] == 2)
			$this->listAction();
		else
			$this->renderView('home/login');
	}

	public function listAction($params = array()) {
		if(isset($_SESSION['USER_ID'])){
			$arrResult = [];
			$params['flag'] = 'list';
			$arrResult = $this->objUser->retrieve($params);

			$this->renderView('user/list', [ 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}

	}

	public function newAction($params = array()) {
		if(isset($_SESSION['USER_ID'])){
			$arrResult = [];
			$arrResult['result'] 		= isset($params['result']) ? $params['result'] : '';
			$arrResult['error'] 		= isset($params['error']) ? $params['error'] : '';
			$arrResult['arrCompany'] 	= $this->objCompany->getDropDownList(['table' => 'tbl_company_master']);
			
			$this->renderView('user/new', [ 'action' => 'create', 'flag' => 'new', 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}

	public function editAction($params = array()) {
		
		if(isset($_SESSION['USER_ID'])){
			@extract($params['requestObj']);

			$arrResult = $params = [];
			$params['id'] = $user;
			$params['flag'] = isset($flag) ? $flag : 'edit';
			$arrResult = $this->objUser->retrieve($params);
			$arrResult['error'] 		= isset($params['error']) ? $params['error'] : '';
			if($_SESSION['USER_TYPE'] == 1){
				$arrResult['arrCompany'] 	= $this->objCompany->getDropDownList(['table' => 'tbl_company_master']);
			}
			
			$this->renderView('user/new', [ 'action' => 'edit', 'flag' => $params['flag'], 'data' => $arrResult ]);
		} else {
			$this->renderView('home/login');
		}
	}

	public function saveAction($params){
		
		if(isset($_SESSION['USER_ID']) && isset($_POST)){
			@extract($params['requestObj']);

			$arrData = array();
			$arrData['data'] = isset($_POST['data']) ? $_POST['data'] : '';
			$action = isset($_POST['action']) ? $_POST['action'] : '';
			$flag = isset($_POST['flag']) ? $_POST['flag'] : '';
			$arrData['id'] = isset($_POST['id']) ? $_POST['id'] : '';

			$arrValidationResult = $this->objUser->validateUser($arrData);
			
			if($action == 'edit')
				$action = 'update';
			
			if(!isset($arrValidationResult['error'])) {
				
				if(isset($_FILES['image']))
					$arrData['files'] = $_FILES['image'];
			
				$arrResult = $this->objUser->$action($arrData);
				
				if($action == 'create') {
					if(isset($arrResult['success']) && $arrResult['success'] == 'success') {
						/********************** START CODE Email ********************************/	
						$arrEmailData = [];
						$arrEmailData['useInvite'] 		= 'yes';
						$arrEmailData['password'] 		= $arrResult['random_password'];
						$arrEmailData['userName'] 		= $arrData['data']['f_name'].' '.$arrData['data']['l_name'];
						$arrEmailData['companyName'] 	= $_SESSION['COMPANY_NAME'];
						$arrEmailData['activation_url']	= HOST."/home/activateProfile?key=".$arrResult['activation_key'];
						
						$emailData = [];
						$emailData['emailFrom'] = EMAIL_FROM_ADMIN;
						
						$emailData['emailTo'] 	= $arrData['data']['email'];
						
						$emailData['subject'] 	= EMAIL_SUBJECT_REGISTRATION;
						
						$emailData['body']	  	= $this->getTemplateData('emails/emailRegistration', ['data' => $arrEmailData]);
						
						$this->objUtil->sendEmail($emailData);
						/********************** END CODE Email ********************************/
					}
				} 
				
				if(isset($arrResult['success']) && isset($arrResult['lastInsertedId'])) {
					 if($action == 'create')
						$res['successMsg'] = "User added Successfully";
					else if($action == 'update' && $flag == 'profile')
						$res['successMsg'] = "Profile updated Successfully";
					else
						$res['successMsg'] = "User updated Successfully";
				
				} else if(isset($arrValidationResult['error'])){
					$res['error'] = $arrValidationResult['error'];
				} else if(isset($arrResult['error'])){
					$res['error'] = $arrResult['error'];
				} 
				
				print $this->objUtil->setAjaxResponse($res);
			}
		} else {
			$this->renderView('home/login');
		}
	}

	public function changePasswordAction ( $arrParams = [] ) {

		if(isset($_SESSION['USER_ID'])) {

			$this->renderView('user/changePassword', [ 'data' => @$arrParams]);

		} else {

			$this->renderView('home/login');

		}
	}
	
	public function savePasswordAction ( $params ) {

		if(isset($_SESSION['USER_ID']) && isset($_POST)) {

			@extract($params['requestObj']);
						
			$arrResult = [];
			
			if (trim($data['password']) != trim($cnf_password)){
				$arrResult['error'] = 'New Password doesn\'t match with confirm password';
			} else {
				$arrResult = $this->objUser->updatePassword(trim($data['password']));
				if(isset($arrResult['lastInsertedId']) && isset($arrResult['success'])) {
					$arrResult['successMsg'] = "Password Changed Successfully";
				} else {
					$arrResult['error'] = 'Ooops error occured while changing password!';
				}
			}
			
			print $this->objUtil->setAjaxResponse($arrResult);

		} else {

			$this->renderView('home/login');

		}
	}
	
	public function activeAction( $params ) {
		
		if(isset($_SESSION['USER_ID'])) {
			@extract($params['requestObj']);

			$arrData = array();
			$arrData['id'] = (isset($id)) ? $id : 0;
			$arrData['is_active'] = (isset($flag)) ? $flag : 0 ;
			
			$arrResult = $this->objUser->setIsActive($arrData);	
			
			$this->listAction();	
		} else {
			$this->renderView('home/login');
		}
	}
	
}

$objUser = new User();
?>