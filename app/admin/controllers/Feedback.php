<?php

/**
* The default home controller, called when no controller/method has been passed 
* to the application. 
*/
class Feedback extends Controller
{
	protected $objFeedback;
	protected $objUtil;
	
	public function __construct() {
		
		$this->objFeedback	= $this->setModel('BaseFeedback');
		
		$this->objUtil 	= new Util();
	}
	
	public function indexAction($params = '') {
		
		if(isset($_SESSION['USER_ID']))
			$this->list();
		else 
			$this->renderView('home/login');
	}
	
	public function	listAction($params = '') {
		
		if(isset($_SESSION['USER_ID']))
			$this->renderView('feedback/list');
		else 
			$this->renderView('home/login');
	}
	
	public function	newAction($params = '') {
		
		if(isset($_SESSION['USER_ID'])){
			
			$arrResult = $this->objFeedback->retrieveProjectList('feedback');
			
			$this->renderView('feedback/new', [ 'data' => $arrResult ]);
		}
		else 
			$this->renderView('home/login');
	}
	
	public function	saveAction($params = '') {
		
		if(isset($_POST) && isset($_SESSION['USER_ID'])) {

			@extract($params['requestObj']);
			
			/*
			$projectId = explode('#', $project_id);
			$data[0]['projectName'] = '#'.$projectId[0].$projectId[1];
			$param = [];
			$param['projectId'] = $projectId[1];
			$param['type'] = isset($flag) ? $flag : 'feedback';
			$param['data'] = $data;
			
			$arrResult = $this->objFeedback->create($param);
			*/
			
			//print $this->objUtil->setAjaxResponse($arrResult);
			/********************** START CODE Email ********************************/	
			$projectId = explode('#', $project_id);
			
			$arrEmailData = [];
			
			$arrEmailData['projectName'] 	= '#'.@$projectId[0].@$projectId[1];
			$arrEmailData['userName'] 		= $_SESSION['USER_NAME'];
			$arrEmailData['companyName'] 	= $_SESSION['COMPANY_NAME'];
			$arrEmailData['formData']		= $data;
			
			$emailData = [];
			$emailData['emailFrom'] = $_SESSION['USER_EMAIL'];
			
			$emailData['emailCC'] 	= $_SESSION['USER_EMAIL'];
			$emailData['emailBCC']  = EMAIL_BCC;
			
			$emailData['subject'] 	= EMAIL_SUBJECT_FEEDBACK.' '.$arrEmailData['projectName'];
			
			$emailData['body']	  	= $this->getTemplateData('emails/feedback', ['data' => $arrEmailData]);
			
			//$this->objUtil->sendEmail($emailData);
			/********************** END CODE Email ********************************/
			if($this->objUtil->sendEmail($emailData)) {
				$arrResult['successMsg'] = "Feedback submitted through mail successfully";
			} else {
				$arrResult['error'] = 'Ooops error occured while sending feedback email please try again!';
			}
			
			print $this->objUtil->setAjaxResponse($arrResult);
			
		} else {
			header('Location: \login');
		}
	}

}

$objFeedback = new Feedback();
?>