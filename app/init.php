<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once 'config.php';

require_once 'settings.php';

require_once __DIR__ . '/../vendor/Autoload.php';

require_once 'core/DBManager.Class.php';

require_once 'core/SessionManager.Class.php';

require_once 'core/App.Class.php';

require_once 'core/Controller.Class.php';

$objSessionManager = NEW SessionManager($app);

require_once __DIR__ . '/../app/admin/layout.php';